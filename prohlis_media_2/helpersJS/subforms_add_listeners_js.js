
/**
 * [Wetter insertion subform] "submit button" onClick listener
 */
$('.js_wetter_subform_submit_button').click(function(){
	var wetter_type = $('#wetter_type').val();
	var db_table = 'wetter';
    var db_field = 'wetter_type';
    var id_field = 'wetter_id';
	var fail = '';
	if (wetter_type == null || wetter_type == "") {
		fail = 'Write wetter type';
	}
	if (fail == "") {

		$.ajax({
			url: '../../../handlersPHP/helpersPHP/db_data_existence_check.php',
			type: 'POST',
			cache: false,
			data: {'db_value':wetter_type, 'db_table':db_table, 'db_field':db_field},
			dataType: 'html',
			success: function(data){
				if (data==wetter_type) {
					toastr.error("Chosen wetter type is already in use", 'Error!', {
					timeOut: 5000
					})
				return false;
				}else{
					$.ajax ({
						url: '../../../handlersPHP/subform_handlers/addition_handlers/insert_item.php',
						type: 'POST',
						cache: false,
						data: {'item_name':wetter_type, 'table_name':db_table, 'field_name':db_field, "id_field":id_field},
						dataType: 'html',
						success: function(data){
							if (data == "RECORD ADDED") {
								toastr.success('New wetter type successfully added!', 'Success!', {
								timeOut: 5000
								})

								$('#wetter').append('<option value="' + wetter_type + '" selected="selected">' + wetter_type + '</option>');
								$(".wetter_red_line").css("box-shadow", "none");
								$('#wetter_type').val('');
							} else {
								toastr.error(data, 'Error!', {
								timeOut: 5000
								})
							}
						}
					})
				}
			}
		})
	} else {
	    $('#wetter_type').css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
        toastr.error(fail, 'Error!', {
        timeOut: 5000
        })
    }
});
$('#wetter_type').focus(function(){
	$('#wetter_type').css("box-shadow", "none");
});

/**
 * [Project insertion subform] "submit button" onClick listener
 */
$('.js_project_subform_submit_button').click(function(){
	var project_name_subform_input_field = $('#project_name_subform_input_field').val();
	var db_table = 'project';
	var db_field_project_name = 'project_name';
	var db_field_type_of_work = 'type_of_work';
	var fail = '';
	if (project_name_subform_input_field == null || project_name_subform_input_field == "") {
		fail = 'Write project name';
	}
	if (fail == "") {
		$.ajax({
			url: '../../../handlersPHP/helpersPHP/db_data_existence_project_check.php',
			type: 'POST',
			cache: false,
			data: {'db_value':project_name_subform_input_field, 'db_table':db_table, 'db_field_project_name':db_field_project_name, 'db_field_type_of_work':db_field_type_of_work, 'type_of_work':type_of_work},
			dataType: 'html',
			success: function(data){
				if (data==project_name_subform_input_field) {
					toastr.error("Chosen project name is already in use", 'Error!', {
					timeOut: 5000
					})

				return false;
				}else{
					$.ajax ({
						url: '../../../handlersPHP/subform_handlers/addition_handlers/insert_project_handler.php',
						type: 'POST',
						cache: false,
						data: {'project_name_subform_input_field':project_name_subform_input_field, 'type_of_work':type_of_work},
						dataType: 'html',
						success: function(data){
							if (data == "RECORD ADDED") {
								//alert('project subform success');
								//$('#project_subform_messageShow').html("<div class='subform_messageAppear'>"+data+"<br></div>");
								toastr.success('New project successfully added!', 'Success!', {
								timeOut: 5000
								})
								$('#project_name').append('<option value="' + project_name_subform_input_field + '" selected="selected">' + project_name_subform_input_field + '</option>');
								$('#project_name').css("box-shadow", "none");
								$('#project_name_subform_input_field').val('');
							} else {
								//alert('project_subform_fail');
								//$('#project_subform_messageShow').html("<div class='subform_messageAppear'>"+data+"<br></div>");
								toastr.error(data, 'Error!', {
								timeOut: 5000
								})
							}
						}
					})
				}
			}
		})
	}else{
	$('#project_name_subform_input_field').css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
	toastr.error(fail, 'Error!', {
	timeOut: 5000
	})
	}
});
$('#project_name_subform_input_field').focus(function(){
	$(this).css("box-shadow", "none");
});

/**
 * [Platz insertion subform] "submit button" onClick listener
 */
$('.js_platz_subform_submit_button').click(function(){
	var platz_name = $('#platz_name').val();
	var stadtteil_name = $('#stadtteil_name').val();
	var db_table = 'platz';
	var db_field = 'platz_name';
	var fail = '';
	if (platz_name == null || platz_name == "") {
		fail = 'Write platz name';
		$('#platz_name').css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
		toastr.error(fail, 'Error!', {
		timeOut: 5000
		})
		return false;
	}

	if (stadtteil_name == null || stadtteil_name == "" || stadtteil_name == "defaultv") {
		fail = 'Write stadtteil name';
		$('.stadtteil_red_line').css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
		toastr.error(fail, 'Error!', {
		timeOut: 5000
		})
		return false;
	}

		$.ajax({
			url: '../../../handlersPHP/helpersPHP/db_data_existence_check.php',
			type: 'POST',
			cache: false,
			data: {'db_value':platz_name, 'db_table':db_table, 'db_field':db_field},
			dataType: 'html',
			success: function(data){
				if (data==platz_name) {
					toastr.error("Chosen platz is already in use", 'Error!', {
					timeOut: 5000
					})

				return false;
				}else{
					$.ajax ({
						url: '../../../handlersPHP/subform_handlers/addition_handlers/insert_platz_handler.php',
						type: 'POST',
						cache: false,
						data: {'platz_name':platz_name, 'stadtteil_name':stadtteil_name},
						dataType: 'html',
						success: function(data){
							if (data == "RECORD ADDED") {
								//alert('platz subform success');
								//$('#platz_subform_messageShow').html("<div class='subform_messageAppear'>"+data+"<br></div>");
								toastr.success('New platz successfully added!', 'Success!', {
								timeOut: 5000
								})
								$('#platz').append('<option value="' + platz_name + '" selected="selected">' + platz_name + '</option>');
								$(".platz_red_line").css("box-shadow", "none");
								$('#platz_name').val('');
							} else {
								//alert('platz_subform_fail');
								//$('#platz_subform_messageShow').html("<div class='subform_messageAppear'>"+data+"<br></div>");
				                //$('#platz_subform_message_show').show();
				                toastr.error(data, 'Error!', {
								timeOut: 5000
								})
							}
						}
					})
				}
			}
		})
});
$('#platz_name').focus(function(){
	$(this).css("box-shadow", "none");
});
$('#stadtteil_name').change(function(){
	$('.stadtteil_red_line').css("box-shadow", "none");
});

/**
 * [Stadtteil insertion subform] "submit button" onClick listener
 */
$('.js_stadtteil_subform_submit_button').click(function(){
	var stadtteil_name = $('#stadtteil_name_field').val();
	var db_table = 'stadtteil';
    var db_field = 'stadtteil_name';
    var id_field = 'stadtteil_id';
	var fail = '';
	if (stadtteil_name == null || stadtteil_name == "") {
		fail = 'Write stadtteil name';
	}
	if (fail == "") {
		$.ajax({
			url: '../../../handlersPHP/helpersPHP/db_data_existence_check.php',
			type: 'POST',
			cache: false,
			data: {'db_value':stadtteil_name, 'db_table':db_table, 'db_field':db_field},
			dataType: 'html',
			success: function(data){
				if (data==stadtteil_name) {
					toastr.error("Chosen stadtteil name is already in use", 'Error!', {
					timeOut: 5000
					})
					return false;
				}else{
					$.ajax ({
						url: '../../../handlersPHP/subform_handlers/addition_handlers/insert_item.php',
						type: 'POST',
						cache: false,
						data: {'item_name':stadtteil_name, 'table_name':db_table, 'field_name':db_field, "id_field":id_field},
						dataType: 'html',
						success: function(data){
							if (data == "RECORD ADDED") {
								$('#stadtteil_name').append('<option value="' + stadtteil_name + '" selected="selected">' + stadtteil_name + '</option>');
								$(".stadtteil_red_line").css("box-shadow", "none");
								$('#stadtteil_name_field').val('');
								$('#new_platz_page').fadeIn('slow');
								$('#new_stadtteil_page').fadeOut('slow');
								toastr.success('New stadtteil name successfully added!', 'Success!', {
								timeOut: 5000
								})
							} else {
								toastr.error(data, 'Error!', {
								timeOut: 5000
								})
							}
						}
					})
				}
			}
		})
	}else{
		$('#stadtteil_name_field').css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
		toastr.error(fail, 'Error!', {
		timeOut: 5000
		})
	}
});
$('#stadtteil_name_field').focus(function(){
	$(this).css("box-shadow", "none");
});

/**
 * [Ausfallgrund insertion subform] "submit button" onClick listener
 */
$('.js_ausfallgrund_subform_submit_button').click(function(){
	var ausfallgrund_type = $('#ausfallgrund_type').val();
	var db_table = 'ausfallgrund';
    var db_field = 'ausfallgrund_type';
    var id_field = 'ausfallgrund_id';
	var fail = '';
	if (ausfallgrund_type == null || ausfallgrund_type == "") {
		fail = 'Write ausfallgrund type';
	}
	if (fail == "") {
		$.ajax({
			url: '../../../handlersPHP/helpersPHP/db_data_existence_check.php',
			type: 'POST',
			cache: false,
			data: {'db_value':ausfallgrund_type, 'db_table':db_table, 'db_field':db_field},
			dataType: 'html',
			success: function(data){
				if (data==ausfallgrund_type) {
					toastr.error("Chosen ausfallgrund type is already in use", 'Error!', {
					timeOut: 5000
					})

				return false;
				}else{
					$.ajax ({
						url: '../../../handlersPHP/subform_handlers/addition_handlers/insert_item.php',
						type: 'POST',
						cache: false,
						data: {'item_name':ausfallgrund_type, 'table_name':db_table, 'field_name':db_field, "id_field":id_field},
						dataType: 'html',
						success: function(data){
							if (data == "RECORD ADDED") {
								$('#ausfallgrund').append('<option value="' + ausfallgrund_type + '" selected="selected">' + ausfallgrund_type + '</option>');
								$('#ausfallgrund').css("box-shadow", "none");
								$('#ausfallgrund_type').val('');
								ausfallgrundFunc();
								toastr.success('New ausfallgrund type successfully added!', 'Success!', {
								timeOut: 5000
								})
							} else {
								toastr.error(data, 'Error!', {
								timeOut: 5000
								})
							}
						}
					})
				}
			}
		})
	}else{
		$('#ausfallgrund_type').css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
		toastr.error(fail, 'Error!', {
		timeOut: 5000
		})
	}
});
$('#ausfallgrund_type').focus(function(){
	$(this).css("box-shadow", "none");
});

/**
 * [Mobilitat insertion subform] "submit button" onClick listener
 */
$('.js_mobilitat_subform_submit_button').click(function(){
	var mobilitat_type = $('#mobilitat_type').val();
	var db_table = 'mobilitat';
    var db_field = 'mobilitat_type';
    var id_field = 'mobilitat_id';
	var fail = '';
	if (mobilitat_type == null || mobilitat_type == "") {
		fail = 'Write mobilitat type';
	}
	if (fail == "") {
		$.ajax({
			url: '../../../handlersPHP/helpersPHP/db_data_existence_check.php',
			type: 'POST',
			cache: false,
			data: {'db_value':mobilitat_type, 'db_table':db_table, 'db_field':db_field},
			dataType: 'html',
			success: function(data){
				if (data==mobilitat_type) {
					toastr.error("Chosen mobilitat type is already in use", 'Error!', {
					timeOut: 5000
					})

				return false;
				}else{
					$.ajax ({
						url: '../../../handlersPHP/subform_handlers/addition_handlers/insert_item.php',
						type: 'POST',
						cache: false,
						data: {'item_name':mobilitat_type, 'table_name':db_table, 'field_name':db_field, "id_field":id_field},
						dataType: 'html',
						success: function(data){
							if (data == "RECORD ADDED") {
								$('#mobilitat').append('<option value="' + mobilitat_type + '" selected="selected">' + mobilitat_type + '</option>');
								$(".mobilitat_red_line").css("box-shadow", "none");
								$('#mobilitat_type').val('');
								toastr.success('New mobilitat type successfully added!', 'Success!', {
								timeOut: 5000
								})
							} else {
								toastr.error(data, 'Error!', {
								timeOut: 5000
								})
							}
						}
					})
				}
			}
		})
	}else{
		$('#mobilitat_type').css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
		toastr.error(fail, 'Error!', {
		timeOut: 5000
		})
	}
});
$('#mobilitat_type').focus(function(){
	$(this).css("box-shadow", "none");
});

/**
 * [Mitarbeiter insertion subform] "submit button" onClick listener
 */
$('.js_mitarbeiter_subform_submit_button').click(function(){
	var fail = '-';
	var mitarbeiter_username;
	var mitarbeiter_password;
	var mitarbeiter_repeat_password;
	var arbeiter_first_name;
	var arbeiter_last_name;
	var db_table = "user";
	var db_field_username = "username";
	var db_field_short_name = "short_name";
	var work_status;
	var arbeiter_short_name;


if ($("#worker_mitarbeiter_check").is(':checked')){
	mitarbeiter_username = '-';
	mitarbeiter_password = '-';
	mitarbeiter_repeat_password = '-';
	work_status = "worker";
	arbeiter_first_name = $("#arbeiter_first_name").val();
	arbeiter_last_name = $("#arbeiter_last_name").val();

	if(arbeiter_first_name == '' || arbeiter_first_name == null){
		fail = 'Write first name';
		$('#arbeiter_first_name').css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
		toastr.error(fail, 'Error!', {
		timeOut: 5000
		})
		return false;
	}
	if(arbeiter_last_name == '' || arbeiter_last_name == null){
		fail = 'Write last name';
		$('#arbeiter_last_name').css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
		toastr.error(fail, 'Error!', {
		timeOut: 5000
		})
		return false;
	}
}else{
	mitarbeiter_username = $("#mitarbeiter_username").val();
	mitarbeiter_password = $("#mitarbeiter_password").val();
	mitarbeiter_repeat_password = $("#mitarbeiter_repeat_password").val();
	arbeiter_first_name = $("#arbeiter_first_name").val();
	arbeiter_last_name = $("#arbeiter_last_name").val();
	work_status = "user";

	if(mitarbeiter_username == '' || mitarbeiter_username == null){
		fail = 'Write username';
		$('#mitarbeiter_username').css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
		toastr.error(fail, 'Error!', {
		timeOut: 5000
		})
		return false;
	}
	if(mitarbeiter_password == '' || mitarbeiter_password == null){
		fail = 'Write password';
		$('#mitarbeiter_password').css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
		toastr.error(fail, 'Error!', {
		timeOut: 5000
		})
		return false;
	}
	if(mitarbeiter_repeat_password == '' || mitarbeiter_repeat_password == null){
		fail = 'Repeat the password';
		$('#mitarbeiter_repeat_password').css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
		toastr.error(fail, 'Error!', {
		timeOut: 5000
		})
		return false;
	}
	
	if(mitarbeiter_password != mitarbeiter_repeat_password){
		$("#mitarbeiter_password").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
		$("#mitarbeiter_repeat_password").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
		fail = 'Passwords should be equal';
		toastr.error(fail, 'Error!', {
		timeOut: 5000
		})
		return false;
	}

	if(arbeiter_first_name == '' || arbeiter_first_name == null){
		fail = 'Write first name';
		$('#arbeiter_first_name').css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
		toastr.error(fail, 'Error!', {
		timeOut: 5000
		})
		return false;
	}
	if(arbeiter_last_name == '' || arbeiter_last_name == null){
		fail = 'Write last name';
		$('#arbeiter_last_name').css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
		toastr.error(fail, 'Error!', {
		timeOut: 5000
		})
		return false;
	}
}
	arbeiter_short_name = arbeiter_first_name+arbeiter_last_name;

	$.ajax ({
		url: '../../../handlersPHP/subform_handlers/addition_handlers/insert_mitarbeiter_handler.php',
		type: 'POST',
		cache: false,
		data: {'mitarbeiter_username':mitarbeiter_username, 'arbeiter_short_name':arbeiter_short_name, 'mitarbeiter_password':mitarbeiter_password, 'arbeiter_first_name':arbeiter_first_name, 'arbeiter_last_name':arbeiter_last_name, 'arbeiter_short_name':arbeiter_short_name,'work_status':work_status},
		dataType: 'html',
		success: function(data){
			var obj = JSON.parse(data);
            if (obj.message == mitarbeiter_username){
				$('#mitarbeiter_username').css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
			    toastr.error('Chosen username is already in use', 'Error!', {
				timeOut: 5000,
				})
            }else if(obj.message == arbeiter_short_name){
				$('#arbeiter_first_name').css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
				$('#arbeiter_last_name').css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
			    toastr.error('Chosen mitarbeiter is already registered in DB', 'Error!', {
				timeOut: 5000
				})
            }else if(obj.message == 'New mitarbeiter added'){
				$('#mitarbeiter02').append('<option value="' + arbeiter_short_name + '" selected="selected">' + arbeiter_first_name + ' ' + arbeiter_last_name + '</option>');
				$('.mitarbeiter02_red_line').css("box-shadow", "none");
				$('#mitarbeiter03').append('<option value="' + arbeiter_short_name + '" disabled>' + arbeiter_first_name + ' ' + arbeiter_last_name + '</option>');
				$('.mitarbeiter03_red_line').css("box-shadow", "none");
				$('#mitarbeiter_username').val('').css("box-shadow", "none");
				$('#mitarbeiter_password').val('').css("box-shadow", "none");
				$('#mitarbeiter_repeat_password').val('').css("box-shadow", "none");
				$('#arbeiter_first_name').val('').css("box-shadow", "none");
				$('#arbeiter_last_name').val('').css("box-shadow", "none");
				toastr.success(obj.message, 'Success!', {
				timeOut: 5000
				})
			}else{
				toastr.error(obj.message, 'Error!', {
				timeOut: 5000
				})
			}
		}
	})
});

/**
 * [Angebot insertion subform] "submit button" onClick listener
 */
$('.js_angebot_subform_submit_button').click(function(){
	var angebot_type = $('#angebot_type').val();
	var db_table = 'angebot';
    var db_field = 'angebot_name';
    var id_field = 'angebot_id';
	var fail = '';
	if (angebot_type == null || angebot_type == "") {
		fail = 'Write angebot type';
	}
	if (fail == "") {
		$.ajax({
			url: '../../../handlersPHP/helpersPHP/db_data_existence_check.php',
			type: 'POST',
			cache: false,
			data: {'db_value':angebot_type, 'db_table':db_table, 'db_field':db_field},
			dataType: 'html',
			success: function(data){
				if (data==angebot_type) {
					toastr.error("Chosen angebot type is already in use", 'Error!', {
					timeOut: 5000
					})

				return false;
				}else{
					$.ajax ({
						url: '../../../handlersPHP/subform_handlers/addition_handlers/insert_item.php',
						type: 'POST',
						cache: false,
						data: {'item_name':angebot_type, 'table_name':db_table, 'field_name':db_field, "id_field":id_field},
						dataType: 'html',
						success: function(data){
							if (data == "RECORD ADDED") {
								$('#angebot').append('<option value="' + angebot_type + '" selected="selected">' + angebot_type + '</option>');
								$(".angebot_red_line").css("box-shadow", "none");
								$('#angebot_type').val('');
								toastr.success('New angebot type successfully added!', 'Success!', {
								timeOut: 5000
								})
							} else {
								toastr.error(data, 'Error!', {
								timeOut: 5000
								})
							}
						}
					})
				}
			}
		})
	}else{
		$('#angebot_type').css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
		toastr.error(fail, 'Error!', {
		timeOut: 5000
		})
	}
});
$('#angebot_type').focus(function(){
	$(this).css("box-shadow", "none");
});

/**
 * [Aktion insertion subform] "submit button" onClick listener
 */
$('.js_aktion_subform_submit_button').click(function(){
	var aktion_type = $('#aktion_type').val();
	var db_table = 'aktion';
    var db_field = 'aktion_type';
    var id_field = 'aktion_id';
	var fail = '';
	if (aktion_type == null || aktion_type == "") {
		fail = 'Write aktion type';
	}
	if (fail == "") {
		$.ajax({
			url: '../../../handlersPHP/helpersPHP/db_data_existence_check.php',
			type: 'POST',
			cache: false,
			data: {'db_value':aktion_type, 'db_table':db_table, 'db_field':db_field},
			dataType: 'html',
			success: function(data){
				if (data==aktion_type) {
					toastr.error("Chosen aktion type is already in use", 'Error!', {
					timeOut: 5000
					})

				return false;
				}else{
					$.ajax ({
						url: '../../../handlersPHP/subform_handlers/addition_handlers/insert_item.php',
						type: 'POST',
						cache: false,
						data: {'item_name':aktion_type, 'table_name':db_table, 'field_name':db_field, "id_field":id_field},
						dataType: 'html',
						success: function(data){
							if (data == "RECORD ADDED") {
								$('#aktion').append('<option value="' + aktion_type + '" selected="selected">' + aktion_type + '</option>');
								$(".aktion_red_line").css("box-shadow", "none");
								$('#aktion_type').val('');
								toastr.success('New aktion type successfully added!', 'Success!', {
								timeOut: 5000
								})
							} else {
								toastr.error(data, 'Error!', {
								timeOut: 5000
								})
							}
						}
					})
				}
			}
		})
	}else{
		$('#aktion_type').css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
		toastr.error(fail, 'Error!', {
		timeOut: 5000
		})
	}
});
$('#aktion_type').focus(function(){
	$(this).css("box-shadow", "none");
});


/**
 * [Arte des Feedback insertion subform] "submit button" onClick listener
 */
$('.js_art_des_feedback_subform_submit_button').click(function(){
	var art_des_feedback_type = $('#art_des_feedback_type').val();
	var db_table = 'art_des_feedback';
    var db_field = 'art_des_feedback_type';
    var id_field = 'art_des_feedback_id';
	var fail = '';
	if (art_des_feedback_type == null || art_des_feedback_type == "") {
		fail = 'Write art des feedback type';
	}
	if (fail == "") {
		$.ajax({
			url: '../../../handlersPHP/helpersPHP/db_data_existence_check.php',
			type: 'POST',
			cache: false,
			data: {'db_value':art_des_feedback_type, 'db_table':db_table, 'db_field':db_field},
			dataType: 'html',
			success: function(data){
				if (data==art_des_feedback_type) {
					toastr.error("Chosen art des feedback type is already in use", 'Error!', {
					timeOut: 5000
					})

				return false;
				}else{
					$.ajax ({
						url: '../../../handlersPHP/subform_handlers/addition_handlers/insert_item.php',
						type: 'POST',
						cache: false,
						data: {'item_name':art_des_feedback_type, 'table_name':db_table, 'field_name':db_field, "id_field":id_field},
						dataType: 'html',
						success: function(data){
							if (data == "RECORD ADDED") {
								$('#art_des_feedback').append('<option value="' + art_des_feedback_type + '" selected="selected">' + art_des_feedback_type + '</option>');
								$(".art_des_feedback_red_line").css("box-shadow", "none");
								$('#art_des_feedback_type').val('');
								toastr.success('New art des feedback type successfully added!', 'Success!', {
								timeOut: 5000
								})
							} else {
								toastr.error(data, 'Error!', {
								timeOut: 5000
								})
							}
						}
					})
				}
			}
		})
	}else{
		$('#art_des_feedback_type').css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
		toastr.error(fail, 'Error!', {
		timeOut: 5000
		})
	}
});
$('#art_des_feedback_type').focus(function(){
	$(this).css("box-shadow", "none");
});