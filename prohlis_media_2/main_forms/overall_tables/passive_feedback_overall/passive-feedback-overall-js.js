var db_table = 'passive_feedback_record';
var db_field = 'passive_feedback_id';

$(document).ready(function(){
    $('#myTable01').fixedHeaderTable({ footer: false, cloneHeadToFoot: false, altClass: 'odd', autoShow: false, fixedColumn:true });
    $('#myTable01').fixedHeaderTable('show', 1000);
});

/**
 * Function for downloading passive feedback
 * (user should specify the record he want to download)
 */
$('.js_record_row_print_button').click(function(){
    var printing_id = $("#printing_id_input_field").val();
    var fail = '';
    if (printing_id == null || printing_id == "") {
        fail = "Write number of row you want to download";
        toastr.error(fail, 'Error!', {
            timeOut: 5000
        })
    } else {
        $.ajax({
            type:'POST',
            url: '../../../handlersPHP/export_handlers/feedbacks/passive-feedback/passive_feedback_export.php',
            data: {'passive_feedback_id':printing_id},
            dataType:'json'
        }).done(function(data){
            var $a = $("<a>");
            $a.attr("href",data.file);
            $("body").append($a);
            $a.attr("download","Passive feedback("+data.datum+").xlsx");
            $a[0].click();
            $a.remove();
            $(".container-loader").fadeOut("fast");
            setTimeout(function(){$(".container-table").fadeIn("slow");},300);
            toastr.info('Report generated and downloaded', {timeOut: 5000})
        });
    }
});