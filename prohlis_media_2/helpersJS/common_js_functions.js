/**
 * Datetype templating function
 */
Date.prototype.yyyymmdd = function() {
    var yyyy = this.getFullYear();
    var mm = this.getMonth() < 9 ? "0" + (this.getMonth() + 1) : (this.getMonth() + 1); // getMonth() is zero-based
    var dd  = this.getDate() < 10 ? "0" + this.getDate() : this.getDate();
    return "".concat(yyyy).concat("-").concat(mm).concat("-").concat(dd);
   };

/**
 * Function for cookies existence validation
 */
$(document).ready(function(){
	var cookies = document.cookie;
	if (cookies != '') {
	} else {
		alert('You need to authorise yourself');
		// window.open("../index.php","_self");
	}
});

function get_cookie(cookie_name){
    var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );
    if (results)
      return (unescape(results[2]));
    else
      return null;
    }

/**
 * [Mitarbeiter multiple selectors]Function for disabling selection possibilities for items 
 * which are selected in other <select> fields
 */
$('.mitarbeiterSelection').change(function() {
    var selectedOptions = $('.mitarbeiterSelection option:selected');
    $('.mitarbeiterSelection option').removeAttr('disabled');

    selectedOptions.each(function() {

        var value = this.value;
        if (value != ''){
        var id = $(this).parent('select').attr('id');
        var options = $('select:not(#' + id + ') option[value=' + value + ']');
        options.attr('disabled', 'true');
        }
    });
});

/**
 * "Einreichen" button onClick listener (Main-form) button
 */
$(".js_main_form_submit_button").click(function(){
    submitRecordFunc();
});

/**
 * "Neuer Rekord" button onClick listener (Main-form) button
 */
$('.js_main_form_new_record_button').click(function(){
	newRecord();
});

/**
 * Function for removing box-shadows from "Bekannt, Unbekannt, under 12(m)..."
 */
function borderShadowNone() {
    $(".people-sum-check").css("box-shadow", "none");
}

/**
 * Box-shadow remove functions in case of resolving issues of error detection
 */
$("#project_name").change(function(){
    $(this).css("box-shadow", "none");
});

$("#datum").focus(function(){
    $(this).css("box-shadow", "none");
});

$("#uhrzeit").focus(function(){
    $(this).css("box-shadow", "none");
});

$("#dauer_in_h").focus(function(){
    $(this).css("box-shadow", "none");
});

$("#mitarbeiter_username").focus(function(){
    $(this).css("box-shadow", "none");
});

$("#arbeiter_first_name").focus(function(){
    $("#arbeiter_last_name").css("box-shadow", "none");
    $("#arbeiter_first_name").css("box-shadow", "none");
});

$("#arbeiter_last_name").focus(function(){
    $("#arbeiter_last_name").css("box-shadow", "none");
    $("#arbeiter_first_name").css("box-shadow", "none");
});

$("#mitarbeiter_repeat_password").focus(function(){
    $('#mitarbeiter_repeat_password').css("box-shadow", "none");
    $('#mitarbeiter_password').css("box-shadow", "none");
});

$("#mitarbeiter_password").focus(function(){
    $('#mitarbeiter_password').css("box-shadow", "none");
    $('#mitarbeiter_repeat_password').css("box-shadow", "none");
});

$(".wetter_red_line").change(function(){
    $('.wetter_red_line').css("box-shadow", "none");
});

$(".platz_red_line").change(function(){
    $('.platz_red_line').css("box-shadow", "none");
});

$(".mobilitat_red_line").change(function(){
    $(".mobilitat_red_line").css("box-shadow", "none");
});

$('#mitarbeiter02').change(function(){
	$('.mitarbeiter02_red_line').css("box-shadow", "none");
});

$("#angebot").change(function(){ 
    $(".angebot_red_line").css("box-shadow", "none"); 
}); 

$("#an_wen").focus(function(){ 
    $(this).css("box-shadow", "none"); 
}); 

$("#art_des_feedback").change(function(){ 
    $('.art_des_feedback_red_line').css("box-shadow", "none"); 
}); 

$("#inhalt_des_feedback").focus(function(){ 
    $(this).css("box-shadow", "none"); 
}); 

$("#weiterbearbeitung").focus(function(){ 
    $(this).css("box-shadow", "none"); 
});

$(".bekannt-color").focus(function(){
    $(".people-sum-check").css("box-shadow", "none");
});

/**
 * Passive feedback section
 */
$("#aktion").change(function(){ 
    $('.aktion_red_line').css("box-shadow", "none"); 
});

$("#mitarbeiter").change(function(){ 
    $('.mitarbeiter_red_line').css("box-shadow", "none"); 
});

$('.rinput_class').change(function(){
    if (this.checked){
        $('#allgemeine_einschatzung_legend').css("box-shadow", "none"); 
    }
}); 

$("#teilnehmer_innen").focus(function(){ 
    $(this).css("box-shadow", "none"); 
});

$("#fb_anzahl").focus(function(){
    $(this).css("box-shadow", "none");
});

$("#fb_geber_innen").focus(function(){
    $(this).css("box-shadow", "none");
});



/**
 * "Delete last inserted record" button onClick listener (Main-form) button
 */
$('.js_revert_button').click(function(){
	$.ajax ({
		url: '../../../handlersPHP/helpersPHP/delete_last_inserted_record.php',
		type: 'POST',
		cache: false,
		data: {'db_table':db_table_revert, 'db_field':db_field_revert},
		dataType: 'html',
		success: function(data){
			if (data == "RECORD DELETED") {
				toastr.success('Last inserted record has been removed', 'Success!', {timeOut: 5000})
			} else {
				toastr.error('Deletion error', 'Error!', {timeOut: 5000})
			}
		}
	})
});

/**
 * Ausfallgrund function, activates if the "ausfallgrund" option is selected,
 * mainly used on "streetwork" form, disables the majority of input fields
 */
function ausfallgrundFunc(){
    var ausfallgrund = $('#ausfallgrund').val();

    if (ausfallgrund == '-' || ausfallgrund == 'defaultv' || ausfallgrund == null) {
        $('.not-ausfallgrund').prop('disabled', false);
        $('#project_name').val('defaultv');
        $('.mitarbeiter02_red_line').css("box-shadow", "none");
        $('#mitarbeiter03').val('defaultv');
        $('#platz').val('defaultv');
        $('#wetter').val('defaultv');
        $('#mobilitat').val('defaultv');
        $('.glyphicon').css("pointer-events", "auto");
        $('.not-ausfallgrund').css("box-shadow", "none");
        toastr.info('Fields enabled', 'Notificatoin!', {
            timeOut: 5000})
    }else{
        $('.not-ausfallgrund').prop('disabled', true);
        $('.platz_red_line').css("box-shadow", "none");
        $('.wetter_red_line').css("box-shadow", "none");
        $('.mobilitat_red_line').css("box-shadow", "none");
        $('.mitarbeiter02_red_line').css("box-shadow", "none");
        $('.not-ausfallgrund').val('');
        $('.not-ausfallgrund-checkbox').prop('checked', false);
        $('.glyphicon').css("pointer-events", "none");
        $('.glyphicon.js_glyphicon_plus_ausfallgrund').css("pointer-events", "auto");
        $('.glyphicon.js_glyphicon_delete_ausfallgrund').css("pointer-events", "auto");
        $('.glyphicon.js_glyphicon_plus_mitarbeiter').css("pointer-events", "auto");
        $('.glyphicon.js_glyphicon_delete_mitarbeiter').css("pointer-events", "auto");
        toastr.info('Fields disabled', 'Notificatoin!', {
            timeOut: 5000
        })
        return false;
    }
}
$('#ausfallgrund').change(ausfallgrundFunc);

$('#worker_mitarbeiter_check').click(function(){
    if ($("#worker_mitarbeiter_check").is(':checked')) {
        $('#mitarbeiter_username').prop('disabled', true);
        $('#mitarbeiter_password').prop('disabled', true);
        $('#mitarbeiter_repeat_password').prop('disabled', true);

    } else {
        $('#mitarbeiter_username').prop('disabled', false);
        $('#mitarbeiter_password').prop('disabled', false);
        $('#mitarbeiter_repeat_password').prop('disabled', false);
    }
});