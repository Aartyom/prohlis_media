var type_of_work = 'passive_feedback';
var db_table_revert = 'passive_feedback_record';
var db_field_revert = 'passive_feedback_id'; 
 
Date.prototype.yyyymmdd = function() { 
   var yyyy = this.getFullYear(); 
   var mm = this.getMonth() < 9 ? "0" + (this.getMonth() + 1) : (this.getMonth() + 1); // getMonth() is zero-based 
   var dd  = this.getDate() < 10 ? "0" + this.getDate() : this.getDate(); 
   return "".concat(yyyy).concat("-").concat(mm).concat("-").concat(dd); 
}; 


//------------------------------------------------------------------ 
 
function submitRecordFunc(){
 
    var angebot = $('#angebot').val();
    var aktion = $('#aktion').val();
	var datum_nf = $("#datum").val();
    if(datum_nf != '') {
        var patterndatum = /[0-9]{2}.[0-9]{2}.[0-9]{4}/i;
        if (patterndatum.test(datum_nf)) {
            var datum_nf = $('#datum').val().split(".");
        }else{
            $("#datum").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
            toastr.error('Date format is incorrect (dd.mm.yyyy)', 'Error!', {
            timeOut: 5000
            })
            return false;
        }
    }else{
        fail = 'Fill the "Datum" field'
        toastr.error(fail, 'Error!', {
        timeOut: 5000
        })
        $("#datum").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
        return false;
    };
    var datum = new Date(datum_nf[2], datum_nf[1] - 1, datum_nf[0]).yyyymmdd();
	var mitarbeiter = $('#mitarbeiter02').val();
	var positiv_resp = $('#positiv_resp').val();
	var negativ_resp = $('#negativ_resp').val();
	var schlussfolgerungen = $('#schlussfolgerungen').val();
	var rahmenbedingungen = $('#rahmenbedingungen').val();
    var teilnehmer_innen = $('#teilnehmer_innen').val();
    var fb_anzahl = $('#fb_anzahl').val();
    var fb_geber_innen = $('#fb_geber_innen').val();

    if (document.querySelector('input[name="allgemeine_einschatzung"]:checked')) {
        var allgemeine_einschatzung = document.querySelector('input[name="allgemeine_einschatzung"]:checked').value;
    } else {
        var allgemeine_einschatzung = null;
    }

	var fail = '';

	if (angebot == null || datum == null || mitarbeiter == null || aktion  == null || allgemeine_einschatzung  == null || fb_anzahl  == null ||
        fb_geber_innen  == null || teilnehmer_innen  == null || angebot == '' || datum == '' || mitarbeiter == '' || aktion  == '' ||
        allgemeine_einschatzung  == '' || fb_anzahl  == '' || fb_geber_innen  == '' || teilnehmer_innen  == '') {
		fail = 'Fill in all required fields';
	}

    if (fail != "") {
    	if (angebot == null || angebot == "") {
			$(".angebot_red_line").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
		}

    	if (mitarbeiter == null || mitarbeiter == "") {
			$(".mitarbeiter02_red_line").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
		}

    	if (datum == "" || datum == null) {
    		$("#datum").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
    	}

        if (aktion == null || aktion == "") {
			$(".aktion_red_line").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
		}

        if (allgemeine_einschatzung == null || allgemeine_einschatzung == "") {
			$("#allgemeine_einschatzung_legend").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
		}

        if (fb_geber_innen == null || fb_geber_innen == "") {
            $("#fb_geber_innen").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
        }

        if (fb_anzahl == null || fb_anzahl == "") {
            $("#fb_anzahl").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
        }

        if (teilnehmer_innen == null || teilnehmer_innen == "") {
            $("#teilnehmer_innen").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
        }

        toastr.error(fail, 'Error!', {
        timeOut: 5000
        })
        return false;
    }

    if(rahmenbedingungen  == '-' || rahmenbedingungen  == null || rahmenbedingungen  == "") rahmenbedingungen = '-';
    if(positiv_resp  == '-' || positiv_resp  == null || positiv_resp  == "") positiv_resp = '-';
    if(negativ_resp  == '-' || negativ_resp  == null || negativ_resp  == "") negativ_resp = '-';
    if(schlussfolgerungen  == '-' || schlussfolgerungen  == null || schlussfolgerungen  == "") schlussfolgerungen = '-';

	$.ajax ({
            url: '../../../handlersPHP/main_form_handlers/passive_feedback_handler.php',
            type: 'POST',
            cache: false,
            data: {'angebot':angebot, 'aktion':aktion, 'datum':datum, 'mitarbeiter':mitarbeiter, 'positiv_resp':positiv_resp, 'negativ_resp':negativ_resp, 'schlussfolgerungen':schlussfolgerungen, 'rahmenbedingungen':rahmenbedingungen, 'teilnehmer_innen':teilnehmer_innen, 'fb_anzahl':fb_anzahl, 'fb_geber_innen':fb_geber_innen, 'allgemeine_einschatzung':allgemeine_einschatzung},
            dataType: 'html',
            success: function(data){
                objectJson = JSON.parse(data);
                    toastr.info(objectJson.message, 'Success!', {
                    timeOut: 5000,

                })

            }
        })
}

$('.js_main_form_download_record_button').click(function(){
    var angebot = $('#angebot').val();
    var aktion = $('#aktion').val();
	var datum_nf = $("#datum").val();
    if(datum_nf != '') {
        var patterndatum = /[0-9]{2}.[0-9]{2}.[0-9]{4}/i;
        if (patterndatum.test(datum_nf)) {
            var datum_nf = $('#datum').val().split(".");
        }else{
            $("#datum").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
            toastr.error('Date format is incorrect (dd.mm.yyyy)', 'Error!', {
            timeOut: 5000
            })
            return false;
        }
    }else{
        fail = 'Fill the "Datum" field'
        toastr.error(fail, 'Error!', {
        timeOut: 5000
        })
        $("#datum").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
        return false;
    };
    var datum = new Date(datum_nf[2], datum_nf[1] - 1, datum_nf[0]).yyyymmdd();
	var mitarbeiter = $('#mitarbeiter02').val();
	var positiv_resp = $('#positiv_resp').val();
	var negativ_resp = $('#negativ_resp').val();
	var schlussfolgerungen = $('#schlussfolgerungen').val();
	var rahmenbedingungen = $('#rahmenbedingungen').val();
    var teilnehmer_innen = $('#teilnehmer_innen').val();
    var fb_anzahl = $('#fb_anzahl').val();
    var fb_geber_innen = $('#fb_geber_innen').val();

    if (document.querySelector('input[name="allgemeine_einschatzung"]:checked')) {
        var allgemeine_einschatzung = document.querySelector('input[name="allgemeine_einschatzung"]:checked').value;
    } else {
        var allgemeine_einschatzung = null;
    }

	var fail = '';

	if (angebot == null || datum == null || mitarbeiter == null || aktion  == null || allgemeine_einschatzung  == null || fb_anzahl  == null ||
        fb_geber_innen  == null || teilnehmer_innen  == null || angebot == '' || datum == '' || mitarbeiter == '' || aktion  == '' ||
        allgemeine_einschatzung  == '' || fb_anzahl  == '' || fb_geber_innen  == '' || teilnehmer_innen  == '') {
		fail = 'Fill in all required fields';
	}

    if (fail != "") {
    	if (angebot == null || angebot == "") {
			$(".angebot_red_line").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
		}

    	if (mitarbeiter == null || mitarbeiter == "") {
			$(".mitarbeiter02_red_line").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
		}

    	if (datum == "" || datum == null) {
    		$("#datum").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
    	}

        if (aktion == null || aktion == "") {
			$(".aktion_red_line").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
		}

        if (allgemeine_einschatzung == null || allgemeine_einschatzung == "") {
			$("#allgemeine_einschatzung_legend").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
		}

        if (fb_geber_innen == null || fb_geber_innen == "") {
            $("#fb_geber_innen").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
        }

        if (fb_anzahl == null || fb_anzahl == "") {
            $("#fb_anzahl").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
        }

        if (teilnehmer_innen == null || teilnehmer_innen == "") {
            $("#teilnehmer_innen").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
        }

        toastr.error(fail, 'Error!', {
        timeOut: 5000
        })
        return false;
    }

    if(rahmenbedingungen  == '-' || rahmenbedingungen  == null || rahmenbedingungen  == "") rahmenbedingungen = '-';
    if(positiv_resp  == '-' || positiv_resp  == null || positiv_resp  == "") positiv_resp = '-';
    if(negativ_resp  == '-' || negativ_resp  == null || negativ_resp  == "") negativ_resp = '-';
    if(schlussfolgerungen  == '-' || schlussfolgerungen  == null || schlussfolgerungen  == "") schlussfolgerungen = '-';

	$.ajax ({
        type:'POST',
        url:"../../../handlersPHP/export_handlers/feedbacks/passive-feedback/passive_feedback_print.php",
        data: {'angebot':angebot, 'aktion':aktion, 'datum':datum, 'mitarbeiter':mitarbeiter, 'positiv_resp':positiv_resp, 'negativ_resp':negativ_resp, 'schlussfolgerungen':schlussfolgerungen, 'rahmenbedingungen':rahmenbedingungen, 'teilnehmer_innen':teilnehmer_innen, 'fb_anzahl':fb_anzahl, 'fb_geber_innen':fb_geber_innen, 'allgemeine_einschatzung':allgemeine_einschatzung},
        dataType: 'json'
    }).done(function(data){
        var $a = $("<a>");
        $a.attr("href",data.file);
        $("body").append($a);
        $a.attr("download","Passive_feedback.xlsx");
        $a[0].click();
        $a.remove();
        toastr.info('Report generated and downloaded', {timeOut: 5000})
    });
});
//--------------------------------------------------------------------

function newRecord(){
    $("#angebot").val('defaultv').css("box-shadow", "none");
    $(".angebot_red_line").css("box-shadow", "none");
    $("#aktion").val('defaultv').css("box-shadow", "none");
    $(".aktion_red_line").css("box-shadow", "none");
    $("#mitarbeiter02").val('defaultv').css("box-shadow", "none");
    $(".mitarbeiter02_red_line").css("box-shadow", "none");
    $("#positiv_resp").val('').css("box-shadow", "none");
    $("#negativ_resp").val('').css("box-shadow", "none");
    $("#teilnehmer_innen").val('').css("box-shadow", "none");
	$("#fb_anzahl").val('').css("box-shadow", "none");
    $("#fb_geber_innen").val('').css("box-shadow", "none");
    $("#rahmenbedingungen").val('').css("box-shadow", "none");
    $("#schlussfolgerungen").val('').css("box-shadow", "none");
    $("#allgemeine_einschatzung_legend").css("box-shadow", "none");
    $(".rinput_class").prop('checked', false);
    toastr.info('New record opened', 'Success!', {timeOut: 5000})
}