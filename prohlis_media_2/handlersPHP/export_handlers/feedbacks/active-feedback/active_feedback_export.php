<?php

require '../../../../resources/frameworks/phpspreadsheet/vendor/autoload.php';
require '../../../../resources/dbconnection.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
$objPHPExcel = $reader->load("../../../../resources/excel-templates/Active_feedback.xlsx");
$objPHPExcel->setActiveSheetIndex(0);
$sheet = $objPHPExcel->getActiveSheet();

if (trim($_POST['active_feedback_id']) == '') {
    $active_feedback_id = '-';
} else {
    $active_feedback_id = trim($_POST['active_feedback_id']);
}

$query = mysqli_query($dbc, "SELECT * FROM active_feedback_record WHERE active_feedback_id='$active_feedback_id'");

while($field_item = mysqli_fetch_array($query)){
    $sheet->SetCellValue('C4', $field_item['angebot']);
    $sheet->SetCellValue('C6', $field_item['datum']);
    $datum = $field_item['datum'];
    $sheet->SetCellValue('C8', $field_item['mitarbeiter']);
    $sheet->SetCellValue('C10', $field_item['an_wen']);
    $sheet->SetCellValue('C12', $field_item['art_des_feedback']);
    $sheet->SetCellValue('B15', $field_item['inhalt_des_feedback']);
    $sheet->SetCellValue('B23', $field_item['weiterbearbeitung']);
}

$writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);

ob_start();
$writer->save('php://output');
$xlsData = ob_get_contents();
ob_end_clean();
$response =  array(
    'op' => 'ok',
    'datum' => $datum,
    'file' => "data:application/vnd.ms-excel;base64,".base64_encode($xlsData)
);
die(json_encode($response));

?>