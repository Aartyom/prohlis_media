/**
 * Platz delete function
 */
$('.js_btn_delete_platz').click(function(){
	var platz = $('#platz').val();
	var db_table = "platz";
	var db_field = "platz_name";
	if (platz == '' || platz == null || platz == 'defaultv' || platz == '-') {
		toastr.warning('Please select platz that you would like to remove', 'Warning!', {timeOut: 5000})
		return false;
	}
		$.ajax ({
			url: '../../../handlersPHP/subform_handlers/deletion_handlers/delete_item.php',
			type: 'POST',
			cache: false,
			data: {"db_table":db_table, "db_field":db_field, 'item_name':platz},
			dataType: 'html',
			success: function(data){
				if (data == "RECORD DELETED") {
					toastr.success('Platz has been removed', 'Success', {timeOut: 5000})
					$('#platz option[value="'+platz+'"]').remove()
					$('#platz').val('defaultv');
					$('#platz').css("box-shadow", "none");
					
				} else {
					toastr.error(data, 'Error', {timeOut: 5000})
				}
			}
		})
});

/**
 * Project delete function
 */
$('.js_btn_delete_project').click(function(){
    var project_name = $('#project_name').val();
	if (project_name == '' || project_name == null || project_name == 'defaultv' || project_name == '-') {
		toastr.warning('Please select the project that you would like to remove', 'Warning', {timeOut: 5000})
		return false;
	}
	$.ajax ({
		url: '../../../handlersPHP/subform_handlers/deletion_handlers/delete_project.php',
		type: 'POST',
		cache: false,
		data: {'project_name':project_name, 'type_of_work':type_of_work},
		dataType: 'html',
		success: function(data){
			if (data == "RECORD DELETED") {
				toastr.success('Project has been removed', 'Success!', {timeOut: 5000})
				$('#project_name option[value="'+project_name+'"]').remove()
				$('#project_name').val('defaultv');
				$('#project_name').css("box-shadow", "none");
			} else {
				toastr.error(data, 'Error!', {timeOut: 5000})
			}
		}
	})
});

/**
 * Wetter delete function
 */
$('.js_btn_delete_wetter').click(function(){
    var wetter = $('#wetter').val();
    var db_table = "wetter";
    var db_field = "wetter_type";
	if (wetter == '' || wetter == null || wetter == 'defaultv' || wetter == '-') {
		toastr.warning('Please select wetter type that you would like to remove', 'Warning!', {timeOut: 5000})
		return false;
	}
    $.ajax ({
        url: '../../../handlersPHP/subform_handlers/deletion_handlers/delete_item.php',
        type: 'POST',
        cache: false,
        data: {"db_table":db_table, "db_field":db_field, 'item_name':wetter},
        dataType: 'html',
        success: function(data){
            if (data == "RECORD DELETED") {
                toastr.success('Wetter type has been removed', 'Success!', {timeOut: 5000})
                $('#wetter option[value="'+wetter+'"]').remove()
				$('#wetter').val('defaultv');
				$('#wetter').css("box-shadow", "none");
            } else {
                toastr.error(data, 'Error!', {timeOut: 5000})
            }
        }
    })
});

/**
 * Angebot delete function
 */
$('.js_btn_delete_angebot').click(function(){
    var angebot = $('#angebot').val();
    var db_table = "angebot";
    var db_field = "angebot_name";
	if (angebot == '' || angebot == null || angebot == 'defaultv' || angebot == '-') {
		toastr.warning('Please select angebot type that you would like to remove', 'Warning!', {timeOut: 5000})
		return false;
	}
    $.ajax ({
        url: '../../../handlersPHP/subform_handlers/deletion_handlers/delete_item.php',
        type: 'POST',
        cache: false,
        data: {"db_table":db_table, "db_field":db_field, 'item_name':angebot},
        dataType: 'html',
        success: function(data){
            if (data == "RECORD DELETED") {
                toastr.success('Angebot type has been removed', 'Success!', {timeOut: 5000})
				$('#angebot option[value="'+angebot+'"]').remove();
				$('#angebot').val('defaultv');
				$('#angebot').css("box-shadow", "none");
            } else {
                toastr.error(data, 'Error!', {timeOut: 5000})
            }
        }
    })
});

/**
 * Aktion delete function
 */
$('.js_btn_delete_aktion').click(function(){
    var aktion = $('#aktion').val();
    var db_table = "aktion";
    var db_field = "aktion_type";
	if (aktion == '' || aktion == null || aktion == 'defaultv' || aktion == '-') {
		toastr.warning('Please select aktion type that you would like to remove', 'Warning!', {timeOut: 5000})
		return false;
	}
    $.ajax ({
        url: '../../../handlersPHP/subform_handlers/deletion_handlers/delete_item.php',
        type: 'POST',
        cache: false,
        data: {"db_table":db_table, "db_field":db_field, 'item_name':aktion},
        dataType: 'html',
        success: function(data){
            if (data == "RECORD DELETED") {
				toastr.success('Aktion type has been removed', 'Success!', {timeOut: 5000})
				$('#aktion option:selected').remove();
				$('#aktion').val('defaultv');
				$('#aktion').css("box-shadow", "none");
            } else {
                toastr.error(data, 'Error!', {timeOut: 5000})
            }
        }
    })
});

/**
 * Art des feedback delete function
 */
$('.js_btn_delete_art_des_feedback').click(function(){
    var art_des_feedback = $('#art_des_feedback').val();
    var db_table = "art_des_feedback";
    var db_field = "art_des_feedback_type";
	if (art_des_feedback == '' || art_des_feedback == null || art_des_feedback == 'defaultv' || art_des_feedback == '-') {
		toastr.warning('Please select art des feedback type that you would like to remove', 'Warning!', {timeOut: 5000})
		return false;
	}
    $.ajax ({
        url: '../../../handlersPHP/subform_handlers/deletion_handlers/delete_item.php',
        type: 'POST',
        cache: false,
        data: {"db_table":db_table, "db_field":db_field, 'item_name':art_des_feedback},
        dataType: 'html',
        success: function(data){
            if (data == "RECORD DELETED") {
                toastr.success('Art des feedback type has been removed', 'Success!', {timeOut: 5000})
                $('#art_des_feedback option[value="'+art_des_feedback+'"]').remove()
				$('#art_des_feedback').val('defaultv');
				$('#art_des_feedback').css("box-shadow", "none");
            } else {
                toastr.error(data, 'Error!', {timeOut: 5000})
            }
        }
    })
});


/**
 * Ausfallgrund delete function
 */
$('.js_btn_delete_ausfallgrund').click(function(){
    var ausfallgrund = $('#ausfallgrund').val();
    var db_table = "ausfallgrund";
    var db_field = "ausfallgrund_type";
	if (ausfallgrund == '' || ausfallgrund == null || ausfallgrund == 'defaultv' || ausfallgrund == '-') {
		toastr.warning('Please select ausfallgrund type that you would like to remove', 'Warning!', {timeOut: 5000})
		return false;
	}
    $.ajax ({
        url: '../../../handlersPHP/subform_handlers/deletion_handlers/delete_item.php',
        type: 'POST',
        cache: false,
        data: {"db_table":db_table, "db_field":db_field, 'item_name':ausfallgrund},
        dataType: 'html',
        success: function(data){
            if (data == "RECORD DELETED") {
                toastr.success('Ausfallgrund type has been removed', 'Success!', {timeOut: 5000})
                $('#ausfallgrund option[value="'+ausfallgrund+'"]').remove()
				$('#ausfallgrund').val('defaultv');
				$('#ausfallgrund').css("box-shadow", "none");
                ausfallgrundFunc();
            } else {
                toastr.error(data, 'Error!', {timeOut: 5000})
            }
        }
    })
});

/**
 * Mobilitat delete function
 */
$('.js_btn_delete_mobilitat').click(function(){
	var mobilitat = $('#mobilitat').val();
    var db_table = "mobilitat";
    var db_field = "mobilitat_type";
	if (mobilitat == '' || mobilitat == null || mobilitat == 'defaultv' || mobilitat == '-') {
		toastr.warning('Please select mobilitat type that you would like to remove', 'Warning!', {timeOut: 5000})
		return false;
	}
	$.ajax ({
		url: '../../../handlersPHP/subform_handlers/deletion_handlers/delete_item.php',
		type: 'POST',
		cache: false,
		data: {"db_table":db_table, "db_field":db_field, 'item_name':mobilitat},
		dataType: 'html',
		success: function(data){
			if (data == "RECORD DELETED") {
				toastr.success('Mobilitat type has been removed', 'Success!', {timeOut: 5000})
				$('#mobilitat option[value="'+mobilitat+'"]').remove()
				$('#mobilitat').val('defaultv');
				$('#mobilitat').css("box-shadow", "none");
			} else {
				toastr.error(data, 'Error!', {timeOut: 5000})
			}
		}
	})
});

/**
 * Stadtteil delete function
 */
$('.js_btn_delete_stadtteil').click(function(){
    var stadtteil_name = $('#stadtteil_name').val();
    var db_table = "stadtteil";
    var db_field = "stadtteil_name";
	if (stadtteil_name == '' || stadtteil_name == null || stadtteil_name == 'defaultv' || stadtteil_name == '-') {
		toastr.warning('Please select stadtteil name that you would like to remove', 'Warning!', {timeOut: 5000})
		return false;
	}
	$.ajax ({
		url: '../../../handlersPHP/subform_handlers/deletion_handlers/delete_item.php',
		type: 'POST',
		cache: false,
		data: {"db_table":db_table, "db_field":db_field, 'item_name':stadtteil_name},
		dataType: 'html',
		success: function(data){
			if (data == "RECORD DELETED") {
				toastr.success('Stadtteil name has been removed', 'Success!', {timeOut: 5000})
				$('#stadtteil_name option[value="'+stadtteil_name+'"]').remove()
				$('#stadtteil_name').val('defaultv');
				$('#stadtteil_name').css("box-shadow", "none");
			} else {
				toastr.error(data, 'Error!', {timeOut: 5000})
			}
		}
	})
});

/**
 * Mitarbeiter delete function
 */
$('.js_btn_delete_mitarbeiter').click(function(){
	var mitarbeiter = $('#mitarbeiter02').val();
	var db_table = "user";
    var db_field = "short_name";
	if (mitarbeiter == '' || mitarbeiter == null || mitarbeiter == 'defaultv' || mitarbeiter == '-') {
		toastr.warning('Please select mitarbeiter that you would like to remove', 'Warning!', {timeOut: 5000})
		return false;
	}
		$.ajax ({
			url: '../../../handlersPHP/subform_handlers/deletion_handlers/delete_item.php',
			type: 'POST',
			cache: false,
			data: {"db_table":db_table, "db_field":db_field, 'item_name':mitarbeiter},
			dataType: 'html',
			success: function(data){
				if (data == "RECORD DELETED") {
					toastr.success('Mitarbeiter has been removed', 'Success!', {timeOut: 5000})
					$('#mitarbeiter02 option[value="'+mitarbeiter+'"]').remove();
					$('#mitarbeiter03 option[value="'+mitarbeiter+'"]').remove();
					$('#mitarbeiter02').val('defaultv');
					$('#mitarbeiter02').css("box-shadow", "none");
				} else {
					toastr.error(data, 'Error!', {timeOut: 5000})
				}
			}
		})
});