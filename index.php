<!DOCTYPE html>
<html>
<head>
<title>Prohlis Media</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<link rel="stylesheet" type="text/css" href="index.css">
<link href="prohlis_media_2/resources/frameworks/toastr-master/build/toastr.min.css" rel="stylesheet">
<script src="prohlis_media_2/resources/frameworks/assets4/js/jquery.min.js"></script>
<script type="text/javascript" src="index.js"></script>
</head>
<body>

  <div class="bgimg w3-display-container w3-animate-opacity w3-text-white main">
    <!-- <div class="w3-display-topleft w3-padding-large w3-xlarge">
      ADNAVALinc.
    </div> -->

    <div class="w3-display-middle">
      <h1 class="w3-jumbo w3-animate-top" style="color:rgba(0, 0, 0, 0.67);">PROHLIS MEDIA</h1>
      <hr class="w3-border-grey" style="margin:auto;width:40%; color: #6b6b6b">

      <p class="w3-large w3-center">
        <button id="start_btn" class="main_button">START</button>
        <!-- <button id="show_popup" class="main_button js-open-button-popup">Show popup</button> -->
      </p>
    </div>

      <!-- <div class="w3-display-bottomleft w3-padding-large">
        Powered by <a href="https://www.w3schools.com/w3css/default.asp" target="_blank">Artem Alektorov</a>
    </div> -->
  </div>

	<!-- Authorisation form -->
	<div class="subform_container auth_popup_overlay">
		<div class="auth_popup_content" style="padding-top: 35px;">
      <h1 style="margin-bottom: 25px;">AUTHORISATION</h1>
			<!-- <img src="../1123.jpg"> -->
				<!-- <div class="message_show" style="color: red"></div> -->
				<form action="<?php echo $_SERVER['PHP_SELF'];?>" method="">

				<div class="dws-input">
					<input type="text" id="login" name="login" placeholder="login" value="<?php echo @$_POST['login'];?>">
				</div>
				<div class="dws-input">
					<input type="password" id="password" name="password" placeholder="password">
				</div>
				<div class="dws-input">

          			<center><div id="log_in" class="dws-submit" name="log_in"><span>ENTER</span></div></center>
					<!-- <button class="dws-submit" id="log_in" name="log_in">Enter</button> -->

          <!-- <div class="main_button js-close-button-popup"><span>Hide popup</span></div> -->

         <!--  <div class="main_button js-delete-cookies"><span>TEST DELETE COOKIES</span></div> -->
				</div>
				<div class="dws-input">
					<a href="#" class="main_button">Restore password</a>
				</div>
        <div id="errorShow"></div>

			</form>
		</div>
	</div>

<script src="prohlis_media_2/resources/frameworks/assets4/js/jquery.min.js"></script>
<script type="text/javascript" src="prohlis_media_2/resources/frameworks/toastr-master/build/toastr.min.js"></script>
<script type="text/javascript" src="index.js"></script>

</body>
</html>
