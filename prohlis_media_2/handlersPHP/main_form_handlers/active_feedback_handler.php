<?php

require('../../resources/dbconnection.php');

	$err = array();
	$response = array();

	if (trim($_POST['angebot']) == '') {
		$err[] = 'Write angebot!';
	} else {
		$angebot = trim($_POST['angebot']);
	}
	
	if (trim($_POST['datum']) == '') {
		$err[] = 'Write datum!';
	} else {
		$datum = trim($_POST['datum']);
	}
	
	if (trim($_POST['mitarbeiter']) == '') {
		$err[] = 'Write mitarbeiter!';
	} else {
		$mitarbeiter = trim($_POST['mitarbeiter']);
	}
	
	if (trim($_POST['an_wen']) == '') {
		$err[] = 'Write an_wen!';
	} else {
		$an_wen = trim($_POST['an_wen']);
	}

	if (trim($_POST['art_des_feedback']) == '') {
		$err[] = 'Write art_des_feedback!';
	} else {
		$art_des_feedback = trim($_POST['art_des_feedback']);
	}

	if (trim($_POST['inhalt_des_feedback']) == '') {
		$err[] = 'Write inhalt_des_feedback!';
	} else {
		$inhalt_des_feedback = trim($_POST['inhalt_des_feedback']);
	}

	if (trim($_POST['weiterbearbeitung']) == '') {
		$err[] = 'Write weiterbearbeitung!';
	} else {
		$weiterbearbeitung = trim($_POST['weiterbearbeitung']);
	}

	 if (empty($err)){
		
		$query = "INSERT INTO active_feedback_record (angebot, datum, mitarbeiter, an_wen, art_des_feedback, inhalt_des_feedback, weiterbearbeitung) VALUES ('$angebot', '$datum', '$mitarbeiter', '$an_wen', '$art_des_feedback', '$inhalt_des_feedback', '$weiterbearbeitung')";
		mysqli_query($dbc,$query);
		$affected_rows = mysqli_affected_rows($dbc);
		
		if ($affected_rows == 1) {
			
			$response['message'] = "Record added";

		} else {

			$response['message'] = 'Error with request execution';
		}

		mysqli_close($dbc);
		echo json_encode($response);
		
	} else {

	 	echo 'Data missing'+$err;
	 }
?>
