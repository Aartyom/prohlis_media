<?php

	require_once('../../../resources/dbconnection.php');

	$err = array();

	if (trim($_POST['project_name']) == '') {
		$err[] = 'project_name';
	} else {
		$project_name = trim($_POST['project_name']);
	}

	if (trim($_POST['type_of_work']) == '') {
		$err[] = 'type_of_work';
	} else {
		$type_of_work = trim($_POST['type_of_work']);
	}

	 if (empty($err)){

		$query = "DELETE FROM project WHERE project_name='$project_name' AND type_of_work='$type_of_work'";
		
		mysqli_query($dbc,$query);

		$affected_rows = mysqli_affected_rows($dbc);
		
		if ($affected_rows == 1) {

			echo "RECORD DELETED";

		} else {

			echo 'Error with request execution this project may be not exists';
		}

		mysqli_close($dbc);
		
	} else {

	 	echo 'Data missing'+$err;
	 }
?>