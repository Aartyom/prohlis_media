var type_of_work = 'active_feedback';
var db_table_revert = 'active_feedback_record';
var db_field_revert = 'active_feedback_id'; 
 
Date.prototype.yyyymmdd = function() { 
   var yyyy = this.getFullYear(); 
   var mm = this.getMonth() < 9 ? "0" + (this.getMonth() + 1) : (this.getMonth() + 1); // getMonth() is zero-based 
   var dd  = this.getDate() < 10 ? "0" + this.getDate() : this.getDate(); 
   return "".concat(yyyy).concat("-").concat(mm).concat("-").concat(dd); 
  }; 
 
 
//------------------------------------------------------------------ 
 
function submitRecordFunc(){
 
    var angebot = $('#angebot').val(); 
    var datum_nf = $("#datum").val(); 
    if(datum_nf != '') { 
        var patterndatum = /[0-9]{2}.[0-9]{2}.[0-9]{4}/i; 
        if (patterndatum.test(datum_nf)) { 
            var datum_nf = $('#datum').val().split("."); 
        }else{ 
            $("#datum").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)"); 
            toastr.error('Date format is incorrect (dd.mm.yyyy)', 'Error!', { 
            timeOut: 5000 
            }) 
            return false; 
        } 
    }else{ 
        fail = 'Fill the "Datum" field' 
        toastr.error(fail, 'Error!', { 
        timeOut: 5000 
        }) 
        $("#datum").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)"); 
        return false; 
    }; 
    var datum = new Date(datum_nf[2], datum_nf[1] - 1, datum_nf[0]).yyyymmdd(); 
    var mitarbeiter = $('#mitarbeiter02').val(); 
    var an_wen = $('#an_wen').val(); 
    var art_des_feedback = $('#art_des_feedback').val(); 
    var inhalt_des_feedback = $('#inhalt_des_feedback').val(); 
    var weiterbearbeitung = $('#weiterbearbeitung').val(); 
    var fail = ''; 
 
  if (angebot == null || datum == null || mitarbeiter == null || an_wen  == null || art_des_feedback == null || inhalt_des_feedback == null || weiterbearbeitung == null || 
        angebot == '' || datum == '' || mitarbeiter == '' || an_wen  == '' || art_des_feedback == '' || inhalt_des_feedback == '' || weiterbearbeitung == '') { 
    fail = 'Fill in all required fields'; 
  } 
 
    if (fail != "") { 
        if (angebot == null || angebot == "") { 
            $(".angebot_red_line").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)"); 
        } 
 
        if (mitarbeiter == null || mitarbeiter == "") { 
            $(".mitarbeiter02_red_line").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)"); 
        } 
        if (datum == "" || datum == null) { 
            $("#datum").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)"); 
        } 
 
        if (an_wen == null || an_wen == "") { 
            $("#an_wen").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)"); 
        } 
 
        if (art_des_feedback == null || art_des_feedback == "") { 
            $(".art_des_feedback_red_line").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)"); 
        } 
 
        if (inhalt_des_feedback == null || inhalt_des_feedback == "") { 
            $("#inhalt_des_feedback").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)"); 
        } 

        if (weiterbearbeitung == null || weiterbearbeitung == "") { 
            $("#weiterbearbeitung").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)"); 
        } 
 
        toastr.error(fail, 'Error!', { 
        timeOut: 5000 
        }) 
        return false; 
    }else{ 
 
        $.ajax ({ 
            url: '../../../handlersPHP/main_form_handlers/active_feedback_handler.php', 
            type: 'POST', 
            cache: false, 
            data: {'angebot':angebot, 'datum':datum, 'mitarbeiter':mitarbeiter, 'an_wen':an_wen, 'art_des_feedback':art_des_feedback, 
            'inhalt_des_feedback':inhalt_des_feedback, 'weiterbearbeitung':weiterbearbeitung}, 
            dataType: 'html', 
            success: function(data){ 
                var obj = JSON.parse(data);
                if (obj.message == "Record added"){
                    toastr.success('Record successfully added!', 'Success!', {
                    timeOut: 5000,
                    })
                } else {
                    toastr.error(data, 'Error!', {
                    timeOut: 5000
                    })
                }
            }
        })
    }
}
$('.js_main_form_download_record_button').click(function(){
    var angebot = $('#angebot').val(); 
    var datum_nf = $("#datum").val(); 
    if(datum_nf != '') { 
        var patterndatum = /[0-9]{2}.[0-9]{2}.[0-9]{4}/i; 
        if (patterndatum.test(datum_nf)) { 
            var datum_nf = $('#datum').val().split("."); 
        }else{ 
            $("#datum").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)"); 
            toastr.error('Date format is incorrect (dd.mm.yyyy)', 'Error!', { 
            timeOut: 5000 
            }) 
            return false; 
        } 
    }else{ 
        fail = 'Fill the "Datum" field' 
        toastr.error(fail, 'Error!', { 
        timeOut: 5000 
        }) 
        $("#datum").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)"); 
        return false; 
    }; 
    var datum = new Date(datum_nf[2], datum_nf[1] - 1, datum_nf[0]).yyyymmdd(); 
    var mitarbeiter = $('#mitarbeiter02').val(); 
    var an_wen = $('#an_wen').val(); 
    var art_des_feedback = $('#art_des_feedback').val(); 
    var inhalt_des_feedback = $('#inhalt_des_feedback').val(); 
    var weiterbearbeitung = $('#weiterbearbeitung').val(); 
    var fail = ''; 
 
  if (angebot == null || datum == null || mitarbeiter == null || an_wen  == null || art_des_feedback == null || inhalt_des_feedback == null || weiterbearbeitung == null || angebot == '' || datum == '' || mitarbeiter == '' || an_wen  == '' || art_des_feedback == '' || inhalt_des_feedback == '' || weiterbearbeitung == '') { 
    fail = 'Fill in all required fields'; 
  } 
 
    if (fail != "") { 
        if (angebot == null || angebot == "") { 
            $(".angebot_red_line").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)"); 
        } 
 
        if (mitarbeiter == null || mitarbeiter == "") { 
            $(".mitarbeiter02_red_line").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)"); 
        } 
        if (datum == "" || datum == null) { 
            $("#datum").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)"); 
        } 
 
        if (an_wen == null || an_wen == "") { 
            $("#an_wen").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)"); 
        } 
 
        if (art_des_feedback == null || art_des_feedback == "") { 
            $(".art_des_feedback_red_line").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)"); 
        } 
 
        if (inhalt_des_feedback == null || inhalt_des_feedback == "") { 
            $("#inhalt_des_feedback").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)"); 
        } 

        if (weiterbearbeitung == null || weiterbearbeitung == "") { 
            $("#weiterbearbeitung").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)"); 
        } 
 
        toastr.error(fail, 'Error!', { 
        timeOut: 5000 
        }) 
        return false; 
    }else{ 
        // $(".container-input-form").fadeOut("fast");
        // setTimeout(function(){$(".container-loader").fadeIn("slow");},300);

        $.ajax({
            type:'POST',
            url:"../../../handlersPHP/export_handlers/feedbacks/active-feedback/active_feedback_print.php",
            data: {'angebot':angebot, 'datum':datum, 'mitarbeiter':mitarbeiter, 'an_wen':an_wen, 'art_des_feedback':art_des_feedback, 'inhalt_des_feedback':inhalt_des_feedback, 'weiterbearbeitung':weiterbearbeitung},
            dataType:'json'
        }).done(function(data){
            var $a = $("<a>");
            $a.attr("href",data.file);
            $("body").append($a);
            $a.attr("download","Active_feedback.xlsx");
            $a[0].click();
            $a.remove();
            // $(".container-loader").fadeOut("slow");
            // setTimeout(function(){$(".container-input-form").fadeIn("slow");},300);
            toastr.info('Report generated and downloaded', {timeOut: 5000})
        });
    }
});
//--------------------------------------------------------------------

function newRecord(){
    $("#angebot").val('defaultv').css("box-shadow", "none");
    $(".angebot_red_line").css("box-shadow", "none");
	$("#datum").val('').css("box-shadow", "none");
    $("#an_wen").val('').css("box-shadow", "none");
    $("#mitarbeiter02").val('defaultv').css("box-shadow", "none");
    $(".mitarbeiter02_red_line").css("box-shadow", "none");
    $("#art_des_feedback").val('defaultv').css("box-shadow", "none");
    $(".art_des_feedback_red_line").css("box-shadow", "none");
	$("#inhalt_des_feedback").val('').css("box-shadow", "none");
	$("#weiterbearbeitung").val('').css("box-shadow", "none");
    toastr.info('New record opened', 'Success!', {timeOut: 5000})
}