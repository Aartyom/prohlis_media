FROM php:7.1-fpm

RUN apt update && apt install libpng-dev --yes \
        libxml2-dev --yes

RUN docker-php-ext-install gd
RUN docker-php-ext-install zip
RUN docker-php-ext-install xml

RUN docker-php-ext-install mysqli gd

# Clean up afterwards
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
