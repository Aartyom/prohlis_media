<?php

	require_once('../../../resources/dbconnection.php');

	$err = array();

	if (trim($_POST['platz_name']) == '') {
		$err[] = 'platz_name';
	} else {
		$platz_name = trim($_POST['platz_name']);
	}

	if (trim($_POST['stadtteil_name']) == '') {
		$err[] = 'stadtteil_name';
	} else {
		$stadtteil_name = trim($_POST['stadtteil_name']);
	}

	 if (empty($err)){
	 	// go ahead
		$query = "INSERT INTO platz (platz_name, stadtteil_name) VALUES ('$platz_name', '$stadtteil_name')";

		mysqli_query($dbc,$query);

		$affected_rows = mysqli_affected_rows($dbc);

		if ($affected_rows == 1) {

			echo "RECORD ADDED";

		} else {

			echo 'Error with request execution';
		}

		mysqli_close($dbc);

	} else {

	 	echo 'Data missing';
	 }
?>
