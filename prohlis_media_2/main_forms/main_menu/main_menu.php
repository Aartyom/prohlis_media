<?php
require('../../resources/dbconnection.php');
$short_name_query = mysqli_query($dbc, 'SELECT * FROM user');
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Main Menu</title>
        <!-- Bootstrap core CSS -->
        <link href="../../resources/frameworks/bootstrap4/css/bootstrap.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="main-menu-stylesheet.css" rel="stylesheet">
        <link href="../../resources/frameworks/toastr-master/build/toastr.min.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
        <!-- <script type="text/javascript" src="../resources/frameworks/toastr-master/build/toastr.min.js"></script> -->
</head>
<body>

<div class="py-5 text-center opaque-overlay">
    <div class="container py-5">
        <div class="row row-current-user d-flex justify-content-end mb-5">
            <div class="col-md-4">
                <h4 id="current_user_username">
                    <?php
                        $username = $_COOKIE['username'];
                        while ($current_user = mysqli_fetch_array($short_name_query)) {
                            if ($current_user['username'] == $username){
                                echo $current_user['first_name']." ".$current_user['last_name'];
                            }
                        }
                    ?>
                </h4>
                <hr id="current_user_hr">
                <p style="margin-top: 10px;"><a href="../../../../logout.php">Logout</a></p>
            </div>
        </div>
      <div class="row">
        <div class="col-md-12 text-black">
            <h1 class="display-5 mb-5">MAIN MENU</h1>
            <div class="btn-group">
                <button class="btn btn-sm btn-outline-primary dropdown-toggle" data-toggle="dropdown"> Streetwork </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="../input_forms/streetwork/streetwork-form.php">Add new streetwork record</a>
                    <a class="dropdown-item" href="../overall_tables/streetwork_overall/streetwork_overall.php">Streetwork records list</a>
                </div>
            </div>
            <div class="btn-group">
                <button class="btn btn-sm btn-outline-primary dropdown-toggle" data-toggle="dropdown"> Gemeinwesenarbeit </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="../input_forms/gemeinwesenarbeit/gemeinwesenarbeit-form.php">Add new gemeinwesenarbeit record</a>
                    <a class="dropdown-item" href="../overall_tables/gemeinwesenarbeit_overall/gemeinwesenarbeit_overall.php">Gemeinwesenarbeit records list</a>
                </div>
            </div>
            <div class="btn-group">
                <button class="btn btn-sm btn-outline-primary dropdown-toggle" data-toggle="dropdown"> Einzelfallhilfe </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="../input_forms/einzelfallhilfe/einzelfallhilfe-form.php">Add new einzelfallhilfe record</a>
                    <a class="dropdown-item" href="../overall_tables/einzelfallhilfe_overall/einzelfallhilfe_overall.php">Einzelfallhilfe records list</a>
                </div>
            </div>
            <div class="btn-group">
                <button class="btn btn-sm btn-outline-primary dropdown-toggle" data-toggle="dropdown"> Gruppenarbeit </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="../input_forms/gruppenarbeit/gruppenarbeit-form.php">Add new gruppenarbeit record</a>
                    <a class="dropdown-item" href="../overall_tables/gruppenarbeit_overall/gruppenarbeit_overall.php">Gruppenarbeit records list</a>
                </div>
            </div>
            <div class="btn-group">
                <button class="btn btn-sm btn-outline-primary dropdown-toggle" data-toggle="dropdown"> Feedbacks </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="../input_forms/active_feedback/active_feedback_form.php">Add new active feedback</a>
                    <a class="dropdown-item" href="../overall_tables/active_feedback_overall/active_feedback_overall.php">Get active feedback records</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="../input_forms/passive_feedback/passive_feedback_form.php">Add new passive feedback</a>
                    <a class="dropdown-item" href="../overall_tables/passive_feedback_overall/passive_feedback_overall.php">Get passive feedback records</a>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
<footer class="footer">
    <div class="container display-6">
    <span class="text-muted">System designed and developed by <span style="font-weight: bold;">Artem Alektorov</span> for <span style="font-weight: bold;">Mobile Jugendarbeit Dresden-Süd e.V.</span><br>WhatsApp & Telegram: +4915774036457; e-mail: don.adnaval@yahoo.com</span>
    </div>
</footer>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="../../resources/frameworks/assets4/js/jquery.min.js"></script>
<script src="../../resources/frameworks/assets4/js/popper.js"></script>
<script src="../../resources/frameworks/bootstrap4/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="../../resources/frameworks/assets4/js/ie10-viewport-bug-workaround.js"></script>
<!-- Notifications framework "Toastr" -->
<script type="text/javascript" src="../../resources/frameworks/toastr-master/build/toastr.min.js"></script>
<!-- Custom scripts -->
<script src="../../helpersJS/subforms_add_listeners_js.js"></script>
<script src="../../helpersJS/subforms_remove_listeners_js.js"></script>
<script src="../../helpersJS/common_js_functions.js"></script>
<script src="main-menuJS.js"></script>
</body>
</html>