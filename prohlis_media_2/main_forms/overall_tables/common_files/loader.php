<?php
require('../dbconnection.php');

$dlquery = "SELECT * FROM user";

$user_short_name1 = mysqli_query($dbc, $dlquery);
$user_short_name2 = mysqli_query($dbc, $dlquery);
$user_short_name3 = mysqli_query($dbc, $dlquery);

$platz_query = mysqli_query($dbc, "SELECT platz_id, platz_name FROM platz");
$wetter_query = mysqli_query($dbc, "SELECT wetter_id, wetter_type FROM wetter");
$ausfallgrund_query = mysqli_query($dbc, "SELECT * FROM ausfallgrund");
$mobilitat_query = mysqli_query($dbc, "SELECT * FROM mobilitat");
$project_query = mysqli_query($dbc, "SELECT * FROM project WHERE type_of_work = 'streetwork'");
$short_name_query = mysqli_query($dbc, 'SELECT * FROM user');

$mitarbeiter1_query = mysqli_query($dbc, 'SELECT * FROM user');
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Starter Template for Bootstrap</title>
        <!-- Bootstrap core CSS -->
        <link href="../bootstrap4/css/bootstrap.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link rel="stylesheet" href="loader-stylesheet.css">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    </head>
    <body>
      <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
          <a class="navbar-brand" href="#">PROHLIS MEDIA</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarsExampleDefault">
              <ul class="navbar-nav ml-auto">
                  <li class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle" id="current_user_username" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?php
          								$username = $_COOKIE['username'];
          								while ($current_user = mysqli_fetch_array($mitarbeiter1_query)) {
          								 	if ($current_user['username'] == $username){
          								 	 	echo $current_user['first_name']." ".$current_user['last_name'];
          								 	 }
          								}
        								?></a>
                      <div class="dropdown-menu" aria-labelledby="dropdown01">
                          <a class="dropdown-item" href="#">Edit settings</a>
                          <a class="dropdown-item" href="logout.php">Log out</a>
                      </div>
                  </li>
              </ul>
            </div>
      </nav>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h5 class="text-center mt-5 mb-5">Please do not close this window <br> until the file will be loaded..</h5>
                    <div class="middle">
                        <div class="sk-folding-cube">
                            <div class="sk-cube1 sk-cube"></div>
                            <div class="sk-cube2 sk-cube"></div>
                            <div class="sk-cube4 sk-cube"></div>
                            <div class="sk-cube3 sk-cube"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- /.container -->
        <!-- Bootstrap core JavaScript
    ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="../assets4/js/jquery.min.js"></script>
        <script src="../assets4/js/popper.js"></script>
        <script src="../bootstrap4/js/bootstrap.min.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="../assets4/js/ie10-viewport-bug-workaround.js"></script>

        <?php

        // require '../thirdparty_frameworks/PHPExcel/Classes/PHPExcel.php';
        // require '../thirdparty_frameworks/PHPExcel/Classes/PHPExcel/IOFactory.php';
        // //require '../dbconnection.php';
        //
        // $objPHPExcel = PHPExcel_IOFactory::load("../Statistik_17_mobile_JA.xlsx");
        //
        // $sheetIndex = 2;
        //
        // date_default_timezone_set('UTC');
        //
        //
        // $year_part = '2017-0';
        // $begining_day = '-01';
        // $end_day = '-31';
        //
        //
        //
        // for ($j=1; $j <= 12; $j++) {
        //
        // 	$objPHPExcel->setActiveSheetIndex($sheetIndex);
        // 	//echo '<p style="color:red">Sheet Index is: '.$sheetIndex.'</p>';
        // 	$row = 4;
        //
        // 	if ($j==1 || $j==3 || $j==5 || $j==7 || $j==8 || $j==10 || $j==12) {
        //
        // 		$end_day = '-31';
        //
        // 		if ($j >= 9) {
        // 			$year_part = '2017-';
        // 		}
        // 	}
        //
        //  	if ($j >= 10) {
        // 		$year_part = '2017-';
        // 	}
        //
        // 	if ($j==4 || $j==6 || $j==9 || $j==11) {
        //
        // 		$end_day = '-30';
        //
        // 		if ($j >= 10) {
        // 			$year_part = '2017-';
        // 		}
        //
        // 	}
        //
        // 	if ($j==2){
        // 		$end_day = '-28';
        // 	}
        //
        // 	$date = $year_part.$j.$begining_day;
        // 	$end_date = $year_part.$j.$end_day;
        //
        // 	//echo $date."</br>";
        // 	//echo $end_date."</br></br>";
        //
        //
        // 	while (strtotime($date) <= strtotime($end_date)) {
        //
        // 		$zx = 0;
        // 		$zc = 0;
        // 		$zv = 0;
        // 		$zb = 0;
        // 		$zn = 0;
        // 		$zm = 0;
        //
        // 		$all_works_record_query = mysqli_query($dbc, "SELECT datum, n_u12_m, n_u12_w, n_u14_m, n_u14_w, n_u14_17_m, n_u14_17_w, n_u18_20_m, n_u18_20_w, n_u21_24_m, n_u21_24_w, n_u25_26_m, n_u25_26_w, n_o27_m, n_o27_w FROM streetwork_record UNION ALL SELECT datum, n_u12_m, n_u12_w, n_u14_m, n_u14_w, n_u14_17_m, n_u14_17_w, n_u18_20_m, n_u18_20_w, n_u21_24_m, n_u21_24_w, n_u25_26_m, n_u25_26_w, n_o27_m, n_o27_w FROM gruppenarbeit_record UNION ALL SELECT datum, n_u12_m, n_u12_w, n_u14_m, n_u14_w, n_u14_17_m, n_u14_17_w, n_u18_20_m, n_u18_20_w, n_u21_24_m, n_u21_24_w, n_u25_26_m, n_u25_26_w, n_o27_m, n_o27_w FROM einzelfallhilfe_record UNION ALL SELECT datum, n_u12_m, n_u12_w, n_u14_m, n_u14_w, n_u14_17_m, n_u14_17_w, n_u18_20_m, n_u18_20_w, n_u21_24_m, n_u21_24_w, n_u25_26_m, n_u25_26_w, n_o27_m, n_o27_w FROM gemeinwesenarbeit_record");
        //
        // 		while ($all_works_record_array = mysqli_fetch_array($all_works_record_query)) {
        //
        // 			if ($all_works_record_array['datum']==$date) {
        // 					//echo $gruppenarbeit_record_array['datum'];
        // 					$n_u12_all = 0;
        // 				 	$n_u12_all = $all_works_record_array['n_u12_m']+$all_works_record_array['n_u12_w'];
        // 				 	$zx = $zx + $n_u12_all;
        // 				 	//echo "<p style='color:red;'>".$date."</p>";
        // 				 	//echo " ".$zx."</br>";
        // 			}
        //
        // 			if ($all_works_record_array['datum']==$date) {
        // 					//echo $gruppenarbeit_record_array['datum'];
        // 					$n_u14_17_all = 0;
        // 				 	$n_u14_17_all = $all_works_record_array['n_u14_17_m']+$all_works_record_array['n_u14_17_w'];
        // 				 	$zc = $zc + $n_u14_17_all;
        // 				 	//echo "<p style='color:red;'>".$date."</p>";
        // 				 	//echo " ".$zx."</br>";
        // 			}
        //
        // 			if ($all_works_record_array['datum']==$date) {
        // 					//echo $gruppenarbeit_record_array['datum'];
        // 					$n_u18_20_all = 0;
        // 				 	$n_u18_20_all = $all_works_record_array['n_u18_20_m']+$all_works_record_array['n_u18_20_w'];
        // 				 	$zv = $zv + $n_u18_20_all;
        // 				 	//echo "<p style='color:red;'>".$date."</p>";
        // 				 	//echo " ".$zx."</br>";
        // 			}
        //
        // 			if ($all_works_record_array['datum']==$date) {
        // 					//echo $gruppenarbeit_record_array['datum'];
        // 					$n_u21_24_all = 0;
        // 				 	$n_u21_24_all = $all_works_record_array['n_u21_24_m']+$all_works_record_array['n_u21_24_w'];
        // 				 	$zb = $zb + $n_u21_24_all;
        // 				 	//echo "<p style='color:red;'>".$date."</p>";
        // 				 	//echo " ".$zx."</br>";
        // 			}
        //
        // 			if ($all_works_record_array['datum']==$date) {
        // 					//echo $gruppenarbeit_record_array['datum'];
        // 					$n_u25_26_all = 0;
        // 				 	$n_u25_26_all = $all_works_record_array['n_u25_26_m']+$all_works_record_array['n_u25_26_w'];
        // 				 	$zn = $zn + $n_u25_26_all;
        // 				 	//echo "<p style='color:red;'>".$date."</p>";
        // 				 	//echo " ".$zx."</br>";
        // 			}
        //
        // 			if ($all_works_record_array['datum']==$date) {
        // 					//echo $gruppenarbeit_record_array['datum'];
        // 					$n_o27_all = 0;
        // 				 	$n_o27_all = $all_works_record_array['n_o27_m']+$all_works_record_array['n_o27_w'];
        // 				 	$zm = $zm + $n_o27_all;
        // 				 	//echo "<p style='color:red;'>".$date."</p>";
        // 				 	//echo " ".$zx."</br>";
        // 			}
        //
        //
        // 		}
        //
        // 		//**** Filling the 6-13 field
        // 		$objPHPExcel->getActiveSheet()->SetCellValue('H'.$row, $zx);
        // 		//echo "sent to H".$row;
        // 		//echo "</br>".$date." ".$zx."</br>";
        //
        // 		//**** Filling the 14-17 field
        // 		$objPHPExcel->getActiveSheet()->SetCellValue('I'.$row, $zc);
        // 		// echo "sent to I".$row;
        // 		// echo "</br>".$date." ".$zc."</br>";
        //
        // 		//**** Filling the 18-20 field
        // 		$objPHPExcel->getActiveSheet()->SetCellValue('J'.$row, $zv);
        // 		// echo "sent to J".$row;
        // 		// echo "</br>".$date." ".$zv."</br>";
        //
        // 		//**** Filling the 21-24 field
        // 		$objPHPExcel->getActiveSheet()->SetCellValue('K'.$row, $zb);
        // 		// echo "sent to K".$row;
        // 		// echo "</br>".$date." ".$zb."</br>";
        //
        // 		//**** Filling the 25-26 field
        // 		$objPHPExcel->getActiveSheet()->SetCellValue('L'.$row, $zn);
        // 		// echo "sent to L".$row;
        // 		// echo "</br>".$date." ".$zn."</br>";
        //
        // 		//**** Filling the 27+ field
        // 		$objPHPExcel->getActiveSheet()->SetCellValue('M'.$row, $zm);
        // 		// echo "sent to M".$row;
        // 		// echo "</br>".$date." ".$zm."</br>";
        //
        //
        // 		// ******Streetwork*************************************************************************************************
        //
        // 			$streetwork_time_query = mysqli_query($dbc,"SELECT datum, uhrzeit FROM streetwork_record");
        //
        // 			$fquery = array();
        //
        // 			while ($streetwork_time_array = mysqli_fetch_array($streetwork_time_query)) {
        //
        // 				if ($streetwork_time_array['datum']==$date) {
        // 					$fquery[] = $streetwork_time_array['uhrzeit'];
        // 				}
        // 			}
        //
        // 			// for($m=0; $m<count($fquery); $m++){
        // 			// 	echo 'THIS IS ARRAY'.$fquery[$m];
        // 			// }
        //
        // 			if(empty($fquery)){
        //
        // 				$timediff=0;
        // 				$objPHPExcel->getActiveSheet()->SetCellValue('O'.$row, $timediff);
        // 				// echo "sent to O".$row.'timediff= '.$timediff;
        // 				// echo 'ARRAY EMPTY FOR THIS DATE</br>';
        //
        //       }else{
        //
        //         // echo 'FILLING THE ARRAY WITH DATA</br>';
        //         $maxtime = max($fquery);
        //         $mintime = min($fquery);
        //         $timediff=$maxtime-$mintime;
        //
        //         // echo $date.' '.$maxtime."  ".$mintime.' '.$timediff.'</br>';
        //
        //         $objPHPExcel->getActiveSheet()->SetCellValue('O'.$row, $timediff);
        //
        //       }
        //
        // 			// $maxtime = max($fquery);
        // 			// $mintime = min($fquery);
        // 			// $timediff=$maxtime-$mintime;
        //
        // 			//echo $date.' '.$maxtime."  ".$mintime.' '.$timediff.'</br>';
        //
        // 			// $objPHPExcel->getActiveSheet()->SetCellValue('O'.$row, $timediff);
        // 			// echo "sent to P".$row;
        // 			// echo "</br>".$date." ".$gruppenarbeit_day_sum."</br>";
        //
        //
        // 	// ********************************************************************************************************************
        //
        // 	// ******Gruppenarbeit*************************************************************************************************
        //
        // 		$gruppenarbeit_day_sum = 0;
        //
        // 		$all_dauer_gruppenarbeit_query = mysqli_query($dbc,"SELECT datum, dauer_in_h FROM gruppenarbeit_record");
        //
        // 		while ($all_dauer_gruppenarbeit_array = mysqli_fetch_array($all_dauer_gruppenarbeit_query)) {
        //
        // 			if ($all_dauer_gruppenarbeit_array['datum']==$date) {
        //
        // 				$gruppenarbeit_day_sum = $gruppenarbeit_day_sum + $all_dauer_gruppenarbeit_array['dauer_in_h'];
        //
        // 			}
        // 		}
        //
        // 		$objPHPExcel->getActiveSheet()->SetCellValue('P'.$row, $gruppenarbeit_day_sum);
        // 		// echo "sent to P".$row;
        // 		// echo "</br>".$date." ".$gruppenarbeit_day_sum."</br>";
        //
        //
        // 	// ********************************************************************************************************************
        //
        // 	// ******Einzelarbeit**************************************************************************************************
        //
        // 		$einzelarbeit_day_sum = 0;
        //
        // 		$all_dauer_einzelarbeit_query = mysqli_query($dbc,"SELECT datum, dauer_in_h FROM einzelfallhilfe_record");
        //
        // 		while ($all_dauer_einzelarbeit_array = mysqli_fetch_array($all_dauer_einzelarbeit_query)) {
        //
        // 			if ($all_dauer_einzelarbeit_array['datum']==$date) {
        //
        // 				$einzelarbeit_day_sum = $einzelarbeit_day_sum + $all_dauer_einzelarbeit_array['dauer_in_h'];
        //
        // 			}
        // 		}
        //
        // 		$objPHPExcel->getActiveSheet()->SetCellValue('Q'.$row, $einzelarbeit_day_sum);
        // 		// echo "sent to P".$row;
        // 		// echo "</br>".$date." ".$gruppenarbeit_day_sum."</br>";
        //
        //
        // 	// ********************************************************************************************************************
        //
        // 	// ***Gemeinwesenarbeit***********************************************************************************************
        //
        // 		$gemeinwesenarbeit_day_sum = 0;
        //
        // 		$all_dauer_gemeinwesenarbeit_query = mysqli_query($dbc,"SELECT datum, dauer_in_h FROM gemeinwesenarbeit_record");
        //
        // 		while ($all_dauer_gemeinwesenarbeit_array = mysqli_fetch_array($all_dauer_gemeinwesenarbeit_query)) {
        //
        // 			if ($all_dauer_gemeinwesenarbeit_array['datum']==$date) {
        //
        // 				$gemeinwesenarbeit_day_sum = $gemeinwesenarbeit_day_sum + $all_dauer_gemeinwesenarbeit_array['dauer_in_h'];
        //
        // 			}
        // 		}
        //
        // 		$objPHPExcel->getActiveSheet()->SetCellValue('R'.$row, $gemeinwesenarbeit_day_sum);
        // 		// echo "sent to P".$row;
        // 		// echo "</br>".$date." ".$gruppenarbeit_day_sum."</br>";
        //
        //
        // 	// ********************************************************************************************************************
        //
        //
        // 		$weiblich_day_sum = 0;
        // 		$mannlich_day_sum = 0;
        //
        // 		$all_genders_query = mysqli_query($dbc,"SELECT datum, n_u12_m, n_u12_w, n_u14_m, n_u14_w, n_u14_17_m, n_u14_17_w, n_u18_20_m, n_u18_20_w, n_u21_24_m, n_u21_24_w, n_u25_26_m, n_u25_26_w, n_o27_m, n_o27_w FROM streetwork_record UNION ALL SELECT datum, n_u12_m, n_u12_w, n_u14_m, n_u14_w, n_u14_17_m, n_u14_17_w, n_u18_20_m, n_u18_20_w, n_u21_24_m, n_u21_24_w, n_u25_26_m, n_u25_26_w, n_o27_m, n_o27_w FROM gruppenarbeit_record");
        //
        // 		while ($all_genders_array = mysqli_fetch_array($all_genders_query)) {
        //
        // 			if ($all_genders_array['datum']==$date) {
        //
        // 				$weiblich_day_sum = $weiblich_day_sum + $all_genders_array['n_u12_w'] + $all_genders_array['n_u14_w'] + $all_genders_array['n_u14_17_w'] + $all_genders_array['n_u18_20_w'] + $all_genders_array['n_u21_24_w'] + $all_genders_array['n_u25_26_w'] + $all_genders_array['n_o27_w'];
        //
        // 				$mannlich_day_sum = $mannlich_day_sum + $all_genders_array['n_u12_m'] + $all_genders_array['n_u14_m'] + $all_genders_array['n_u14_17_m'] + $all_genders_array['n_u18_20_m'] + $all_genders_array['n_u21_24_m'] + $all_genders_array['n_u25_26_m'] + $all_genders_array['n_o27_m'];
        // 			}
        // 		}
        //
        // 		$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, $weiblich_day_sum);
        // 		$objPHPExcel->getActiveSheet()->SetCellValue('D'.$row, $mannlich_day_sum);
        // 		// echo "sent to С + D ".$row;
        // 		// echo "</br>".$date." ".$weiblich_day_sum." ".$mannlich_day_sum."</br>";
        //
        // 		// echo '<p style="color:red">'.$weiblich_day_sum." ".$mannlich_day_sum."</br></p>";
        //
        //
        //
        //
        //
        //
        //
        //
        //
        //
        // 		$row++;
        // 		$date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
        //
        // 	}
        //
        //
        // 	$sheetIndex++;
        //
        // }
        //
        //
        //
        // header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        // header('Content-Disposition: attachment;filename="Statistik_17_mobile_JA.xlsx"');
        // header('Cache-Control: max-age=0');
        //
        // $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        // $objWriter->save('php://output');
        //
        // header('Location: ../main_menu/main_menu.php');
        // exit;

//         echo '<script type="text/javascript" language="javascript">
// window.open("../main_menu/main_menu.php");
// </script>';

        ?>
    </body>
</html>
