<?php

require '../../../resources/frameworks/phpspreadsheet/vendor/autoload.php';
require '../../../resources/dbconnection.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
// $objPHPExcel = $reader->load("../../../resources/excel-templates/Statistik_17_mobile_JA.xlsx");
$objPHPExcel = $reader->load("../../../resources/excel-templates/17.xlsx");


$sheetIndex = 2;
// $objPHPExcel->setActiveSheetIndex($sheetIndex);
// $objPHPExcel->getActiveSheet()->SetCellValue('C4', 'hello');

date_default_timezone_set('UTC');

$year_part = '2017-0';
$begining_day = '-01';
$end_day = '-31';

for ($j=1; $j <= 12; $j++) {

	$objPHPExcel->setActiveSheetIndex($sheetIndex);
	$row = 4;

	if ($j==1 || $j==3 || $j==5 || $j==7 || $j==8 || $j==10 || $j==12) {

		$end_day = '-31';

		if ($j >= 9) {
			$year_part = '2017-';
		}
	}

 	if ($j >= 10) {
		$year_part = '2017-';
	}

	if ($j==4 || $j==6 || $j==9 || $j==11) {

		$end_day = '-30';

		if ($j >= 10) {
			$year_part = '2017-';
		}

	}

	if ($j==2){
		$end_day = '-28';
	}

	$date = $year_part.$j.$begining_day;
	$end_date = $year_part.$j.$end_day;

	while (strtotime($date) <= strtotime($end_date)) {

		$zx = 0;
		$zc = 0;
		$zv = 0;
		$zb = 0;
		$zn = 0;
		$zm = 0;

		$all_works_record_query = mysqli_query($dbc, "SELECT datum, n_u12_m, n_u12_w, n_u14_m, n_u14_w, n_u14_17_m, n_u14_17_w, n_u18_20_m, n_u18_20_w, n_u21_24_m, n_u21_24_w, n_u25_26_m, n_u25_26_w, n_o27_m, n_o27_w FROM streetwork_record UNION ALL SELECT datum, n_u12_m, n_u12_w, n_u14_m, n_u14_w, n_u14_17_m, n_u14_17_w, n_u18_20_m, n_u18_20_w, n_u21_24_m, n_u21_24_w, n_u25_26_m, n_u25_26_w, n_o27_m, n_o27_w FROM gruppenarbeit_record UNION ALL SELECT datum, n_u12_m, n_u12_w, n_u14_m, n_u14_w, n_u14_17_m, n_u14_17_w, n_u18_20_m, n_u18_20_w, n_u21_24_m, n_u21_24_w, n_u25_26_m, n_u25_26_w, n_o27_m, n_o27_w FROM einzelfallhilfe_record UNION ALL SELECT datum, n_u12_m, n_u12_w, n_u14_m, n_u14_w, n_u14_17_m, n_u14_17_w, n_u18_20_m, n_u18_20_w, n_u21_24_m, n_u21_24_w, n_u25_26_m, n_u25_26_w, n_o27_m, n_o27_w FROM gemeinwesenarbeit_record");

		while ($all_works_record_array = mysqli_fetch_array($all_works_record_query)) {

			if ($all_works_record_array['datum']==$date) {
					//echo $gruppenarbeit_record_array['datum'];
					$n_u12_all = 0;
				 	$n_u12_all = $all_works_record_array['n_u12_m']+$all_works_record_array['n_u12_w']+$all_works_record_array['n_u14_m']+$all_works_record_array['n_u14_w'];
				 	$zx = $zx + $n_u12_all;
				 	//echo "<p style='color:red;'>".$date."</p>";
				 	//echo " ".$zx."</br>";
			}

			if ($all_works_record_array['datum']==$date) {
					//echo $gruppenarbeit_record_array['datum'];
					$n_u14_17_all = 0;
				 	$n_u14_17_all = $all_works_record_array['n_u14_17_m']+$all_works_record_array['n_u14_17_w'];
				 	$zc = $zc + $n_u14_17_all;
				 	//echo "<p style='color:red;'>".$date."</p>";
				 	//echo " ".$zx."</br>";
			}

			if ($all_works_record_array['datum']==$date) {
					//echo $gruppenarbeit_record_array['datum'];
					$n_u18_20_all = 0;
				 	$n_u18_20_all = $all_works_record_array['n_u18_20_m']+$all_works_record_array['n_u18_20_w'];
				 	$zv = $zv + $n_u18_20_all;
				 	//echo "<p style='color:red;'>".$date."</p>";
				 	//echo " ".$zx."</br>";
			}

			if ($all_works_record_array['datum']==$date) {
					//echo $gruppenarbeit_record_array['datum'];
					$n_u21_24_all = 0;
				 	$n_u21_24_all = $all_works_record_array['n_u21_24_m']+$all_works_record_array['n_u21_24_w'];
				 	$zb = $zb + $n_u21_24_all;
				 	//echo "<p style='color:red;'>".$date."</p>";
				 	//echo " ".$zx."</br>";
			}

			if ($all_works_record_array['datum']==$date) {
					//echo $gruppenarbeit_record_array['datum'];
					$n_u25_26_all = 0;
				 	$n_u25_26_all = $all_works_record_array['n_u25_26_m']+$all_works_record_array['n_u25_26_w'];
				 	$zn = $zn + $n_u25_26_all;
				 	//echo "<p style='color:red;'>".$date."</p>";
				 	//echo " ".$zx."</br>";
			}

			if ($all_works_record_array['datum']==$date) {
					//echo $gruppenarbeit_record_array['datum'];
					$n_o27_all = 0;
				 	$n_o27_all = $all_works_record_array['n_o27_m']+$all_works_record_array['n_o27_w'];
				 	$zm = $zm + $n_o27_all;
				 	//echo "<p style='color:red;'>".$date."</p>";
				 	//echo " ".$zx."</br>";
			}


		}

		//**** Filling the 6-13 field
		$objPHPExcel->getActiveSheet()->SetCellValue('H'.$row, $zx);
		//echo "sent to H".$row;
		//echo "</br>".$date." ".$zx."</br>";

		//**** Filling the 14-17 field
		$objPHPExcel->getActiveSheet()->SetCellValue('I'.$row, $zc);
		// echo "sent to I".$row;
		// echo "</br>".$date." ".$zc."</br>";

		//**** Filling the 18-20 field
		$objPHPExcel->getActiveSheet()->SetCellValue('J'.$row, $zv);
		// echo "sent to J".$row;
		// echo "</br>".$date." ".$zv."</br>";

		//**** Filling the 21-24 field
		$objPHPExcel->getActiveSheet()->SetCellValue('K'.$row, $zb);
		// echo "sent to K".$row;
		// echo "</br>".$date." ".$zb."</br>";

		//**** Filling the 25-26 field
		$objPHPExcel->getActiveSheet()->SetCellValue('L'.$row, $zn);
		// echo "sent to L".$row;
		// echo "</br>".$date." ".$zn."</br>";

		//**** Filling the 27+ field
		$objPHPExcel->getActiveSheet()->SetCellValue('M'.$row, $zm);
		// echo "sent to M".$row;
		// echo "</br>".$date." ".$zm."</br>";


		// ******Streetwork*************************************************************************************************

			$streetwork_time_query = mysqli_query($dbc,"SELECT datum, uhrzeit FROM streetwork_record");

			$fquery = array();

			while ($streetwork_time_array = mysqli_fetch_array($streetwork_time_query)) {

				if ($streetwork_time_array['datum']==$date) {
					$fquery[] = $streetwork_time_array['uhrzeit'];
				}
			}

			if(empty($fquery)){

				$timediff=0;
				$objPHPExcel->getActiveSheet()->SetCellValue('O'.$row, $timediff);

      }else{

        $maxtime = max($fquery);
        $mintime = min($fquery);
        $timediff=$maxtime-$mintime;

        $objPHPExcel->getActiveSheet()->SetCellValue('O'.$row, $timediff);

      }

			// $maxtime = max($fquery);
			// $mintime = min($fquery);
			// $timediff=$maxtime-$mintime;

			//echo $date.' '.$maxtime."  ".$mintime.' '.$timediff.'</br>';

			// $objPHPExcel->getActiveSheet()->SetCellValue('O'.$row, $timediff);
			// echo "sent to P".$row;
			// echo "</br>".$date." ".$gruppenarbeit_day_sum."</br>";


	// ********************************************************************************************************************

	// ******Gruppenarbeit*************************************************************************************************

		$gruppenarbeit_day_sum = 0;

		$all_dauer_gruppenarbeit_query = mysqli_query($dbc,"SELECT datum, dauer_in_h FROM gruppenarbeit_record");

		while ($all_dauer_gruppenarbeit_array = mysqli_fetch_array($all_dauer_gruppenarbeit_query)) {

			if ($all_dauer_gruppenarbeit_array['datum']==$date) {

				$gruppenarbeit_day_sum = $gruppenarbeit_day_sum + $all_dauer_gruppenarbeit_array['dauer_in_h'];

			}
		}

		$objPHPExcel->getActiveSheet()->SetCellValue('P'.$row, $gruppenarbeit_day_sum);

	// ********************************************************************************************************************

	// ******Einzelarbeit**************************************************************************************************

		$einzelarbeit_day_sum = 0;

		$all_dauer_einzelarbeit_query = mysqli_query($dbc,"SELECT datum, dauer_in_h FROM einzelfallhilfe_record");

		while ($all_dauer_einzelarbeit_array = mysqli_fetch_array($all_dauer_einzelarbeit_query)) {

			if ($all_dauer_einzelarbeit_array['datum']==$date) {

				$einzelarbeit_day_sum = $einzelarbeit_day_sum + $all_dauer_einzelarbeit_array['dauer_in_h'];

			}
		}

		$objPHPExcel->getActiveSheet()->SetCellValue('Q'.$row, $einzelarbeit_day_sum);

	// ********************************************************************************************************************

	// ***Gemeinwesenarbeit***********************************************************************************************

		$gemeinwesenarbeit_day_sum = 0;

		$all_dauer_gemeinwesenarbeit_query = mysqli_query($dbc,"SELECT datum, dauer_in_h FROM gemeinwesenarbeit_record");

		while ($all_dauer_gemeinwesenarbeit_array = mysqli_fetch_array($all_dauer_gemeinwesenarbeit_query)) {

			if ($all_dauer_gemeinwesenarbeit_array['datum']==$date) {

				$gemeinwesenarbeit_day_sum = $gemeinwesenarbeit_day_sum + $all_dauer_gemeinwesenarbeit_array['dauer_in_h'];

			}
		}

		$objPHPExcel->getActiveSheet()->SetCellValue('R'.$row, $gemeinwesenarbeit_day_sum);

	// ********************************************************************************************************************

		$weiblich_day_sum = 0;
		$mannlich_day_sum = 0;

		$all_genders_query = mysqli_query($dbc,"SELECT datum, n_u12_m, n_u12_w, n_u14_m, n_u14_w, n_u14_17_m, n_u14_17_w, n_u18_20_m, n_u18_20_w, n_u21_24_m, n_u21_24_w, n_u25_26_m, n_u25_26_w, n_o27_m, n_o27_w FROM streetwork_record UNION ALL SELECT datum, n_u12_m, n_u12_w, n_u14_m, n_u14_w, n_u14_17_m, n_u14_17_w, n_u18_20_m, n_u18_20_w, n_u21_24_m, n_u21_24_w, n_u25_26_m, n_u25_26_w, n_o27_m, n_o27_w FROM gruppenarbeit_record");

		while ($all_genders_array = mysqli_fetch_array($all_genders_query)) {

			if ($all_genders_array['datum']==$date) {

				$weiblich_day_sum = $weiblich_day_sum + $all_genders_array['n_u12_w'] + $all_genders_array['n_u14_w'] + $all_genders_array['n_u14_17_w'] + $all_genders_array['n_u18_20_w'] + $all_genders_array['n_u21_24_w'] + $all_genders_array['n_u25_26_w'] + $all_genders_array['n_o27_w'];

				$mannlich_day_sum = $mannlich_day_sum + $all_genders_array['n_u12_m'] + $all_genders_array['n_u14_m'] + $all_genders_array['n_u14_17_m'] + $all_genders_array['n_u18_20_m'] + $all_genders_array['n_u21_24_m'] + $all_genders_array['n_u25_26_m'] + $all_genders_array['n_o27_m'];
			}
		}

		$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, $weiblich_day_sum);
		$objPHPExcel->getActiveSheet()->SetCellValue('D'.$row, $mannlich_day_sum);

		$row++;
		$date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));

	}
	$sheetIndex++;
}

// header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
// header('Content-Disposition: attachment;filename="Statistik_17_mobile_JA.xlsx"');
// header('Cache-Control: max-age=0');

$writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);

ob_start();
$writer->save('php://output');
$xlsData = ob_get_contents();
ob_end_clean();
$response =  array(
    'op' => 'ok',
    'file' => "data:application/vnd.ms-excel;base64,".base64_encode($xlsData)
);
die(json_encode($response));

?>
