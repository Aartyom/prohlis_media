<?php

	require_once('../../../resources/dbconnection.php');

	$err = array();

	if (trim($_POST['project_name_subform_input_field']) == '') {
		$err[] = 'Project name missing!';
	} else {
		$project_name = trim($_POST['project_name_subform_input_field']);
	}

	if (trim($_POST['type_of_work']) == '') {
		$err[] = 'type_of_work';
	} else {
		$type_of_work = trim($_POST['type_of_work']);
	}

	 if (empty($err)){
	 	// go ahead
		$query = "INSERT INTO project (project_name, type_of_work) VALUES ('$project_name', '$type_of_work')";
		
		mysqli_query($dbc,$query);

		$affected_rows = mysqli_affected_rows($dbc);
		
		if ($affected_rows == 1) {

			echo "RECORD ADDED";

		} else {

			echo 'Error with request execution';
		}

		mysqli_close($dbc);
		
	} else {

	 	echo 'Data missing'+$err;
	 }
?>