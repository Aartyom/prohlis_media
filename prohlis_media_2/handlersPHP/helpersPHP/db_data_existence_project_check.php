<?php

	require_once('../../resources/dbconnection.php');

	$err = array();

	if (trim($_POST['db_table']) == '') {
		$err[] = 'db_table';
	} else {
		$db_table = trim($_POST['db_table']); // project
	}

	if (trim($_POST['db_field_project_name']) == '') {
		$err[] = 'db_field_project_name';
	} else {
		$db_field_project_name = trim($_POST['db_field_project_name']); //project_name
	}

	if (trim($_POST['db_value']) == '') {
		$err[] = 'db_value';
	} else {
		$db_value = trim($_POST['db_value']);
	}

	if (trim($_POST['type_of_work']) == '') {
		$err[] = 'type_of_work';
	} else {
		$type_of_work = trim($_POST['type_of_work']); //type_of_work
	}

	if (trim($_POST['db_field_type_of_work']) == '') {
		$err[] = 'db_field_type_of_work';
	} else {
		$db_field_type_of_work = trim($_POST['db_field_type_of_work']);
	}
	

	 if (empty($err)){
	 	// go ahead
		mysqli_query($dbc,"SELECT $db_field_project_name, $db_field_type_of_work  FROM $db_table WHERE $db_field_project_name = '$db_value' AND $db_field_type_of_work = '$type_of_work'");

		$affected_rows = mysqli_affected_rows($dbc);
		
		if ($affected_rows == 1) {

			echo $db_value;

		} else {

			echo 'Error with request execution';
		}

		mysqli_close($dbc);
		
	} else {

	 	echo 'Data missing';
	 }
?>