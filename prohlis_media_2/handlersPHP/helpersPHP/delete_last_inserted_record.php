<?php

	require_once('../../resources/dbconnection.php');

	$err = array();

	if (trim($_POST['db_table']) == '') {
		$err[] = 'db_table';
	} else {
		$db_table = trim($_POST['db_table']);
	}

	if (trim($_POST['db_field']) == '') {
		$err[] = 'db_field';
	} else {
		$db_field = trim($_POST['db_field']);
	}

	 if (empty($err)){
	 	
		$query = "DELETE FROM $db_table ORDER BY $db_field DESC LIMIT 1";
		
		mysqli_query($dbc,$query);

		$affected_rows = mysqli_affected_rows($dbc);
		
		if ($affected_rows == 1) {

			echo "RECORD DELETED";

		} else {

			echo 'Error with request execution';
		}

		mysqli_close($dbc);
		
	} else {

	 	echo 'Request missing';
	 }
?>