<?php

require '../../../../resources/frameworks/phpspreadsheet/vendor/autoload.php';
require '../../../../resources/dbconnection.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
$objPHPExcel = $reader->load("../../../../resources/excel-templates/Active_feedback.xlsx");

if (trim($_POST['angebot']) == '') {
    $angebot = '-';
} else {
    $angebot = trim($_POST['angebot']);
}

if (trim($_POST['datum']) == '') {
    $datum = '-';
} else {
    $datum = trim($_POST['datum']);
}

if (trim($_POST['mitarbeiter']) == '') {
    $mitarbeiter = '-';
} else {
    $mitarbeiter = trim($_POST['mitarbeiter']);
}

if (trim($_POST['an_wen']) == '') {
    $an_wen = '-';
} else {
    $an_wen = trim($_POST['an_wen']);
}

if (trim($_POST['art_des_feedback']) == '') {
    $art_des_feedback = '-';
} else {
    $art_des_feedback = trim($_POST['art_des_feedback']);
}

if (trim($_POST['inhalt_des_feedback']) == '') {
    $inhalt_des_feedback = '-';
} else {
    $inhalt_des_feedback = trim($_POST['inhalt_des_feedback']);
}

if (trim($_POST['weiterbearbeitung']) == '') {
    $weiterbearbeitung = '-';
} else {
    $weiterbearbeitung = trim($_POST['weiterbearbeitung']);
}

$objPHPExcel->setActiveSheetIndex(0);
$sheet = $objPHPExcel->getActiveSheet();
$sheet->SetCellValue('C4', $angebot);
$sheet->SetCellValue('C6', $datum);
$sheet->SetCellValue('C8', $mitarbeiter);
$sheet->SetCellValue('C10', $an_wen);
$sheet->SetCellValue('C12', $art_des_feedback);
$sheet->SetCellValue('B15', $inhalt_des_feedback);
$sheet->SetCellValue('B23', $weiterbearbeitung);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Active_feedback.xlsx"');
header('Cache-Control: max-age=0');

$writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
ob_start();
$writer->save('php://output');
$xlsData = ob_get_contents();
ob_end_clean();
$response =  array(
    'op' => 'ok',
    'file' => "data:application/vnd.ms-excel;base64,".base64_encode($xlsData)
);
die(json_encode($response));
?>