<?php

require '../../../../resources/frameworks/phpspreadsheet/vendor/autoload.php';
require '../../../../resources/dbconnection.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
$objPHPExcel = $reader->load("../../../../resources/excel-templates/Passive_feedback.xlsx");

if (trim($_POST['angebot']) == '') {
    $angebot = '-';
} else {
    $angebot = trim($_POST['angebot']);
}

if (trim($_POST['aktion']) == '') {
    $aktion = '-';
} else {
    $aktion = trim($_POST['aktion']);
}

if (trim($_POST['datum']) == '') {
    $datum = '-';
} else {
    $datum = trim($_POST['datum']);
}

if (trim($_POST['mitarbeiter']) == '') {
    $mitarbeiter = '-';
} else {
    $mitarbeiter = trim($_POST['mitarbeiter']);
}

if (trim($_POST['positiv_resp']) == '') {
    $positiv_resp = '-';
} else {
    $positiv_resp = trim($_POST['positiv_resp']);
}

if (trim($_POST['negativ_resp']) == '') {
    $negativ_resp = '-';
} else {
    $negativ_resp = trim($_POST['negativ_resp']);
}

if (trim($_POST['schlussfolgerungen']) == '') {
    $schlussfolgerungen = '-';
} else {
    $schlussfolgerungen = trim($_POST['schlussfolgerungen']);
}

if (trim($_POST['rahmenbedingungen']) == '') {
    $rahmenbedingungen = '-';
} else {
    $rahmenbedingungen = trim($_POST['rahmenbedingungen']);
}

if (trim($_POST['teilnehmer_innen']) == '') {
    $teilnehmer_innen = '-';
} else {
    $teilnehmer_innen = trim($_POST['teilnehmer_innen']);
}

if (trim($_POST['fb_anzahl']) == '') {
    $fb_anzahl = '-';
} else {
    $fb_anzahl = trim($_POST['fb_anzahl']);
}

if (trim($_POST['fb_geber_innen']) == '') {
    $fb_geber_innen = '-';
} else {
    $fb_geber_innen = trim($_POST['fb_geber_innen']);
}

if (trim($_POST['allgemeine_einschatzung']) == '') {
    $allgemeine_einschatzung = '-';
} else {
    $allgemeine_einschatzung = trim($_POST['allgemeine_einschatzung']);
}

$objPHPExcel->setActiveSheetIndex(0);
$sheet = $objPHPExcel->getActiveSheet();
$sheet->SetCellValue('C4', $angebot);
$sheet->SetCellValue('H4', $aktion);
$sheet->SetCellValue('C6', $datum);
$sheet->SetCellValue('H6', $mitarbeiter);
$sheet->SetCellValue('B23', $positiv_resp);
$sheet->SetCellValue('G23', $negativ_resp);
$sheet->SetCellValue('B20', $schlussfolgerungen);
$sheet->SetCellValue('E13', $rahmenbedingungen);
$sheet->SetCellValue('E16', $teilnehmer_innen);
$sheet->SetCellValue('J16', $fb_anzahl);
$sheet->SetCellValue('B16', $fb_geber_innen);
$sheet->SetCellValue('B10', $allgemeine_einschatzung);

// header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
// header('Content-Disposition: attachment;filename="Passive_feedback.xlsx"');
// header('Cache-Control: max-age=0');

$writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
ob_start();
$writer->save('php://output');
$xlsData = ob_get_contents();
ob_end_clean();
$response =  array(
    'op' => 'ok',
    'file' => "data:application/vnd.ms-excel;base64,".base64_encode($xlsData)
);
die(json_encode($response));

?>