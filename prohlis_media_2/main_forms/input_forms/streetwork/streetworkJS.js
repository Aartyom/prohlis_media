var type_of_work = 'streetwork';
var db_table_revert = 'streetwork_record';
var db_field_revert = 'streetwork_record_id';

function newRecord() {
	$("#n_bekannt").val('');
	$("#n_unbekannt").val('');
	$("#n_u12_m").val('');
	$("#n_u12_w").val('');
	$("#n_u14_m").val('');
	$("#n_u14_w").val('');
	$("#n_u14_17_m").val('');
	$("#n_u14_17_w").val('');
	$("#n_u18_20_m").val('');
	$("#n_u18_20_w").val('');
	$("#n_u21_24_m").val('');
	$("#n_u21_24_w").val('');
	$("#n_u25_26_m").val('');
	$("#n_u25_26_w").val('');
	$("#n_o27_m").val('');
	$("#n_o27_w").val('');
	
	$('#th_sucht').prop('checked', false).val('');
	$('#th_gewalt').prop('checked', false).val('');
	$('#th_kriminalitat').prop('checked', false).val('');
	$('#th_gesundheit').prop('checked', false).val('');
	$('#th_freizeit').prop('checked', false).val('');
	$('#th_identitat').prop('checked', false).val('');
	$('#th_wohnraum').prop('checked', false).val('');
	$('#th_familie').prop('checked', false).val('');
	$('#th_behorden').prop('checked', false).val('');
	$('#th_finanzielle').prop('checked', false).val('');
	$('#th_arbeit').prop('checked', false).val('');
	$('#th_beziehung').prop('checked', false).val('');
	$('#th_politische_themen').prop('checked', false).val('');
	$('#th_digitales').prop('checked', false).val('');
	$('#th_vorstellung').prop('checked', false).val('');

	$("#platz").val('defaultv').css("box-shadow", "none");
	// $("#wetter").val('defaultv').css("box-shadow", "none");
	$("#ausfallgrund").val('defaultv').css("box-shadow", "none");
	// $("#mobilitat").val('defaultv').css("box-shadow", "none");
	$("#platzwahrnehmung").val('').css("box-shadow", "none");
	$("#th_sonstiges").val('').css("box-shadow", "none");
	$("#anmerkungen").val('').css("box-shadow", "none");
	toastr.success('New record opened', 'Success!', {timeOut: 5000})
}

Date.prototype.yyyymmdd = function() {
    var yyyy = this.getFullYear();
    var mm = this.getMonth() < 9 ? "0" + (this.getMonth() + 1) : (this.getMonth() + 1); // getMonth() is zero-based
    var dd  = this.getDate() < 10 ? "0" + this.getDate() : this.getDate();
    return "".concat(yyyy).concat("-").concat(mm).concat("-").concat(dd);
};

function submitRecordFunc(){
	var project_name = $('#project_name').val();
	var datum_nf = $("#datum").val();
	
	if(datum_nf != '') {
		var patterndatum = /[0-9]{2}.[0-9]{2}.[0-9]{4}/i;
		if (patterndatum.test(datum_nf)) {
			var datum_nf = $('#datum').val().split(".");
		}else{
			$("#datum").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
			toastr.error('Date format is incorrect (dd.mm.yyyy)', 'Error!', {
			timeOut: 5000
			})
			return false;
		}
	}else{
		fail = 'Fill the "Datum" field'
		toastr.error(fail, 'Error!', {
		timeOut: 5000
		})
		$("#datum").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
		return false;
	}

	var datum = new Date(datum_nf[2], datum_nf[1] - 1, datum_nf[0]).yyyymmdd();
	var uhrzeit = $('#uhrzeit').val();
	
	if(uhrzeit != '') {
		var pattern = /[0-9]{2}:[0-9]{2}/i;
		if (pattern.test(uhrzeit)) {
			var uhrzeit = $('#uhrzeit').val();
		}else{
			$("#uhrzeit").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
			toastr.error('Time format is incorrect (hh:mm)', 'Error!', {
			timeOut: 5000
			})
			return false;
		}
	}else{
		fail = 'Fill the "Uhrzeit" field'
		toastr.error(fail, 'Error!', {
		timeOut: 5000
		})
		$("#uhrzeit").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
		return false;
	}

	var mitarbeiter01 = $('#mitarbeiter01').val();
	var mitarbeiter02 = $('#mitarbeiter02').val();
	var mitarbeiter03 = $('#mitarbeiter03').val();
	var n_bekannt_up = $('#n_bekannt').val();
	var n_unbekannt_up = $('#n_unbekannt').val();
	var n_u12_m_up = $('#n_u12_m').val();
	var n_u12_w_up = $('#n_u12_w').val();
	var n_u14_m_up = $('#n_u14_m').val();
	var n_u14_w_up = $('#n_u14_w').val();
	var n_u14_17_m_up = $('#n_u14_17_m').val();
	var n_u14_17_w_up = $('#n_u14_17_w').val();
	var n_u18_20_m_up = $('#n_u18_20_m').val();
	var n_u18_20_w_up = $('#n_u18_20_w').val();
	var n_u21_24_m_up = $('#n_u21_24_m').val();
	var n_u21_24_w_up = $('#n_u21_24_w').val();
	var n_u25_26_m_up = $('#n_u25_26_m').val();
	var n_u25_26_w_up = $('#n_u25_26_w').val();
	var n_o27_m_up = $('#n_o27_m').val();
	var n_o27_w_up = $('#n_o27_w').val();
	var th_sucht = $('#th_sucht').val();
	var th_gewalt = $('#th_gewalt').val();
	var th_kriminalitat = $('#th_kriminalitat').val();
	var th_gesundheit = $('#th_gesundheit').val();
	var th_freizeit = $('#th_freizeit').val();
	var th_identitat = $('#th_identitat').val();
	var th_wohnraum = $('#th_wohnraum').val();
	var th_familie = $('#th_familie').val();
	var th_behorden = $('#th_behorden').val();
	var th_finanzielle = $('#th_finanzielle').val();
	var th_arbeit = $('#th_arbeit').val();
	var th_beziehung = $('#th_beziehung').val();
	var th_politische_themen = $('#th_politische_themen').val();
	var th_digitales = $('#th_digitales').val();
	var th_vorstellung = $('#th_vorstellung').val();
	var platz = $('#platz').val();
	var wetter = $('#wetter').val();
	var ausfallgrund = $('#ausfallgrund').val();
	var mobilitat = $('#mobilitat').val();
	var platzwahrnehmung = $('#platzwahrnehmung').val();
	var th_sonstiges = $('#th_sonstiges').val();
	var anmerkungen = $('#anmerkungen').val();
	var fail = '';

	if (ausfallgrund == null || ausfallgrund == 'defaultv' || ausfallgrund == '' || ausfallgrund == "-") {

		if (datum == null || uhrzeit == null || mitarbeiter01  == null || mitarbeiter02  == null || platz == null || wetter == null || mobilitat == null ||
		datum == '' || uhrzeit == '' || mitarbeiter01  == '' || mitarbeiter02  == '' || platz == '' || wetter == '' || mobilitat == '') {
			fail = 'Fill in all required fields';

			if (mitarbeiter01 == null || mitarbeiter01 == "" || mitarbeiter01 == "-") {
				$("#mitarbeiter01").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
			}

			if (mitarbeiter02 == null || mitarbeiter02 == "" || mitarbeiter02 == "-") {
				$(".mitarbeiter02_red_line").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
			}

			if (datum == "" || datum == null) {
				$("#datum").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
			}

			if (uhrzeit == null || uhrzeit == "") {
				$("#uhrzeit").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
			}

			if (wetter == null || wetter == "") {
				$(".wetter_red_line").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
			}

			if (platz == null || platz == "") {
				$(".platz_red_line").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
			}

			if (mobilitat == null || mobilitat == "") {
				$(".mobilitat_red_line").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");

			}
			toastr.error(fail, 'Error!', {
			timeOut: 5000
			})
			return false;
		}else{
			if(mitarbeiter03  == '' || mitarbeiter03  == null || mitarbeiter03  == 'defaultv') mitarbeiter03 = '-';
		    if(n_bekannt_up  == '-' || n_bekannt_up  == null || n_bekannt_up  == '') n_bekannt_up = 0;
		    if(n_unbekannt_up  == '-' || n_unbekannt_up  == null || n_unbekannt_up  == '') n_unbekannt_up = 0;
		    if(n_u12_m_up  == '-' || n_u12_m_up  == null || n_u12_m_up  == '') n_u12_m_up = 0;
		    if(n_u12_m_up  == '-' || n_u12_m_up  == null || n_u12_m_up  == '') n_u12_m_up = 0;
		    if(n_u12_w_up  == '-' || n_u12_w_up  == null || n_u12_w_up  == '') n_u12_w_up = 0;
		    if(n_u14_m_up  == '-' || n_u14_m_up  == null || n_u14_m_up  == '') n_u14_m_up = 0;
		    if(n_u14_w_up  == '-' || n_u14_w_up  == null || n_u14_w_up  == '') n_u14_w_up = 0;
		    if(n_u14_17_m_up  == '-' || n_u14_17_m_up  == null || n_u14_17_m_up  == '') n_u14_17_m_up = 0;
		    if(n_u14_17_w_up  == '-' || n_u14_17_w_up  == null || n_u14_17_w_up  == '') n_u14_17_w_up = 0;
		    if(n_u18_20_m_up  == '-' || n_u18_20_m_up  == null || n_u18_20_m_up  == '') n_u18_20_m_up = 0;
		    if(n_u18_20_w_up  == '-' || n_u18_20_w_up  == null || n_u18_20_w_up  == '') n_u18_20_w_up = 0;
		    if(n_u21_24_m_up  == '-' || n_u21_24_m_up  == null || n_u21_24_m_up  == '') n_u21_24_m_up = 0;
		    if(n_u21_24_w_up  == '-' || n_u21_24_w_up  == null || n_u21_24_w_up  == '') n_u21_24_w_up = 0;
		    if(n_u25_26_m_up  == '-' || n_u25_26_m_up  == null || n_u25_26_m_up  == '') n_u25_26_m_up = 0;
		    if(n_u25_26_w_up  == '-' || n_u25_26_w_up  == null || n_u25_26_w_up  == '') n_u25_26_w_up = 0;
		    if(n_o27_m_up  == '-' || n_o27_m_up  == null ||n_o27_m_up  == '') n_o27_m_up = 0;
		    if(n_o27_w_up  == '-' || n_o27_w_up  == null ||n_o27_w_up  == '') n_o27_w_up = 0;
		    if(anmerkungen  == '' || anmerkungen  == null) anmerkungen = '-';
			if(platzwahrnehmung  == '' || platzwahrnehmung  == null) platzwahrnehmung = '-';
			if(ausfallgrund  == '' || ausfallgrund  == null) ausfallgrund = '-';
		    if(th_sonstiges  == '' || th_sonstiges  == null) th_sonstiges = '-';
		    if(project_name  == '' || project_name  == null || project_name  == 'defaultv') project_name = '-';

			if($("#th_sucht").is(':checked')){th_sucht=1;}else{th_sucht=0;}
			if($("#th_gewalt").is(':checked')){th_gewalt=1;}else{th_gewalt=0;}
			if($("#th_kriminalitat").is(':checked')){th_kriminalitat=1;}else{th_kriminalitat=0;}
			if($("#th_gesundheit").is(':checked')){th_gesundheit=1;}else{th_gesundheit=0;}
			if($("#th_freizeit").is(':checked')){th_freizeit=1;}else{th_freizeit=0;}
			if($("#th_identitat").is(':checked')){th_identitat=1;}else{th_identitat=0;}
			if($("#th_wohnraum").is(':checked')){th_wohnraum=1;}else{th_wohnraum=0;}
			if($("#th_familie").is(':checked')){th_familie=1;}else{th_familie=0;}
			if($("#th_behorden").is(':checked')){th_behorden=1;}else{th_behorden=0;}
			if($("#th_finanzielle").is(':checked')){th_finanzielle=1;}else{th_finanzielle=0;}
			if($("#th_arbeit").is(':checked')){th_arbeit=1;}else{th_arbeit=0;}
			if($("#th_beziehung").is(':checked')){th_beziehung=1;}else{th_beziehung=0;}
			if($("#th_politische_themen").is(':checked')){th_politische_themen=1;}else{th_politische_themen=0;}
			if($("#th_digitales").is(':checked')){th_digitales=1;}else{th_digitales=0;}
			if($("#th_vorstellung").is(':checked')){th_vorstellung=1;}else{th_vorstellung=0;}

			var n_bekannt = parseInt(n_bekannt_up);
			var n_unbekannt = parseInt(n_unbekannt_up);
			var n_u12_m = parseInt(n_u12_m_up);
			var n_u12_w = parseInt(n_u12_w_up);
			var n_u14_m = parseInt(n_u14_m_up);
			var n_u14_w = parseInt(n_u14_w_up);
			var n_u14_17_m = parseInt(n_u14_17_m_up);
			var n_u14_17_w = parseInt(n_u14_17_w_up);
			var n_u18_20_m = parseInt(n_u18_20_m_up);
			var n_u18_20_w = parseInt(n_u18_20_w_up);
			var n_u21_24_m = parseInt(n_u21_24_m_up);
			var n_u21_24_w = parseInt(n_u21_24_w_up);
			var n_u25_26_m = parseInt(n_u25_26_m_up);
			var n_u25_26_w = parseInt(n_u25_26_w_up);
			var n_o27_m = parseInt(n_o27_m_up);
			var n_o27_w = parseInt(n_o27_w_up);

    		var num_people_ages = n_u12_m + n_u12_w + n_u14_m + n_u14_w + n_u14_17_m + n_u14_17_w + n_u18_20_m + n_u18_20_w
	 		+ n_u21_24_m + n_u21_24_w + n_u25_26_m + n_u25_26_w + n_o27_m + n_o27_w;

			var num_bekannt_and_unbekannt = n_bekannt + n_unbekannt;

			if (num_people_ages != num_bekannt_and_unbekannt) {
				fail = "Number of bekannt and unbekannt does not correspond to the total number of people";
				$(".people-sum-check").css("box-shadow", "0px 3px 0px 0px rgb(255, 162, 0)");
				toastr.error(fail, 'Error!', {
				timeOut: 5000
				})
		        return false;
			}
		}
	} else {
		if (mitarbeiter01=='' || mitarbeiter02=='' || datum=='' || uhrzeit=='' || mitarbeiter01==null || mitarbeiter02==null || datum==null || uhrzeit==null || mitarbeiter01=='defaultv' || mitarbeiter02=='defaultv'){

			fail = 'Fill in all required fields';

			if (mitarbeiter01 == null || mitarbeiter01 == "" || mitarbeiter01 == "-") {
				$("#mitarbeiter01").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
			}

			if (mitarbeiter02 == null || mitarbeiter02 == "" || mitarbeiter02 == "-") {
				$(".mitarbeiter02_red_line").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
			}
			if (datum == "" || datum == null) {
				$("#datum").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
			}

			if (uhrzeit == null || uhrzeit == "") {
				$("#uhrzeit").css("box-shadow", "0px 3px 0px 0px rgb(255,0,0)");
			}
			toastr.error(fail, 'Error!', {
			timeOut: 5000
			})
			return false;
		}
			project_name = '-';
			platz = '-';
			wetter = '-';
			mobilitat = '-';
			mitarbeiter03 = '-';
			n_bekannt = 0;
			n_unbekannt = 0;
			n_u12_m = 0;
			n_u12_w = 0;
			n_u14_m = 0;
			n_u14_w = 0;
			n_u14_17_m = 0;
			n_u14_17_w = 0;
			n_u18_20_m = 0;
			n_u18_20_w = 0;
			n_u21_24_m = 0;
			n_u21_24_w = 0;
			n_u25_26_m = 0;
			n_u25_26_w = 0;
			n_o27_m = 0;
			n_o27_w = 0;
			th_sucht = 0;
			th_gewalt = 0;
			th_kriminalitat = 0;
			th_gesundheit = 0;
			th_freizeit = 0;
			th_identitat = 0;
			th_wohnraum = 0;
			th_familie = 0;
			th_behorden = 0;
			th_finanzielle = 0;
			th_arbeit = 0;
			th_beziehung = 0;
			th_politische_themen = 0;
			th_digitales = 0;
			th_vorstellung = 0;
			platzwahrnehmung = '-';
			th_sonstiges = '-';
			if(anmerkungen  == '' || anmerkungen  == null) anmerkungen = '-';
	}
	$.ajax ({
        url: '../../../handlersPHP/main_form_handlers/streetwork_handler.php',
        type: 'POST',
        cache: false,
        data: {'project_name':project_name, 'datum':datum, 'uhrzeit':uhrzeit, 'mitarbeiter01':mitarbeiter01, 'mitarbeiter02':mitarbeiter02,
        'mitarbeiter03':mitarbeiter03, 'n_bekannt':n_bekannt, 'n_unbekannt':n_unbekannt, 'n_u12_m':n_u12_m,
        'n_u12_w':n_u12_w, 'n_u14_m':n_u14_m, 'n_u14_w':n_u14_w, 'n_u14_17_m':n_u14_17_m, 'n_u14_17_w':n_u14_17_w,
        'n_u18_20_m':n_u18_20_m, 'n_u18_20_w':n_u18_20_w, 'n_u21_24_m':n_u21_24_m, 'n_u21_24_w':n_u21_24_w, 'n_u25_26_m':n_u25_26_m, 'n_u25_26_w':n_u25_26_w, 'n_o27_m':n_o27_m, 'n_o27_w':n_o27_w,
        'th_sucht':th_sucht, 'th_gewalt':th_gewalt, 'th_kriminalitat':th_kriminalitat, 'th_gesundheit':th_gesundheit,
        'th_freizeit':th_freizeit, 'th_identitat':th_identitat, 'th_wohnraum':th_wohnraum, 'th_familie':th_familie,
        'th_behorden':th_behorden, 'th_finanzielle':th_finanzielle, 'th_arbeit':th_arbeit, 'th_beziehung':th_beziehung,
        'th_politische_themen':th_politische_themen, 'th_digitales':th_digitales, 'th_vorstellung':th_vorstellung,
        'platz':platz, 'wetter':wetter, 'ausfallgrund':ausfallgrund, 'mobilitat':mobilitat, 'platzwahrnehmung':platzwahrnehmung,
        'th_sonstiges':th_sonstiges, 'anmerkungen':anmerkungen},
        dataType: 'html',
        success: function(data){
            var obj = JSON.parse(data);
            if (obj.message == "Record added"){
			    toastr.success('Record successfully added!', 'Success!', {
				timeOut: 5000,
				})
            } else {
			    toastr.error(data, 'Error!', {
				timeOut: 5000
				})
            }
        }
    })
}


