<?php

require '../../../../resources/frameworks/phpspreadsheet/vendor/autoload.php';
require '../../../../resources/dbconnection.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
$objPHPExcel = $reader->load("../../../../resources/excel-templates/Passive_feedback.xlsx");
$objPHPExcel->setActiveSheetIndex(0);
$sheet = $objPHPExcel->getActiveSheet();

if (trim($_POST['passive_feedback_id']) == '') {
    $passive_feedback_id = '-';
} else {
    $passive_feedback_id = trim($_POST['passive_feedback_id']);
}

$query = mysqli_query($dbc, "SELECT * FROM passive_feedback_record WHERE passive_feedback_id='$passive_feedback_id'");

while($field_item = mysqli_fetch_array($query)){

    $sheet->SetCellValue('C4', $field_item['angebot']);
    $sheet->SetCellValue('H4', $field_item['aktion']);
    $sheet->SetCellValue('C6', $field_item['datum']);
    $datum = $field_item['datum'];
    $sheet->SetCellValue('H6', $field_item['mitarbeiter']);
    $sheet->SetCellValue('B23', $field_item['positiv_resp']);
    $sheet->SetCellValue('G23', $field_item['negativ_resp']);
    $sheet->SetCellValue('B20', $field_item['schlussfolgerungen']);
    $sheet->SetCellValue('E13', $field_item['rahmenbedingungen']);
    $sheet->SetCellValue('E16', $field_item['teilnehmer_innen']);
    $sheet->SetCellValue('J16', $field_item['fb_anzahl']);
    $sheet->SetCellValue('B16', $field_item['fb_geber_innen']);
    $sheet->SetCellValue('B10', $field_item['allgemeine_einschatzung']);
}

$writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);

ob_start();
$writer->save('php://output');
$xlsData = ob_get_contents();
ob_end_clean();
$response =  array(
    'op' => 'ok',
    'datum' => $datum,
    'file' => "data:application/vnd.ms-excel;base64,".base64_encode($xlsData)
);
die(json_encode($response));

?>