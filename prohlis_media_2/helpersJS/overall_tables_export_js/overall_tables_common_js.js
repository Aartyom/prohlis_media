$(document).ready(function(){
    var cookies = document.cookie;
    if (cookies != '') {
    } else {
        alert('You need to authorise yourself');
        // window.open("../index.php","_self");
    }
});

function get_cookie(cookie_name){
    var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );
    if (results)
        return (unescape(results[2]));
    else
        return null;
}

$('.js_record_row_delete_button').click(function(){
    var deletion_id = $("#deletion_id_input_field").val();
    var fail = '';
    if (deletion_id == null || deletion_id == "") {
        fail = "Write number of row you want to delete";
        toastr.error(fail, 'Error!', {
            timeOut: 5000
        })
    } else {
        $.ajax ({
            url: '../../../handlersPHP/overall_tables_handlers/selected_row_delete_handler.php',
            type: 'POST',
            cache: false,
            data: {'deletion_id':deletion_id,'db_table':db_table,'db_field':db_field},
            dataType: 'html',
            success: function(data){
                if (data == "RECORD DELETED") {
                    toastr.success('Record successfully deleted', 'Success!', {
                        timeOut: 5000
                    })
                    $('#deletion_id_input_field').val('');
                    // location.reload();
                } else {
                    toastr.error(data, 'Error!', {
                        timeOut: 5000
                    })
                }
            }
        })
    }
});


$('.js_main_form_export_button_2017').click(function(){

    $(".container-table").fadeOut("fast");
    setTimeout(function(){$(".container-loader").fadeIn("slow");},300);
    
    $.ajax({
        type:'POST',
        url:"../../../handlersPHP/export_handlers/main_reports/MAIN_REPORT_2017.php",
        data: {},
        dataType:'json'
    }).done(function(data){
        var $a = $("<a>");
        $a.attr("href",data.file);
        $("body").append($a);
        $a.attr("download","Statistik_17_mobile_JA.xlsx");
        $a[0].click();
        $a.remove();
        $(".container-loader").fadeOut("fast");
        setTimeout(function(){$(".container-table").fadeIn("slow");},300);
        toastr.info('Report generated and downloaded', {timeOut: 5000})
    });
});

/**
 * "Statistics" export button push event listener
 */

 $('.js_statistics_export_button').click(function(){
    $(".container-table").fadeOut("fast");
    setTimeout(function(){$(".container-loader").fadeIn("slow");},300);
    
    $.ajax({
        type:'POST',
        url:"../../../handlersPHP/export_handlers/extra_statistics/statistik_vorschlag_export_handler.php",
        data: {},
        dataType:'json'
    }).done(function(data){
        var $a = $("<a>");
        $a.attr("href",data.file);
        $("body").append($a);
        $a.attr("download","statistik_vorschlag_2017.xlsx");
        $a[0].click();
        $a.remove();
        $(".container-loader").fadeOut("fast");
        setTimeout(function(){$(".container-table").fadeIn("slow");},300);
        toastr.info('Report generated and downloaded', {timeOut: 5000})
    });
 })

 $('.js_main_form_export_button_2018').click(function(){

    $(".container-table").fadeOut("fast");
    setTimeout(function(){$(".container-loader").fadeIn("slow");},300);
    
    $.ajax({
        type:'POST',
        url:"../../../handlersPHP/export_handlers/main_reports/MAIN_REPORT_2018.php",
        data: {},
        dataType:'json'
    }).done(function(data){
        var $a = $("<a>");
        $a.attr("href",data.file);
        $("body").append($a);
        $a.attr("download","Statistik_18_mobile_JA.xlsx");
        $a[0].click();
        $a.remove();
        $(".container-loader").fadeOut("fast");
        setTimeout(function(){$(".container-table").fadeIn("slow");},300);
        toastr.info('Report generated and downloaded', {timeOut: 5000})
    });
});