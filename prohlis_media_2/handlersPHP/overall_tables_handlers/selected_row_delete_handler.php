<?php

	require_once('../../resources/dbconnection.php');

	$err = array();

	if (trim($_POST['deletion_id']) == '') {
		$err[] = 'ID';
	} else {
		$deletion_id = trim($_POST['deletion_id']);
	}

	if (trim($_POST['db_table']) == '') {
		$err[] = 'db_table';
	} else {
		$db_table = trim($_POST['db_table']);
	}

	if (trim($_POST['db_field']) == '') {
		$err[] = 'db_field';
	} else {
		$db_field = trim($_POST['db_field']);
	}

	 if (empty($err)){

		$query = "DELETE FROM $db_table WHERE $db_field ='$deletion_id'";

		mysqli_query($dbc,$query);

		$affected_rows = mysqli_affected_rows($dbc);

		if ($affected_rows == 1) {

			echo "RECORD DELETED";

		} else {

			echo 'Error with request execution';
		}

		mysqli_close($dbc);

	} else {

	 	echo 'Data missing'+$err;
	 }
?>
