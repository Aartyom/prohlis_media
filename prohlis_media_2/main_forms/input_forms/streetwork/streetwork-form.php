<?php
require('../../../resources/dbconnection.php');

$dlquery = "SELECT * FROM user";

$user_short_name1 = mysqli_query($dbc, $dlquery);
$user_short_name2 = mysqli_query($dbc, $dlquery);
$user_short_name3 = mysqli_query($dbc, $dlquery);

$platz_query = mysqli_query($dbc, "SELECT platz_id, platz_name FROM platz ORDER BY platz_name ASC");
$wetter_query = mysqli_query($dbc, "SELECT wetter_id, wetter_type FROM wetter ORDER BY wetter_type ASC");
$ausfallgrund_query = mysqli_query($dbc, "SELECT * FROM ausfallgrund ORDER BY ausfallgrund_type ASC");
$mobilitat_query = mysqli_query($dbc, "SELECT * FROM mobilitat ORDER BY mobilitat_type ASC");
$project_query = mysqli_query($dbc, "SELECT * FROM project WHERE type_of_work = 'streetwork' ORDER BY project_name ASC");
$short_name_query = mysqli_query($dbc, 'SELECT * FROM user');

$mitarbeiter1_query = mysqli_query($dbc, 'SELECT * FROM user');
?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Neue Streetwork-Datensatz</title>
        <!-- Bootstrap core CSS -->
        <link href="../../../resources/frameworks/bootstrap4/css/bootstrap.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="streetwork-stylesheet.css" rel="stylesheet">
        <link href="../../../resources/frameworks/toastr-master/build/toastr.min.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
        <!-- <script type="text/javascript" src="../resources/frameworks/toastr-master/build/toastr.min.js"></script> -->
    </head>
    <body>
        <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
            <a class="navbar-brand" href="../../main_menu/main_menu.php">PROHLIS MEDIA</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="../../main_menu/main_menu.php">Hauptmenü</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../../overall_tables/streetwork_overall/streetwork_overall.php">Tabelle der Datensätze</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" id="current_user_username" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <?php
								$username = $_COOKIE['username'];
								while ($current_user = mysqli_fetch_array($mitarbeiter1_query)) {
								 	if ($current_user['username'] == $username){
								 	 	echo $current_user['first_name']." ".$current_user['last_name'];
								 	 }
                                }
                            ?>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="dropdown01">
                            <a class="dropdown-item" href="#">Edit settings</a>
                            <a class="dropdown-item" href="../../../../../logout.php">Log out</a>
                        </div>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                </form>
            </div>
        </nav>
        <div class="container">
            <div class="row">
                <div class="w-100 text-center" style="margin:5px 0px;">
                    <h2><span class="text-uppercase">NEUE STREETWORK-DATENSATZ</span></h2>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <div class="input-group input-group-md">
                        <!-- <span class="input-group-addon justify-content-end">Wetter:</span> -->
                        <select data-default-value="defaultv" class="custom-select d-block w-100" id="project_name" name="project_name">
                            <option value="defaultv" disabled selected hidden>Choose projekt</option>
                            <option value="-">-</option>
                            <?php
					            while ($project = mysqli_fetch_array($project_query)) {
						            echo '<option value="' . $project['project_name'] . '">' . $project['project_name'] . '</option>';
					            }
					        ?>
                        </select>
                        <div class="input-group-btn">
                            <div class="input-group-btn">
                                <button class="btn btn-primary bg-grey" type="button" data-toggle="modal" data-target="#addProject">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </button>
                                <button class="btn btn-primary bg-grey" type="button" data-toggle="modal" data-target="#deleteProject">
                                    <i class="fa fa-minus" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="input-group input-group-md">
                        <input type="text" class="form-control font-size-sm" id="datum" placeholder="dd.mm.yyyy" name="datum" value="<?php echo date('d.m.o'); ?>">
                        <span class="input-group-addon justify-content-center w-50 font-size-sm">Datum</span>
                    </div>
                    <div class="input-group input-group-md input-group-uhrzeit">
                        <input id="uhrzeit" name="uhrzeit" type="text" class="form-control font-size-sm" size="10" maxlength="5" pattern="[0-9]{2}:[0-9]{2}" value="<?php echo date('H:i'); ?>" placeholder="hh:mm" required>
                        <span class="input-group-addon justify-content-center w-50 font-size-sm">Uhrzeit</span>
                    </div>
                </div>
                <div class="col-md-3">
                    <!-- <div class="input-group input-group-md">
                        <input id="mitarbeiter01" name="mitarbeiter01" type="text" class="form-control font-size-sm" placeholder="-">
                        <span class="input-group-addon justify-content-center w-40 font-size-sm">Mitarbeiter 1</span>
                    </div> -->

                    <div class="input-group input-group-sm" style="margin-bottom:0px">
                        <select id="mitarbeiter01" name="mitarbeiter01" style="margin-bottom:10px" class="custom-select d-block w-100 font-size-sm height-35 mitarbeiterSelection" required disabled>
                            <?php
                                $cookie_mitarbeiter1 = $_COOKIE['username'];
                                while ($worker1 = mysqli_fetch_array($user_short_name1)) {
                                    if ($cookie_mitarbeiter1 == $worker1['username']) {
                                    echo '<option value="'.$worker1['short_name'].'">'.$worker1['first_name']." ".$worker1['last_name'].'</option>';
                                    }
                                }
                            ?>
                        </select>
                    </div>

                    <div class="input-group input-group-sm" style="margin-bottom:0px">
                        <select id="mitarbeiter03" name="mitarbeiter03" data-default-value="defaultv" class="custom-select d-block w-100 font-size-sm height-35 mitarbeiterSelection mitarbeiter03-borders-style" required>
                            <option value="defaultv" disabled selected hidden> Choose worker </option>
                            <option value="-">-</option>
                            <?php
                                $cookie_mitarbeiter3 = $_COOKIE['username'];
                                while ($worker3 = mysqli_fetch_array($user_short_name3)) {
                                    if ($cookie_mitarbeiter3 != $worker3['username']) {
                                    echo '<option value="' . $worker3['short_name'] . '">'.$worker3['first_name']." ".$worker3['last_name'].'</option>';
                                    }
                                }
                            ?>
                        </select>
                        <span class="input-group-addon justify-content-center w-50 font-size-sm">Optional</span>
                    </div>

                    <!-- <div class="input-group input-group-md input-group-mitarbeiter2">
                        <input id="mitarbeiter2" type="text" class="form-control" placeholder="-">
                        <span class="input-group-addon justify-content-center w-40 font-size-sm">Mitarbeiter 2</span>
                    </div> -->
                </div>
                <div class="col-md-3">
                    <div class="input-group input-group-sm height-35 mitarbeiter02_red_line">
                        <select id="mitarbeiter02" name="mitarbeiter02" data-default-value="defaultv" class="custom-select d-block w-100 height-35 mitarbeiterSelection" required>
                            <option value="defaultv" disabled selected hidden> Choose worker </option>
                            <?php
                                $cookie_mitarbeiter2 = $_COOKIE['username'];
                                while ($worker2 = mysqli_fetch_array($user_short_name2)) {
                                    if ($cookie_mitarbeiter2 != $worker2['username']) {
                                    echo '<option value="' . $worker2['short_name'] . '">'.$worker2['first_name']." ".$worker2['last_name'].'</option>';
                                    }
                                }
                            ?>
                        </select>
                        <div class="input-group-btn">
                            <div class="input-group-btn">
                                <button class="btn btn-primary bg-grey" type="button" data-toggle="modal" data-target="#addMitarbeiter">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </button>
                                <button class="btn btn-primary bg-grey" type="button" data-toggle="modal" data-target="#deleteMitarbeiter">
                                    <i class="fa fa-minus" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-4 text-center">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group input-group-sm people-sum-check">
                                <input type="text" class="form-control not-ausfallgrund bekannt-color" id="n_bekannt" name="n_bekannt" size="4" placeholder="-">
                                <span class="input-group-addon justify-content-center">Bekannt</span>
                            </div>
                            <div class="input-group input-group-sm people-sum-check">
                                <input type="text" class="form-control not-ausfallgrund bekannt-color" id="n_unbekannt" name="n_unbekannt" size="4" placeholder="-">
                                <span class="input-group-addon justify-content-center">Unekannt</span>
                            </div>
                            <div class="input-group input-group-sm people-sum-check">
                                <input type="text" class="form-control not-ausfallgrund bekannt-color" id="n_u12_m" name="n_u12_m" size="4" placeholder="-">
                                <span class="input-group-addon justify-content-center">unter 12(m)</span>
                            </div>
                            <div class="input-group input-group-sm people-sum-check">
                                <input type="text" class="form-control not-ausfallgrund bekannt-color" id="n_u12_w" name="n_u12_w" size="4" placeholder="-">
                                <span class="input-group-addon justify-content-center">unter 12(w)</span>
                            </div>
                            <div class="input-group input-group-sm people-sum-check">
                                <input type="text" class="form-control not-ausfallgrund bekannt-color" id="n_u14_m" name="n_u14_m" size="4" placeholder="-">
                                <span class="input-group-addon justify-content-center">12-14(m)</span>
                            </div>
                            <div class="input-group input-group-sm people-sum-check">
                                <input type="text" class="form-control not-ausfallgrund bekannt-color" id="n_u14_w" name="n_u14_w" size="4" placeholder="-">
                                <span class="input-group-addon justify-content-center">12-14(w)</span>
                            </div>
                            <div class="input-group input-group-sm people-sum-check">
                                <input type="text" class="form-control not-ausfallgrund bekannt-color" id="n_u14_17_m" name="n_u14_17_m" size="4" placeholder="-">
                                <span class="input-group-addon justify-content-center">14-17(m)</span>
                            </div>
                            <div class="input-group input-group-sm people-sum-check">
                                <input type="text" class="form-control not-ausfallgrund bekannt-color" id="n_u14_17_w" name="n_u14_17_w" size="4" placeholder="-">
                                <span class="input-group-addon justify-content-center">14-17(w)</span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group input-group-sm people-sum-check">
                                <input type="text" class="form-control not-ausfallgrund bekannt-color" id="n_u18_20_m" name="n_u18_20_m" size="4" placeholder="-">
                                <span class="input-group-addon justify-content-center">18-20(m)</span>
                            </div>
                            <div class="input-group input-group-sm people-sum-check">
                                <input type="text" id="n_u18_20_w" name="n_u18_20_w" size="4" class="form-control not-ausfallgrund bekannt-color" placeholder="-">
                                <span class="input-group-addon justify-content-center">18-20(w)</span>
                            </div>
                            <div class="input-group input-group-sm people-sum-check">
                                <input type="text" class="form-control not-ausfallgrund bekannt-color" id="n_u21_24_m" name="n_u21_24_m" size="4" placeholder="-">
                                <span class="input-group-addon justify-content-center">21-24(m)</span>
                            </div>
                            <div class="input-group input-group-sm people-sum-check">
                                <input type="text" class="form-control not-ausfallgrund bekannt-color" id="n_u21_24_w" name="n_u21_24_w" size="4" placeholder="-">
                                <span class="input-group-addon justify-content-center">21-24(w)</span>
                            </div>
                            <div class="input-group input-group-sm people-sum-check">
                                <input type="text" class="form-control not-ausfallgrund bekannt-color" id="n_u25_26_m" name="n_u25_26_m" size="4" placeholder="-">
                                <span class="input-group-addon justify-content-center">25-26(m)</span>
                            </div>
                            <div class="input-group input-group-sm people-sum-check">
                                <input type="text" class="form-control not-ausfallgrund bekannt-color" id="n_u25_26_w" name="n_u25_26_w" size="4" placeholder="-">
                                <span class="input-group-addon justify-content-center">25-26(w)</span>
                            </div>
                            <div class="input-group input-group-sm people-sum-check">
                                <input type="text" class="form-control not-ausfallgrund bekannt-color" id="n_o27_m" name="n_o27_m" size="4" placeholder="-">
                                <span class="input-group-addon justify-content-center">über 27(m)</span>
                            </div>
                            <div class="input-group input-group-sm people-sum-check">
                                <input type="text" class="form-control not-ausfallgrund bekannt-color" id="n_o27_w" name="n_o27_w" size="4" placeholder="-">
                                <span class="input-group-addon justify-content-center">über 27(w)</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-check">
                                <label class="custom-control custom-checkbox">
                                    <input class="custom-control-input not-ausfallgrund" id="th_sucht" name="th_sucht" type="checkbox">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description font-size-sm">Sucht</span>
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="custom-control custom-checkbox">
                                    <input class="custom-control-input not-ausfallgrund" type="checkbox" id="th_gewalt" name="th_gewalt">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description font-size-sm">Gewalt</span>
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="custom-control custom-checkbox">
                                    <input class="custom-control-input not-ausfallgrund" type="checkbox" id="th_kriminalitat" name="th_kriminalitat">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description font-size-sm">Kriminalitat</span>
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="custom-control custom-checkbox">
                                    <input class="custom-control-input not-ausfallgrund" id="th_gesundheit" name="th_gesundheit" type="checkbox">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description font-size-sm">Gesundheit</span>
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="custom-control custom-checkbox">
                                    <input class="custom-control-input not-ausfallgrund" type="checkbox" id="th_freizeit" name="th_freizeit">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description font-size-sm">Freizeit</span>
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="custom-control custom-checkbox">
                                    <input class="custom-control-input not-ausfallgrund" type="checkbox" id="th_identitat" name="th_identitat">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description font-size-sm">Identitat</span>
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="custom-control custom-checkbox">
                                    <input class="custom-control-input not-ausfallgrund" id="th_wohnraum" name="th_wohnraum" type="checkbox">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description font-size-sm">Wohnraum</span>
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="custom-control custom-checkbox">
                                    <input class="custom-control-input not-ausfallgrund" type="checkbox" id="th_familie" name="th_familie">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description font-size-sm">Familie</span>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-check">
                                <label class="custom-control custom-checkbox">
                                    <input class="custom-control-input not-ausfallgrund" type="checkbox" id="th_behorden" name="th_behorden">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description font-size-sm">Behorden/Amter</span>
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="custom-control custom-checkbox">
                                    <input class="custom-control-input not-ausfallgrund" type="checkbox" id="th_finanzielle" name="th_finanzielle">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description font-size-sm">Finanzielle Situation</span>
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="custom-control custom-checkbox">
                                    <input class="custom-control-input not-ausfallgrund" type="checkbox" id="th_arbeit" name="th_arbeit">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description font-size-sm">Arbeit/Ausbildung/Schule</span>
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="custom-control custom-checkbox">
                                    <input class="custom-control-input not-ausfallgrund" type="checkbox" id="th_beziehung" name="th_beziehung">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description font-size-sm">Beziehung/Sexualitat</span>
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="custom-control custom-checkbox">
                                    <input class="custom-control-input not-ausfallgrund" type="checkbox" id="th_politische_themen" name="th_politische_themen">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description font-size-sm">Politische Themen</span>
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="custom-control custom-checkbox">
                                    <input class="custom-control-input not-ausfallgrund" type="checkbox" id="th_digitales" name="th_digitales">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description font-size-sm">Digitales</span>
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="custom-control custom-checkbox">
                                    <input class="custom-control-input not-ausfallgrund" type="checkbox" id="th_vorstellung" name="th_vorstellung">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description font-size-sm">Vorstellung</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 text-center">
                    <div class="input-group input-group-sm platz_red_line">
                        <!-- <span class="input-group-addon justify-content-end">Platz:</span> -->
                        <select name="platz" id="platz" class="custom-select d-block w-100 not-ausfallgrund" data-default-value="defaultv" required>
                            <option value="defaultv" disabled selected hidden>Choose platz</option>
                            <?php
                                while ($platz = mysqli_fetch_array($platz_query)){
                                    echo '<option value="'.$platz['platz_name'].'">'. $platz['platz_name'] .'</option>';
                                }
                            ?>
                        </select>
                        <div class="input-group-btn">
                            <div class="input-group-btn">
                                <button class="btn btn-primary bg-grey" type="button" data-toggle="modal" data-target="#addPlatz">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </button>
                                <button class="btn btn-primary bg-grey" type="button" data-toggle="modal" data-target="#deletePlatz">
                                    <i class="fa fa-minus" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                    </div>

                    <div class="input-group input-group-sm wetter_red_line">
                        <select name="wetter" id="wetter" class="custom-select d-block w-100 not-ausfallgrund" required>
                            <option value="defaultv" disabled selected hidden>Choose wetter</option>
                            <?php
                                while ($wetter = mysqli_fetch_array($wetter_query)) {
                                    echo '<option value="'.$wetter['wetter_type'].'">'.$wetter['wetter_type'].'</option>';
                                }
                            ?>
                        </select>
                        <div class="input-group-btn">
                            <div class="input-group-btn">
                                <button class="btn btn-primary bg-grey" type="button" data-toggle="modal" data-target="#addWetter">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </button>
                                <button class="btn btn-primary bg-grey" type="button" data-toggle="modal" data-target="#deleteWetter">
                                    <i class="fa fa-minus" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    
                    <div class="input-group input-group-sm">
                        <select name="ausfallgrund" id="ausfallgrund" data-default-value="defaultv" class="custom-select d-block w-100" required>
                            <option value="defaultv" disabled selected hidden>Choose ausfallgrund</option>
                            <option value="-">-</option>
                            <?php
                                while ($ausfallgrund = mysqli_fetch_array($ausfallgrund_query)) {
                                    echo '<option value="'.$ausfallgrund['ausfallgrund_type'].'">'.$ausfallgrund['ausfallgrund_type'].'</option>';
                                }
                            ?>
                        </select>
                        <div class="input-group-btn">
                            <div class="input-group-btn">
                                <button class="btn btn-primary bg-grey" type="button" data-toggle="modal" data-target="#addAusfallgrund">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </button>
                                <button class="btn btn-primary bg-grey" type="button" data-toggle="modal" data-target="#deleteAusfallgrund">
                                    <i class="fa fa-minus" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    
                    <div class="input-group input-group-sm mobilitat_red_line">
                        <select name="mobilitat" id="mobilitat" data-default-value="defaultv" class="custom-select d-block w-100 not-ausfallgrund" required>
                            <option value="defaultv" disabled selected hidden>Choose mobilitat</option>
                            <?php
                                while ($mobilitat = mysqli_fetch_array($mobilitat_query)) {
                                    echo '<option value="'.$mobilitat['mobilitat_type'].'">'.$mobilitat['mobilitat_type'].'</option>';
                                }
                            ?>
                        </select>
                        <div class="input-group-btn">
                            <div class="input-group-btn">
                                <button class="btn btn-primary bg-grey" type="button" data-toggle="modal" data-target="#addMobilitat">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </button>
                                <button class="btn btn-primary bg-grey" type="button" data-toggle="modal" data-target="#deleteMobilitat">
                                    <i class="fa fa-minus" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    
                    <div class="input-group">
                        <input type="text" class="form-control font-size-sm not-ausfallgrund" placeholder="Platzwahrnehmung" id="platzwahrnehmung" name="platzwahrnehmung">
                    </div>
                    <div class="input-group">
                            <input type="text" class="form-control font-size-sm not-ausfallgrund" placeholder="Sonstiges" id="th_sonstiges" name="th_sonstiges">
                        </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <!-- <h3>Column title</h3> -->
                    <div class="form-group" style="margin-bottom:0px">
                        <!-- <label for="formInput153">Anmerkungen:</label> -->
                        <textarea class="form-control font-size-sm" rows="3" id="anmerkungen" name="anmerkungen" placeholder="Anmerkungen.."></textarea>
                    </div>
                </div>
                <div class="col-md-6 d-flex align-items-center justify-content-center">
                    <!-- <h3>Column title</h3> -->
                    <div role="group" aria-label="Basic example" class="btn-group btn-group-lg">
                        <button type="button" class="btn btn-secondary trans-up" data-toggle="modal" data-target="#deleteLastRecord">rückgängig</button>
                        <button type="button" class="btn btn-secondary trans-up js_main_form_submit_button">Einreichen</button>
                        <button type="button" class="btn btn-secondary trans-up js_main_form_new_record_button">neuer Rekord</button>
                    </div>
                </div>
            </div>
        
            <!-- Modal projekt -->
            <div class="modal fade" id="addProject" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header text-center modal-header-custom">
                            <h3 class="modal-title w-100" id="exampleModalLabel">NEUE projekt</h3>
                        </div>
                        <div class="modal-body modal-body-custom">
                                <input type="text" name="project_name_subform_input_field" class="form-control" id="project_name_subform_input_field" placeholder="Project type..">
                        </div>
                        <div class="modal-footer d-flex justify-content-center modal-footer-custom">
                            <div role="group" aria-label="Basic example" class="btn-group btn-group-md">
                                <button type="button" class="btn btn-secondary trans-up" data-dismiss="modal">Abbrechen</button>
                                <button type="button" class="btn btn-secondary trans-up js_project_subform_submit_button">Einreichen</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal mitarbeiter -->
            <div class="modal fade" id="addMitarbeiter" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header text-center modal-header-custom">
                                <h3 class="modal-title w-100" id="exampleModalLabel">NEUE mitarbeiter</h3>
                            </div>
                            <div class="modal-body modal-body-custom">
                            <div class="row mitarbeiter-subform-row">
                                <div class="col">
                                    <p>To add new user, please fill in the selected fields, if you need to define person without access to the system, please select the "worker" option <div class="form-check">
                                            <label class="custom-control custom-checkbox">
                                                <input class="custom-control-input" type="checkbox" id="worker_mitarbeiter_check" name="worker_mitarbeiter_check">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description font-size-sm">WORKER</span>
                                            </label>
                                        </div></p>
                                </div>
                            </div>
                                <div class="row mitarbeiter-subform-row">
                                    <div class="col">
                                    <div class="form-group">
                                        <label for="mitarbeiter_username">Username</label>
                                        <input type="text" class="form-control" id="mitarbeiter_username" name="mitarbeiter_username" placeholder="-">
                                    </div>
                                    </div>
                                </div>
                                <div class="row mitarbeiter-subform-row">
                                    <div class="col">
                                    <div class="form-group">
                                        <label for="mitarbeiter_password">Password</label>
                                        <input type="password" class="form-control" id="mitarbeiter_password" name="mitarbeiter_password" placeholder="-">
                                    </div>
                                    </div>
                                    <div class="col">
                                    <div class="form-group">
                                        <label for="mitarbeiter_repeat_password">Repeat password</label>
                                        <input type="password" class="form-control" id="mitarbeiter_repeat_password" name="mitarbeiter_repeat_password" placeholder="-">
                                    </div>
                                    </div>
                                </div>
                                <div class="row mitarbeiter-subform-row">
                                    <div class="col">
                                    <div class="form-group">
                                        <label for="arbeiter_first_name">First name</label>
                                        <input type="text" class="form-control" id="arbeiter_first_name" name="arbeiter_first_name" placeholder="-">
                                    </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="arbeiter_last_name">Second name</label>
                                            <input type="text" class="form-control" id="arbeiter_last_name" name="arbeiter_last_name" placeholder="-">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer d-flex justify-content-center modal-footer-custom">
                                <div role="group" aria-label="Basic example" class="btn-group btn-group-md">
                                    <button type="button" class="btn btn-secondary trans-up" data-dismiss="modal">Abbrechen</button>
                                    <button type="button" class="btn btn-secondary trans-up js_mitarbeiter_subform_submit_button">Einreichen</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            <!-- Modal platz -->
            <div class="modal fade" id="addPlatz" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header text-center modal-header-custom">
                            <h3 class="modal-title w-100" id="exampleModalLabel">NEUE platz</h3>
                        </div>
                        <div class="modal-body modal-body-custom">
                            <input type="text" name="platz_name" class="form-control" id="platz_name" placeholder="Platz name..">
                            <div class="input-group input-group-sm stadtteil_red_line">
                                <select name="stadtteil_name" id="stadtteil_name" data-default-value="defaultv" class="custom-select d-block w-100" required>
                                    <option value="defaultv" disabled selected hidden>Choose stadtteil</option>
                                    <?php
                                        $stadtteil_name_query = mysqli_query($dbc, 'SELECT * FROM stadtteil');
                                        while ($stadtteil_name_array = mysqli_fetch_array($stadtteil_name_query)) {
                                            echo '<option value="'.$stadtteil_name_array['stadtteil_name'].'">'.$stadtteil_name_array['stadtteil_name'].'</option>';
                                        }
						            ?>
                                </select>
                                <div class="input-group-btn">
                                    <div class="input-group-btn">
                                        <button class="btn btn-primary bg-grey" type="button" data-toggle="modal" data-target="#addStadtteil">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                        </button>
                                        <button class="btn btn-primary bg-grey" type="button" data-toggle="modal" data-target="#deleteStadtteil">
                                            <i class="fa fa-minus" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer d-flex justify-content-center modal-footer-custom">
                            <div role="group" aria-label="Basic example" class="btn-group btn-group-md">
                                <button type="button" class="btn btn-secondary trans-up" data-dismiss="modal">Abbrechen</button>
                                <button type="button" class="btn btn-secondary trans-up js_platz_subform_submit_button">Einreichen</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal wetter -->
            <div class="modal fade" id="addWetter" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header text-center modal-header-custom">
                            <h3 class="modal-title w-100" id="exampleModalLabel">NEUE wetter</h3>
                        </div>
                        <div class="modal-body modal-body-custom">
                                <input type="text" name="wetter_type" class="form-control" id="wetter_type" placeholder="Wetter type..">
                        </div>
                        <div class="modal-footer d-flex justify-content-center modal-footer-custom">
                            <div role="group" aria-label="Basic example" class="btn-group btn-group-md">
                                <button type="button" class="btn btn-secondary trans-up" data-dismiss="modal">Abbrechen</button>
                                <button type="button" class="btn btn-secondary trans-up js_wetter_subform_submit_button">Einreichen</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal ausfallgrund -->
            <div class="modal fade" id="addAusfallgrund" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header text-center modal-header-custom">
                            <h3 class="modal-title w-100" id="exampleModalLabel">NEUE ausfallgrund</h3>
                        </div>
                        <div class="modal-body modal-body-custom">
                                <input type="text" name="ausfallgrund_type" class="form-control" id="ausfallgrund_type" placeholder="Ausfallgrund type..">
                        </div>
                        <div class="modal-footer d-flex justify-content-center modal-footer-custom">
                            <div role="group" aria-label="Basic example" class="btn-group btn-group-md">
                                <button type="button" class="btn btn-secondary trans-up" data-dismiss="modal">Abbrechen</button>
                                <button type="button" class="btn btn-secondary trans-up js_ausfallgrund_subform_submit_button">Einreichen</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal mobilitat -->
            <div class="modal fade" id="addMobilitat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header text-center modal-header-custom">
                            <h3 class="modal-title w-100" id="exampleModalLabel">NEUE mobilitat</h3>
                        </div>
                        <div class="modal-body modal-body-custom">
                                <input type="text" name="mobilitat_type" class="form-control" id="mobilitat_type" placeholder="Mobilitat type..">
                        </div>
                        <div class="modal-footer d-flex justify-content-center modal-footer-custom">
                            <div role="group" aria-label="Basic example" class="btn-group btn-group-md">
                                <button type="button" class="btn btn-secondary trans-up" data-dismiss="modal">Abbrechen</button>
                                <button type="button" class="btn btn-secondary trans-up js_mobilitat_subform_submit_button">Einreichen</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal stadtteil -->
            <div class="modal fade" id="addStadtteil" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header text-center modal-header-custom">
                            <h3 class="modal-title w-100" id="exampleModalLabel">NEUE stadtteil</h3>
                        </div>
                        <div class="modal-body modal-body-custom">
                            <input type="text" name="stadtteil_name_field" class="form-control" id="stadtteil_name_field" placeholder="Stadtteil name..">
                        </div>
                        <div class="modal-footer d-flex justify-content-center modal-footer-custom">
                            <div role="group" aria-label="Basic example" class="btn-group btn-group-md">
                                <button type="button" class="btn btn-secondary trans-up" data-dismiss="modal">Abbrechen</button>
                                <button type="button" class="btn btn-secondary trans-up js_stadtteil_subform_submit_button">Einreichen</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--**************************************** Deletion modal forms ****************************************-->
            
            <!-- Delete last record -->
            <div class="modal fade" id="deleteLastRecord" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header text-center modal-header-custom">
                            <h3 class="modal-title w-100" id="exampleModalLabel">Achtung</h3>
                        </div>
                        <div class="modal-body modal-body-custom text-center">
                            <span>Sind Sie sicher, dass Sie löschen möchten</span>
                        </div>
                        <div class="modal-footer d-flex justify-content-center modal-footer-custom">
                            <div role="group" aria-label="Basic example" class="btn-group btn-group-md">
                                <button type="button" class="btn btn-secondary trans-up" data-dismiss="modal" style="background-color: rgba(255, 0, 0, 0.7);" data-dismiss="modal">Abbrechen</button>
                                <button type="button" class="btn btn-secondary trans-up js_revert_button" style="background-color:rgba(0, 128, 0, 0.63);">Einreichen</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Delete projekt -->
            <div class="modal fade" id="deleteProject" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header text-center modal-header-custom">
                            <h3 class="modal-title w-100" id="exampleModalLabel">Achtung</h3>
                        </div>
                        <div class="modal-body modal-body-custom text-center">
                            <span>Sind Sie sicher, dass Sie löschen möchten</span>
                        </div>
                        <div class="modal-footer d-flex justify-content-center modal-footer-custom">
                            <div role="group" aria-label="Basic example" class="btn-group btn-group-md">
                                <button type="button" class="btn btn-secondary trans-up" data-dismiss="modal" style="background-color: rgba(255, 0, 0, 0.7);" data-dismiss="modal">Abbrechen</button>
                                <button type="button" class="btn btn-secondary trans-up js_btn_delete_project" style="background-color:rgba(0, 128, 0, 0.63);">Einreichen</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Delete mitarbeiter -->
            <div class="modal fade" id="deleteMitarbeiter" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header text-center modal-header-custom">
                            <h3 class="modal-title w-100" id="exampleModalLabel">Achtung</h3>
                        </div>
                        <div class="modal-body modal-body-custom text-center">
                            <span>Sind Sie sicher, dass Sie löschen möchten</span>
                        </div>
                        <div class="modal-footer d-flex justify-content-center modal-footer-custom">
                            <div role="group" aria-label="Basic example" class="btn-group btn-group-md">
                                <button type="button" class="btn btn-secondary trans-up" data-dismiss="modal" style="background-color: rgba(255, 0, 0, 0.7);">Abbrechen</button>
                                <button type="button" class="btn btn-secondary trans-up js_btn_delete_mitarbeiter" style="background-color:rgba(0, 128, 0, 0.63);">Einreichen</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Delete platz -->
            <div class="modal fade" id="deletePlatz" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header text-center modal-header-custom">
                            <h3 class="modal-title w-100" id="exampleModalLabel">Achtung</h3>
                        </div>
                        <div class="modal-body modal-body-custom text-center">
                            <span>Sind Sie sicher, dass Sie löschen möchten</span>
                        </div>
                        <div class="modal-footer d-flex justify-content-center modal-footer-custom">
                            <div role="group" aria-label="Basic example" class="btn-group btn-group-md">
                                <button type="button" class="btn btn-secondary trans-up" data-dismiss="modal" style="background-color: rgba(255, 0, 0, 0.7);">Abbrechen</button>
                                <button type="button" class="btn btn-secondary trans-up js_btn_delete_platz" style="background-color:rgba(0, 128, 0, 0.63);">Einreichen</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Delete wetter -->
            <div class="modal fade" id="deleteWetter" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header text-center modal-header-custom">
                            <h3 class="modal-title w-100" id="exampleModalLabel">Achtung</h3>
                        </div>
                        <div class="modal-body modal-body-custom text-center">
                            <span>Sind Sie sicher, dass Sie löschen möchten</span>
                        </div>
                        <div class="modal-footer d-flex justify-content-center modal-footer-custom">
                            <div role="group" aria-label="Basic example" class="btn-group btn-group-md">
                                <button type="button" class="btn btn-secondary trans-up" data-dismiss="modal" style="background-color: rgba(255, 0, 0, 0.7);">Abbrechen</button>
                                <button type="button" class="btn btn-secondary trans-up js_btn_delete_wetter" style="background-color:rgba(0, 128, 0, 0.63);">Einreichen</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Delete ausfallgrund -->
            <div class="modal fade" id="deleteAusfallgrund" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header text-center modal-header-custom">
                            <h3 class="modal-title w-100" id="exampleModalLabel">Achtung</h3>
                        </div>
                        <div class="modal-body modal-body-custom text-center">
                            <span>Sind Sie sicher, dass Sie löschen möchten</span>
                        </div>
                        <div class="modal-footer d-flex justify-content-center modal-footer-custom">
                            <div role="group" aria-label="Basic example" class="btn-group btn-group-md">
                                <button type="button" class="btn btn-secondary trans-up" data-dismiss="modal" style="background-color: rgba(255, 0, 0, 0.7);">Abbrechen</button>
                                <button type="button" class="btn btn-secondary trans-up js_btn_delete_ausfallgrund" style="background-color:rgba(0, 128, 0, 0.63);">Einreichen</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Delete mobilitat -->
            <div class="modal fade" id="deleteMobilitat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header text-center modal-header-custom">
                            <h3 class="modal-title w-100" id="exampleModalLabel">Achtung</h3>
                        </div>
                        <div class="modal-body modal-body-custom text-center">
                            <span>Sind Sie sicher, dass Sie löschen möchten</span>
                        </div>
                        <div class="modal-footer d-flex justify-content-center modal-footer-custom">
                            <div role="group" aria-label="Basic example" class="btn-group btn-group-md">
                                <button type="button" class="btn btn-secondary trans-up" data-dismiss="modal" style="background-color: rgba(255, 0, 0, 0.7);">Abbrechen</button>
                                <button type="button" class="btn btn-secondary trans-up js_btn_delete_mobilitat" style="background-color:rgba(0, 128, 0, 0.63);">Einreichen</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <!-- ************************************************************************************************************************* -->

        </div>

        <!-- Bootstrap core JavaScript
    ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="../../../resources/frameworks/assets4/js/jquery.min.js"></script>
        <script src="../../../resources/frameworks/assets4/js/popper.js"></script>
        <script src="../../../resources/frameworks/bootstrap4/js/bootstrap.min.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="../../../resources/frameworks/assets4/js/ie10-viewport-bug-workaround.js"></script>
        <!-- Notifications framework "Toastr" -->
        <script type="text/javascript" src="../../../resources/frameworks/toastr-master/build/toastr.min.js"></script>
        <!-- Custom scripts -->
        <script src="../../../helpersJS/subforms_add_listeners_js.js"></script>
        <script src="../../../helpersJS/subforms_remove_listeners_js.js"></script>
        <script src="../../../helpersJS/common_js_functions.js"></script>
        <script src="streetworkJS.js"></script>
    </body>
</html>