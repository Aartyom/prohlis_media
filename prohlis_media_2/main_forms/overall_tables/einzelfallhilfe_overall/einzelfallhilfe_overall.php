<?php

require('../../../resources/dbconnection.php');

$query="SELECT * FROM einzelfallhilfe_record";
$response = @mysqli_query($dbc, $query);
$short_name_query = mysqli_query($dbc, 'SELECT * FROM user');

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Einzelfallhilfe Overall</title>
        <!-- Bootstrap core CSS -->
        <link href="../../../resources/frameworks/bootstrap4/css/bootstrap.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="einzelfallhilfe-overall-stylesheet.css" rel="stylesheet">
        <link href="../common_files/loader-stylesheet.css" rel="stylesheet">
        <link href="../../../resources/frameworks/toastr-master/build/toastr.min.css" rel="stylesheet">
        <link href="../../../resources/frameworks/fixed-header-table/css/defaultTheme.css" rel="stylesheet">
        <!-- <link href="../common_files/myTheme.css" rel="stylesheet"> -->
        <link href="../common_files/custom-table-style.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
        <!-- <script type="text/javascript" src="../resources/frameworks/toastr-master/build/toastr.min.js"></script> -->
    </head>
    <body>
        <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
            <a class="navbar-brand" href="../../main_menu/main_menu.php">PROHLIS MEDIA</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="../../main_menu/main_menu.php">Hauptmenü</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../../input_forms/einzelfallhilfe/einzelfallhilfe-form.php">Return to insertion form</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" id="current_user_username" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <?php
                            $username = $_COOKIE['username'];
                            while ($current_user = mysqli_fetch_array($short_name_query)) {
                                if ($current_user['username'] == $username){
                                    echo $current_user['first_name']." ".$current_user['last_name'];
                                }
                            }
                            ?>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="dropdown01">
                            <a class="dropdown-item" href="#">Edit settings</a>
                            <a class="dropdown-item" href="../../../../../logout.php">Log out</a>
                        </div>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                </form>
            </div>
        </nav>

        <div class="container container-table">
            <div class="row">
                <div class="col">
                    <div class="w-100 text-center">
                        <h2><span class="text-uppercase">Einzelfallhilfe OVERALL</span></h2>
                    </div>
                </div>
            </div>
            <div class="row" id="table-row-custom">
                <div class="col">
                    <div class="table-container w-100">
                        <table class="fancyTable table" id="myTable01">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Project</th>
                                    <th>Datum</th>
                                    <th>Dauer_in_h</th>
                                    <th>Mitarbeiter 1</th>
                                    <th>Mitarbeiter 2</th>
                                    <th>Mitarbeiter 3</th>
                                    <th>n_u12_m</th>
                                    <th>n_u12_w</th>
                                    <th>n_u14_m</th>
                                    <th>n_u14_w</th>
                                    <th>n_u14_17_m</th>
                                    <th>n_u14_17_w</th>
                                    <th>n_u18_20_m</th>
                                    <th>n_u18_20_w</th>
                                    <th>n_u21_24_m</th>
                                    <th>n_u21_24_w</th>
                                    <th>n_u25_26_m</th>
                                    <th>n_u25_26_w</th>
                                    <th>n_o27_m</th>
                                    <th>n_o27_w</th>
                                    <th>th_sucht</th>
                                    <th>th_gewalt</th>
                                    <th>th_kriminalitat</th>
                                    <th>th_gesundheit</th>
                                    <th>th_freizeit</th>
                                    <th>th_identitat</th>
                                    <th>th_wohnraum</th>
                                    <th>th_familie</th>
                                    <th>th_behorden</th>
                                    <th>th_finanzielle</th>
                                    <th>th_arbeit</th>
                                    <th>th_beziehung</th>
                                    <th>th_politische_themen</th>
                                    <th>th_digitales</th>
                                    <th>th_vorstellung</th>
                                    <th>th_sonstiges</th>
                                    <th>anmerkungen</th>
                                </tr>
                            </thead>
                                <tbody>
                                <?php while($row = mysqli_fetch_array($response)){
                                    echo '

        							<tr>
                                        <th>'. $row['einzelfallhilfe_record_id']. '</th>
                                        <td>'. $row['project_name']. '</td>
                                        <td>'. $row['datum']. '</td>
                                        <td>'. $row['dauer_in_h']. '</td>
                                        <td>'. $row['mitarbeiter01']. '</td>
                                        <td>'. $row['mitarbeiter02']. '</td>
                                        <td>'. $row['mitarbeiter03']. '</td>
                                        <td>'. $row['n_u12_m']. '</td>
                                        <td>'. $row['n_u12_w']. '</td>
                                        <td>'. $row['n_u14_m']. '</td>
                                        <td>'. $row['n_u14_w']. '</td>
                                        <td>'. $row['n_u14_17_m']. '</td>
                                        <td>'. $row['n_u14_17_w']. '</td>
                                        <td>'. $row['n_u18_20_m']. '</td>
                                        <td>'. $row['n_u18_20_w']. '</td>
                                        <td>'. $row['n_u21_24_m']. '</td>
                                        <td>'. $row['n_u21_24_w']. '</td>
                                        <td>'. $row['n_u25_26_m']. '</td>
                                        <td>'. $row['n_u25_26_w']. '</td>
                                        <td>'. $row['n_o27_m']. '</td>
                                        <td>'. $row['n_o27_w']. '</td>
                                        <td>'. $row['th_sucht']. '</td>
                                        <td>'. $row['th_gewalt']. '</td>
                                        <td>'. $row['th_kriminalitat']. '</td>
                                        <td>'. $row['th_gesundheit']. '</td>
                                        <td>'. $row['th_freizeit']. '</td>
                                        <td>'. $row['th_identitat']. '</td>
                                        <td>'. $row['th_wohnraum']. '</td>
                                        <td>'. $row['th_familie']. '</td>
                                        <td>'. $row['th_behorden']. '</td>
                                        <td>'. $row['th_finanzielle']. '</td>
                                        <td>'. $row['th_arbeit']. '</td>
                                        <td>'. $row['th_beziehung']. '</td>
                                        <td>'. $row['th_politische_themen']. '</td>
                                        <td>'. $row['th_digitales']. '</td>
                                        <td>'. $row['th_vorstellung']. '</td>
                                        <td>'. $row['th_sonstiges']. '</td>
                                        <td>'. $row['anmerkungen']. '</td>
                                    </tr>';

                                } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group w-50">
                        <label for="mitarbeiter_username">Remove record:</label>
                        <div class="input-group input-group-md platz_red_line">
                            <input type="text" class="form-control" id="deletion_id_input_field" name="deletion_id_input_field" placeholder="ID of record">
                            <div class="input-group-btn">
                                <div class="input-group-btn">
                                    <button class="btn btn-primary bg-grey js_record_row_delete_button" type="button" data-toggle="modal" data-target="#addPlatz">
                                        <span class="trans-up">delete</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group w-50">
                        <label for="mitarbeiter_username">Fill in the final report:</label>
                        <div role="group" aria-label="Basic example" class="btn-group btn-group-md">
                            <button type="button" class="btn btn-secondary trans-up js_main_form_export_button_2017">export 2017</button>
                            <button type="button" class="btn btn-secondary trans-up js_main_form_export_button_2018">export 2018</button>
                            <button type="button" class="btn btn-secondary trans-up js_statistics_export_button">statistics</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container container-loader" id="loader-popup">
            <div class="row">
                <div class="col-md-12">
                    <h5 class="text-center mt-5 mb-5">Please do not close this window <br> until the file will be loaded..</h5>
                    <div class="middle">
                        <div class="sk-folding-cube">
                            <div class="sk-cube1 sk-cube"></div>
                            <div class="sk-cube2 sk-cube"></div>
                            <div class="sk-cube4 sk-cube"></div>
                            <div class="sk-cube3 sk-cube"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="../../../resources/frameworks/assets4/js/jquery.min.js"></script>
        <script src="../../../resources/frameworks/assets4/js/popper.js"></script>
        <script src="../../../resources/frameworks/bootstrap4/js/bootstrap.min.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="../../../resources/frameworks/assets4/js/ie10-viewport-bug-workaround.js"></script>
        <!-- Notifications framework "Toastr" -->
        <script type="text/javascript" src="../../../resources/frameworks/toastr-master/build/toastr.min.js"></script>
        <!-- Fixed-header-table framework -->
        <script type="text/javascript" src="../../../resources/frameworks/fixed-header-table/jquery.fixedheadertable.js"></script>
        <!-- Custom scripts -->
        <script src="../../../helpersJS/overall_tables_export_js/overall_tables_common_js.js"></script>
        <script src="einzelfallhilfe-overall-js.js"></script>
    </body>
</html>
