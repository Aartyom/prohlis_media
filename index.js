
$('#start_btn').click(function(){
    var cookies = document.cookie;
    if (cookies == '') {
        $('.auth_popup_overlay').fadeIn();
        $('main_body').css('filter','blur(5px)');
        $('.auth_popup_overlay').addClass('disabled');
    } else {
        window.open("prohlis_media_2/main_forms/main_menu/main_menu.php","_self");
    }
});

// ------------ Script for opening hidden auth form --------------

$('.js-open-button-popup').click(function() {
    $('.auth_popup_overlay').fadeIn();
    $('main_body').css('filter','blur(5px)');
    $('.auth_popup_overlay').addClass('disabled');
});

// ---------------------------------------------------------------

// закрыть на крестик
$('.js-close-button-popup').click(function() {
    $('.auth_popup_overlay').fadeOut();
    $('main_body').css('filter','none');
});

// закрыть по клику вне окна
$(document).mouseup(function (e) {
    var popup = $('.auth_popup_content');
    if (e.target!=popup[0]&&popup.has(e.target).length === 0){
        $('.auth_popup_overlay').fadeOut();

    }
});

$('#log_in').click(function(){
    var login = $('#login').val();
    var password = $('#password').val();
    var fail = "";
    if (login == "") fail = 'Write login';
    else if (password == "") fail = 'Write password';
    if (fail != "") {
        toastr.error(response.message, 'Error!', {timeOut: 5000})
        return false;
    }

    $.ajax ({
        url: 'index_handler.php',
        type: 'POST',
        cache: false,
        data: {'login':login,'password':password},
        dataType: 'html',
        success: function(data){
            var response = JSON.parse(data);
            if (response.message == 'Pair correct!'){
                document.cookie = "username =" + login + "; expires=Thu, 18 Dec 2019 12:00:00 UTC; path=/";
                window.open("prohlis_media_2/main_forms/main_menu/main_menu.php","_self");
            } else {
                toastr.error(response.message, 'Error!', {timeOut: 5000})
            }
        }
    })
});