-- MySQL dump 10.13  Distrib 5.7.20, for Linux (x86_64)
--
-- Host: localhost    Database: prohlis_media_db
-- ------------------------------------------------------
-- Server version	5.7.20-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `active_feedback_record`
--

DROP TABLE IF EXISTS `active_feedback_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `active_feedback_record` (
  `active_feedback_id` int(11) NOT NULL AUTO_INCREMENT,
  `angebot` varchar(45) NOT NULL,
  `datum` date NOT NULL,
  `mitarbeiter` varchar(45) NOT NULL,
  `an_wen` varchar(45) NOT NULL,
  `art_des_feedback` varchar(45) NOT NULL,
  `inhalt_des_feedback` varchar(45) NOT NULL,
  `weiterbearbeitung` varchar(45) NOT NULL,
  PRIMARY KEY (`active_feedback_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `aktion`
--

DROP TABLE IF EXISTS `aktion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aktion` (
  `aktion_id` int(11) NOT NULL AUTO_INCREMENT,
  `aktion_type` varchar(45) NOT NULL,
  PRIMARY KEY (`aktion_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `angebot`
--

DROP TABLE IF EXISTS `angebot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `angebot` (
  `angebot_id` int(11) NOT NULL AUTO_INCREMENT,
  `angebot_name` varchar(45) NOT NULL,
  PRIMARY KEY (`angebot_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `art_des_feedback`
--

DROP TABLE IF EXISTS `art_des_feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `art_des_feedback` (
  `art_des_feedback_id` int(11) NOT NULL AUTO_INCREMENT,
  `art_des_feedback_type` varchar(45) NOT NULL,
  PRIMARY KEY (`art_des_feedback_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ausfallgrund`
--

DROP TABLE IF EXISTS `ausfallgrund`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ausfallgrund` (
  `ausfallgrund_id` int(11) NOT NULL AUTO_INCREMENT,
  `ausfallgrund_type` varchar(45) NOT NULL,
  PRIMARY KEY (`ausfallgrund_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `einzelfallhilfe_record`
--

DROP TABLE IF EXISTS `einzelfallhilfe_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `einzelfallhilfe_record` (
  `einzelfallhilfe_record_id` int(11) NOT NULL AUTO_INCREMENT,
  `datum` date NOT NULL,
  `dauer_in_h` int(11) NOT NULL,
  `mitarbeiter01` varchar(45) NOT NULL,
  `mitarbeiter02` varchar(45) NOT NULL,
  `mitarbeiter03` varchar(45) NOT NULL,
  `n_u12_m` int(10) NOT NULL,
  `n_u12_w` int(10) NOT NULL,
  `n_u14_m` int(10) NOT NULL,
  `n_u14_w` int(10) NOT NULL,
  `n_u14_17_m` int(10) NOT NULL,
  `n_u14_17_w` int(10) NOT NULL,
  `n_u18_20_m` int(10) NOT NULL,
  `n_u18_20_w` int(10) NOT NULL,
  `n_u21_24_m` int(10) NOT NULL,
  `n_u21_24_w` int(10) NOT NULL,
  `n_u25_26_m` int(10) NOT NULL,
  `n_u25_26_w` int(10) NOT NULL,
  `n_o27_m` int(10) NOT NULL,
  `n_o27_w` int(10) NOT NULL,
  `th_sucht` tinyint(1) NOT NULL,
  `th_gewalt` tinyint(1) NOT NULL,
  `th_kriminalitat` tinyint(1) NOT NULL,
  `th_gesundheit` tinyint(1) NOT NULL,
  `th_freizeit` tinyint(1) NOT NULL,
  `th_identitat` tinyint(1) NOT NULL,
  `th_wohnraum` tinyint(1) NOT NULL,
  `th_familie` tinyint(1) NOT NULL,
  `th_behorden` tinyint(1) NOT NULL,
  `th_finanzielle` tinyint(1) NOT NULL,
  `th_arbeit` tinyint(1) NOT NULL,
  `th_beziehung` tinyint(1) NOT NULL,
  `th_politische_themen` tinyint(1) NOT NULL,
  `th_digitales` tinyint(1) NOT NULL,
  `th_vorstellung` tinyint(1) NOT NULL,
  `th_sonstiges` varchar(45) NOT NULL,
  `anmerkungen` varchar(250) NOT NULL,
  `project_name` varchar(45) NOT NULL,
  PRIMARY KEY (`einzelfallhilfe_record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gemeinwesenarbeit_record`
--

DROP TABLE IF EXISTS `gemeinwesenarbeit_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gemeinwesenarbeit_record` (
  `gemeinwesenarbeit_record_id` int(11) NOT NULL AUTO_INCREMENT,
  `datum` date NOT NULL,
  `dauer_in_h` int(10) NOT NULL,
  `mitarbeiter01` varchar(45) NOT NULL,
  `mitarbeiter02` varchar(45) NOT NULL,
  `mitarbeiter03` varchar(45) NOT NULL,
  `n_bekannt` int(10) NOT NULL,
  `n_unbekannt` int(10) NOT NULL,
  `n_u12_m` int(10) NOT NULL,
  `n_u12_w` int(10) NOT NULL,
  `n_u14_m` int(10) NOT NULL,
  `n_u14_w` int(10) NOT NULL,
  `n_u14_17_m` int(10) NOT NULL,
  `n_u14_17_w` int(10) NOT NULL,
  `n_u18_20_m` int(10) NOT NULL,
  `n_u18_20_w` int(10) NOT NULL,
  `n_u21_24_m` int(10) NOT NULL,
  `n_u21_24_w` int(10) NOT NULL,
  `n_u25_26_m` int(10) NOT NULL,
  `n_u25_26_w` int(10) NOT NULL,
  `n_o27_m` int(10) NOT NULL,
  `n_o27_w` int(10) NOT NULL,
  `th_sucht` tinyint(1) NOT NULL,
  `th_gewalt` tinyint(1) NOT NULL,
  `th_kriminalitat` tinyint(1) NOT NULL,
  `th_gesundheit` tinyint(1) NOT NULL,
  `th_freizeit` tinyint(1) NOT NULL,
  `th_identitat` tinyint(1) NOT NULL,
  `th_wohnraum` tinyint(1) NOT NULL,
  `th_familie` tinyint(1) NOT NULL,
  `th_behorden` tinyint(1) NOT NULL,
  `th_finanzielle` tinyint(1) NOT NULL,
  `th_arbeit` tinyint(1) NOT NULL,
  `th_beziehung` tinyint(1) NOT NULL,
  `th_politische_themen` tinyint(1) NOT NULL,
  `th_digitales` tinyint(1) NOT NULL,
  `th_vorstellung` tinyint(1) NOT NULL,
  `platz` varchar(45) NOT NULL,
  `wetter` varchar(45) NOT NULL,
  `mobilitat` varchar(45) NOT NULL,
  `platzwahrnehmung` varchar(45) NOT NULL,
  `th_sonstiges` varchar(45) NOT NULL,
  `anmerkungen` varchar(250) NOT NULL,
  `project_name` varchar(45) NOT NULL,
  PRIMARY KEY (`gemeinwesenarbeit_record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gruppenarbeit_record`
--

DROP TABLE IF EXISTS `gruppenarbeit_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gruppenarbeit_record` (
  `gruppenarbeit_record_id` int(11) NOT NULL AUTO_INCREMENT,
  `datum` date NOT NULL,
  `dauer_in_h` varchar(45) NOT NULL,
  `mitarbeiter01` varchar(45) NOT NULL,
  `mitarbeiter02` varchar(45) NOT NULL,
  `mitarbeiter03` varchar(45) NOT NULL,
  `n_bekannt` int(11) NOT NULL,
  `n_unbekannt` int(11) NOT NULL,
  `n_u12_m` int(11) NOT NULL,
  `n_u12_w` int(11) NOT NULL,
  `n_u14_m` int(11) NOT NULL,
  `n_u14_w` int(11) NOT NULL,
  `n_u14_17_m` int(11) NOT NULL,
  `n_u14_17_w` int(11) NOT NULL,
  `n_u18_20_m` int(11) NOT NULL,
  `n_u18_20_w` int(11) NOT NULL,
  `n_u21_24_m` int(11) NOT NULL,
  `n_u21_24_w` int(11) NOT NULL,
  `n_u25_26_m` int(11) NOT NULL,
  `n_u25_26_w` int(11) NOT NULL,
  `n_o27_m` int(11) NOT NULL,
  `n_o27_w` int(11) NOT NULL,
  `th_sucht` tinyint(1) NOT NULL,
  `th_gewalt` tinyint(1) NOT NULL,
  `th_kriminalitat` tinyint(1) NOT NULL,
  `th_gesundheit` tinyint(1) NOT NULL,
  `th_freizeit` tinyint(1) NOT NULL,
  `th_identitat` tinyint(1) NOT NULL,
  `th_wohnraum` tinyint(1) NOT NULL,
  `th_familie` tinyint(1) NOT NULL,
  `th_behorden` tinyint(1) NOT NULL,
  `th_finanzielle` tinyint(1) NOT NULL,
  `th_arbeit` tinyint(1) NOT NULL,
  `th_beziehung` tinyint(1) NOT NULL,
  `th_politische_themen` tinyint(1) NOT NULL,
  `th_digitales` tinyint(1) NOT NULL,
  `th_vorstellung` tinyint(1) NOT NULL,
  `th_sonstiges` varchar(45) NOT NULL,
  `anmerkungen` varchar(100) NOT NULL,
  `project_name` varchar(45) NOT NULL,
  PRIMARY KEY (`gruppenarbeit_record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mobilitat`
--

DROP TABLE IF EXISTS `mobilitat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobilitat` (
  `mobilitat_id` int(11) NOT NULL AUTO_INCREMENT,
  `mobilitat_type` varchar(45) NOT NULL,
  PRIMARY KEY (`mobilitat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `passive_feedback_record`
--

DROP TABLE IF EXISTS `passive_feedback_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `passive_feedback_record` (
  `passive_feedback_id` int(11) NOT NULL AUTO_INCREMENT,
  `angebot` varchar(45) NOT NULL,
  `aktion` varchar(45) NOT NULL,
  `datum` varchar(45) NOT NULL,
  `mitarbeiter` varchar(45) NOT NULL,
  `positiv_resp` varchar(45) NOT NULL,
  `negativ_resp` varchar(45) NOT NULL,
  `schlussfolgerungen` varchar(45) NOT NULL,
  `rahmenbedingungen` varchar(45) NOT NULL,
  `teilnehmer_innen` varchar(45) NOT NULL,
  `fb_anzahl` varchar(45) NOT NULL,
  `fb_geber_innen` varchar(45) NOT NULL,
  `allgemeine_einschatzung` varchar(45) NOT NULL,
  PRIMARY KEY (`passive_feedback_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `platz`
--

DROP TABLE IF EXISTS `platz`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `platz` (
  `platz_id` int(11) NOT NULL AUTO_INCREMENT,
  `platz_name` varchar(45) NOT NULL,
  `stadtteil_name` varchar(45) NOT NULL,
  PRIMARY KEY (`platz_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project` (
  `project_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_name` varchar(45) NOT NULL,
  `type_of_work` varchar(45) NOT NULL,
  PRIMARY KEY (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stadtteil`
--

DROP TABLE IF EXISTS `stadtteil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stadtteil` (
  `stadtteil_id` int(11) NOT NULL AUTO_INCREMENT,
  `stadtteil_name` varchar(45) NOT NULL,
  PRIMARY KEY (`stadtteil_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `streetwork_record`
--

DROP TABLE IF EXISTS `streetwork_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `streetwork_record` (
  `streetwork_record_id` int(11) NOT NULL AUTO_INCREMENT,
  `datum` date NOT NULL,
  `uhrzeit` time(4) NOT NULL,
  `mitarbeiter01` varchar(40) NOT NULL,
  `mitarbeiter02` varchar(40) NOT NULL,
  `mitarbeiter03` varchar(40) NOT NULL,
  `n_bekannt` int(11) NOT NULL,
  `n_unbekannt` int(11) NOT NULL,
  `n_u12_m` int(11) NOT NULL,
  `n_u12_w` int(11) NOT NULL,
  `n_u14_m` int(11) NOT NULL,
  `n_u14_w` int(11) NOT NULL,
  `n_u14_17_m` int(11) NOT NULL,
  `n_u14_17_w` int(11) NOT NULL,
  `n_u18_20_m` int(11) NOT NULL,
  `n_u18_20_w` int(11) NOT NULL,
  `n_u21_24_m` int(11) NOT NULL,
  `n_u21_24_w` int(11) NOT NULL,
  `n_u25_26_m` int(11) NOT NULL,
  `n_u25_26_w` int(11) NOT NULL,
  `n_o27_m` int(11) NOT NULL,
  `n_o27_w` int(11) NOT NULL,
  `th_sucht` tinyint(1) NOT NULL,
  `th_gewalt` tinyint(1) NOT NULL,
  `th_kriminalitat` tinyint(1) NOT NULL,
  `th_gesundheit` tinyint(1) NOT NULL,
  `th_freizeit` tinyint(1) NOT NULL,
  `th_identitat` tinyint(1) NOT NULL,
  `th_wohnraum` tinyint(1) NOT NULL,
  `th_familie` tinyint(1) NOT NULL,
  `th_behorden` tinyint(1) NOT NULL,
  `th_finanzielle` tinyint(1) NOT NULL,
  `th_arbeit` tinyint(1) NOT NULL,
  `th_beziehung` tinyint(1) NOT NULL,
  `th_politische_themen` tinyint(1) NOT NULL,
  `th_digitales` tinyint(1) NOT NULL,
  `th_vorstellung` tinyint(1) NOT NULL,
  `platz` varchar(40) NOT NULL,
  `wetter` varchar(40) NOT NULL,
  `ausfallgrund` varchar(40) NOT NULL,
  `mobilitat` varchar(40) NOT NULL,
  `platzwahrnehmung` varchar(40) NOT NULL,
  `th_sonstiges` varchar(40) NOT NULL,
  `anmerkungen` varchar(250) NOT NULL,
  `project_name` varchar(45) NOT NULL,
  PRIMARY KEY (`streetwork_record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(225) NOT NULL,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `short_name` varchar(45) NOT NULL,
  `work_status` varchar(45) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wetter`
--

DROP TABLE IF EXISTS `wetter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wetter` (
  `wetter_id` int(11) NOT NULL AUTO_INCREMENT,
  `wetter_type` varchar(45) NOT NULL,
  PRIMARY KEY (`wetter_id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `worker`
--

DROP TABLE IF EXISTS `worker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `worker` (
  `worker_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `short_name` varchar(100) NOT NULL,
  PRIMARY KEY (`worker_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-09  0:16:27
