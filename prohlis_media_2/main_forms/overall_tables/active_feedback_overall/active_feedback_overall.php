<?php
require('../../../resources/dbconnection.php');

$response = @mysqli_query($dbc, "SELECT * FROM active_feedback_record");
$short_name_query = mysqli_query($dbc, 'SELECT * FROM user');

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Active Feedback Overall</title>
        <!-- Bootstrap core CSS -->
        <link href="../../../resources/frameworks/bootstrap4/css/bootstrap.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="active-feedback-overall-stylesheet.css" rel="stylesheet">
        <link href="../common_files/loader-stylesheet.css" rel="stylesheet">
        <link href="../../../resources/frameworks/toastr-master/build/toastr.min.css" rel="stylesheet">
        <link href="../../../resources/frameworks/fixed-header-table/css/defaultTheme.css" rel="stylesheet">
        <!-- <link href="../common_files/myTheme.css" rel="stylesheet"> -->
        <link href="../common_files/custom-table-style.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
        <!-- <script type="text/javascript" src="../resources/frameworks/toastr-master/build/toastr.min.js"></script> -->
    </head>
    <body>
        <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
            <a class="navbar-brand" href="../main_menu/main_menu.php">PROHLIS MEDIA</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="../../main_menu/main_menu.php">Hauptmenü</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../../input_forms/active_feedback/active_feedback_form.php">Return to insertion form</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" id="current_user_username" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <?php
                            $username = $_COOKIE['username'];
                            while ($current_user = mysqli_fetch_array($short_name_query)) {
                                if ($current_user['username'] == $username){
                                    echo $current_user['first_name']." ".$current_user['last_name'];
                                }
                            }
                            ?>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="dropdown01">
                            <a class="dropdown-item" href="#">Edit settings</a>
                            <a class="dropdown-item" href="../../../../../logout.php">Log out</a>
                        </div>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                </form>
            </div>
        </nav>

        <div class="container container-table">
            <div class="row">
                <div class="col">
                    <div class="w-100 text-center">
                        <h2><span class="text-uppercase">ACTIVE FEEDBACK OVERALL</span></h2>
                    </div>
                </div>
            </div>
            <div class="row" id="table-row-custom">
                <div class="col">
                    <div class="table-container w-100">
                        <table class="fancyTable table" id="myTable01">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Angebot</th>
                                    <th>Datum</th>
                                    <th>Mitarbeiter</th>
                                    <th>An wen?</th>
                                    <th>Art des feedback</th>
                                    <th>Inhalt des feedback</th>
                                    <th>Weiterbearbeitung</th>
                                </tr>
                            </thead>
                                <tbody>
                                    <?php while($row = mysqli_fetch_array($response)){
                                        echo '
                                            <tr>
                                                <th>'. $row['active_feedback_id']. '</th>
                                                <td>'. $row['angebot']. '</td>
                                                <td>'. $row['datum']. '</td>
                                                <td>'. $row['mitarbeiter']. '</td>
                                                <td>'. $row['an_wen']. '</td>
                                                <td>'. $row['art_des_feedback']. '</td>
                                                <td>'. $row['inhalt_des_feedback']. '</td>
                                                <td>'. $row['weiterbearbeitung']. '</td>
                                            </tr>';
                                    } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group w-50">
                        <label for="mitarbeiter_username">Remove record:</label>
                        <div class="input-group input-group-md">
                            <input type="text" class="form-control" id="deletion_id_input_field" name="deletion_id_input_field" placeholder="ID of record">
                            <div class="input-group-btn">
                                <div class="input-group-btn">
                                    <button class="btn btn-primary bg-grey js_record_row_delete_button" type="button" data-toggle="modal" data-target="#addPlatz">
                                        <span class="trans-up">delete</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group w-50">
                        <label for="printing_id_input_field">Download selected feedback:</label>
                        <div class="input-group input-group-md">
                            <input type="text" class="form-control" id="printing_id_input_field" name="printing_id_input_field" placeholder="ID of record">
                            <div class="input-group-btn">
                                <div class="input-group-btn">
                                    <button class="btn btn-primary bg-grey js_record_row_print_button" type="button">
                                        <span class="trans-up">download</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <!-- <div role="group" aria-label="Basic example" class="btn-group btn-group-md"> -->
                            <!-- <button type="button" class="btn btn-secondary trans-up js_main_form_export_button_2017">export 2017</button>
                            <button type="button" class="btn btn-secondary trans-up js_main_form_export_button_2018">export 2018</button>
                            <button type="button" class="btn btn-secondary trans-up js_statistics_export_button">statistics</button> -->
                        <!-- </div> -->
                    </div>
                </div>
            </div>
        </div>
        <div class="container container-loader" id="loader-popup">
            <div class="row">
                <div class="col-md-12">
                    <h5 class="text-center mt-5 mb-5">Please do not close this window <br> until the file will be loaded..</h5>
                    <div class="middle">
                        <div class="sk-folding-cube">
                            <div class="sk-cube1 sk-cube"></div>
                            <div class="sk-cube2 sk-cube"></div>
                            <div class="sk-cube4 sk-cube"></div>
                            <div class="sk-cube3 sk-cube"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="../../../resources/frameworks/assets4/js/jquery.min.js"></script>
        <script src="../../../resources/frameworks/assets4/js/popper.js"></script>
        <script src="../../../resources/frameworks/bootstrap4/js/bootstrap.min.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="../../../resources/frameworks/assets4/js/ie10-viewport-bug-workaround.js"></script>
        <!-- Notifications framework "Toastr" -->
        <script type="text/javascript" src="../../../resources/frameworks/toastr-master/build/toastr.min.js"></script>
        <!-- Fixed-header-table framework -->
        <script type="text/javascript" src="../../../resources/frameworks/fixed-header-table/jquery.fixedheadertable.js"></script>
        <!-- Custom scripts -->
        <script src="../../../helpersJS/overall_tables_export_js/overall_tables_common_js.js"></script>
        <script src="active-feedback-overall-js.js"></script>
    </body>
</html>
