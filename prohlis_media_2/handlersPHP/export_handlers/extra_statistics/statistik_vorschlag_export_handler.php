<?php

require '../../../resources/frameworks/phpspreadsheet/vendor/autoload.php';
require '../../../resources/dbconnection.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Cell\Cell;

$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
$objPHPExcel = $reader->load("../../../resources/excel-templates/Statistik_vorschlag.xlsx");

for ($workpartnum = 0; $workpartnum <= 4; $workpartnum++){
    
    if($workpartnum == 0){
        $sheetIndex = 1;
        $all_works_record_request = "SELECT datum, n_u12_m, n_u12_w, n_u14_m, n_u14_w, n_u14_17_m, n_u14_17_w, n_u18_20_m, n_u18_20_w, n_u21_24_m, n_u21_24_w, n_u25_26_m, n_u25_26_w, n_o27_m, n_o27_w FROM streetwork_record WHERE year(datum)='2017' UNION ALL SELECT datum, n_u12_m, n_u12_w, n_u14_m, n_u14_w, n_u14_17_m, n_u14_17_w, n_u18_20_m, n_u18_20_w, n_u21_24_m, n_u21_24_w, n_u25_26_m, n_u25_26_w, n_o27_m, n_o27_w FROM gruppenarbeit_record WHERE year(datum)='2017' UNION ALL SELECT datum, n_u12_m, n_u12_w, n_u14_m, n_u14_w, n_u14_17_m, n_u14_17_w, n_u18_20_m, n_u18_20_w, n_u21_24_m, n_u21_24_w, n_u25_26_m, n_u25_26_w, n_o27_m, n_o27_w FROM einzelfallhilfe_record WHERE year(datum)='2017' UNION ALL SELECT datum, n_u12_m, n_u12_w, n_u14_m, n_u14_w, n_u14_17_m, n_u14_17_w, n_u18_20_m, n_u18_20_w, n_u21_24_m, n_u21_24_w, n_u25_26_m, n_u25_26_w, n_o27_m, n_o27_w FROM gemeinwesenarbeit_record WHERE year(datum)='2017'";

        $all_bekannt_unbekannt_record_request = "SELECT datum, n_bekannt, n_unbekannt FROM streetwork_record WHERE year(datum)='2017' UNION ALL SELECT datum, n_bekannt, n_unbekannt FROM gruppenarbeit_record WHERE year(datum)='2017' UNION ALL SELECT datum, n_bekannt, n_unbekannt FROM gemeinwesenarbeit_record WHERE year(datum)='2017'";

        $all_themes_record_request = "SELECT datum, th_sucht, th_gewalt, th_kriminalitat, th_gesundheit, th_freizeit, th_identitat, th_wohnraum, th_familie, th_behorden, th_finanzielle, th_arbeit, th_beziehung, th_politische_themen, th_digitales, th_vorstellung FROM streetwork_record WHERE year(datum)='2017' UNION ALL SELECT datum, th_sucht, th_gewalt, th_kriminalitat, th_gesundheit, th_freizeit, th_identitat, th_wohnraum, th_familie, th_behorden, th_finanzielle, th_arbeit, th_beziehung, th_politische_themen, th_digitales, th_vorstellung FROM gruppenarbeit_record WHERE year(datum)='2017' UNION ALL SELECT datum, th_sucht, th_gewalt, th_kriminalitat, th_gesundheit, th_freizeit, th_identitat, th_wohnraum, th_familie, th_behorden, th_finanzielle, th_arbeit, th_beziehung, th_politische_themen, th_digitales, th_vorstellung FROM einzelfallhilfe_record WHERE year(datum)='2017' UNION ALL SELECT datum, th_sucht, th_gewalt, th_kriminalitat, th_gesundheit, th_freizeit, th_identitat, th_wohnraum, th_familie, th_behorden, th_finanzielle, th_arbeit, th_beziehung, th_politische_themen, th_digitales, th_vorstellung FROM gemeinwesenarbeit_record WHERE year(datum)='2017'";
    }else if ($workpartnum == 1){
        $sheetIndex = 2;
        $all_works_record_request = "SELECT datum, n_u12_m, n_u12_w, n_u14_m, n_u14_w, n_u14_17_m, n_u14_17_w, n_u18_20_m, n_u18_20_w, n_u21_24_m, n_u21_24_w, n_u25_26_m, n_u25_26_w, n_o27_m, n_o27_w FROM streetwork_record WHERE year(datum)='2017'";

        $all_bekannt_unbekannt_record_request = "SELECT datum, n_bekannt, n_unbekannt FROM streetwork_record WHERE year(datum)='2017'";

        $all_themes_record_request = "SELECT datum, th_sucht, th_gewalt, th_kriminalitat, th_gesundheit, th_freizeit, th_identitat, th_wohnraum, th_familie, th_behorden, th_finanzielle, th_arbeit, th_beziehung, th_politische_themen, th_digitales, th_vorstellung FROM streetwork_record WHERE year(datum)='2017'";

        $wetterRequest = "SELECT month(datum) AS month, n_u12_m, n_u12_w, n_u14_m, n_u14_w, n_u14_17_m, n_u14_17_w, n_u18_20_m, n_u18_20_w, n_u21_24_m, n_u21_24_w,  n_u25_26_m, n_u25_26_w, n_o27_m, n_o27_w, wetter FROM streetwork_record WHERE year(datum)='2017' ORDER BY wetter";
    }else if ($workpartnum == 2){
        $sheetIndex = 3;
        $all_works_record_request = "SELECT datum, n_u12_m, n_u12_w, n_u14_m, n_u14_w, n_u14_17_m, n_u14_17_w, n_u18_20_m, n_u18_20_w, n_u21_24_m, n_u21_24_w, n_u25_26_m, n_u25_26_w, n_o27_m, n_o27_w FROM gruppenarbeit_record WHERE year(datum)='2017'";

        $all_bekannt_unbekannt_record_request = "SELECT datum, n_bekannt, n_unbekannt FROM gruppenarbeit_record WHERE year(datum)='2017'";

        $all_themes_record_request = "SELECT datum, th_sucht, th_gewalt, th_kriminalitat, th_gesundheit, th_freizeit, th_identitat, th_wohnraum, th_familie, th_behorden, th_finanzielle, th_arbeit, th_beziehung, th_politische_themen, th_digitales, th_vorstellung FROM gruppenarbeit_record WHERE year(datum)='2017'";

    }else if ($workpartnum == 3){
        $sheetIndex = 4;
        $all_works_record_request = "SELECT datum, n_u12_m, n_u12_w, n_u14_m, n_u14_w, n_u14_17_m, n_u14_17_w, n_u18_20_m, n_u18_20_w, n_u21_24_m, n_u21_24_w, n_u25_26_m, n_u25_26_w, n_o27_m, n_o27_w FROM gemeinwesenarbeit_record WHERE year(datum)='2017'";

        $all_bekannt_unbekannt_record_request = "SELECT datum, n_bekannt, n_unbekannt FROM gemeinwesenarbeit_record WHERE year(datum)='2017'";

        $all_themes_record_request = "SELECT datum, th_sucht, th_gewalt, th_kriminalitat, th_gesundheit, th_freizeit, th_identitat, th_wohnraum, th_familie, th_behorden, th_finanzielle, th_arbeit, th_beziehung, th_politische_themen, th_digitales, th_vorstellung FROM gemeinwesenarbeit_record WHERE year(datum)='2017'";

        $wetterRequest = "SELECT month(datum) AS month, n_u12_m, n_u12_w, n_u14_m, n_u14_w, n_u14_17_m, n_u14_17_w, n_u18_20_m, n_u18_20_w, n_u21_24_m, n_u21_24_w,  n_u25_26_m, n_u25_26_w, n_o27_m, n_o27_w, wetter FROM gemeinwesenarbeit_record WHERE year(datum)='2017' ORDER BY wetter";
    }else if ($workpartnum == 4){
        $sheetIndex = 5;
        $all_works_record_request = "SELECT datum, n_u12_m, n_u12_w, n_u14_m, n_u14_w, n_u14_17_m, n_u14_17_w, n_u18_20_m, n_u18_20_w, n_u21_24_m, n_u21_24_w, n_u25_26_m, n_u25_26_w, n_o27_m, n_o27_w FROM einzelfallhilfe_record WHERE year(datum)='2017'";

        $all_themes_record_request = "SELECT datum, th_sucht, th_gewalt, th_kriminalitat, th_gesundheit, th_freizeit, th_identitat, th_wohnraum, th_familie, th_behorden, th_finanzielle, th_arbeit, th_beziehung, th_politische_themen, th_digitales, th_vorstellung FROM einzelfallhilfe_record WHERE year(datum)='2017'";

    }

    $row12_m = 5;
    $row12_w = 6;
    $row14_m = 7;
    $row14_w = 8;
    $row14_17_m = 9;
    $row14_17_w = 10;
    $row18_20_m = 11;
    $row18_20_w = 12;
    $row21_24_m = 13;
    $row21_24_w = 14;
    $row25_26_m = 15;
    $row25_26_w = 16;
    $row27_m = 17;
    $row27_w = 18;
    $rowmale = 24;
    $rowfemale = 25;
    $rowbekannt = 30;
    $rowunbekannt = 31;

    $rowsucht = 36;
    $rowgewalt = 37;
    $rowkriminalitat = 38;
    $rowgesundheit = 39;
    $rowfreizeit = 40;
    $rowidentitat = 41;
    $rowwohnraum = 42;
    $rowfamilie = 43;
    $rowbehorden = 44;
    $rowfinanzielle = 45;
    $rowarbeit = 46;
    $rowbeziehung = 47;
    $rowpolitische_themen = 48;
    $rowdigitales = 49;
    $rowvorstellung = 50;

    // // --------------------------------------------------------------
    // $objPHPExcel->setActiveSheetIndex($sheetIndex);
    // $objPHPExcel->getActiveSheet()->SetCellValue('C4', 'hello');

    // $сolumnIndex = PHPExcel_Cell::columnIndexFromString(3);
    // $objPHPExcel->getActiveSheet()->SetCellValue($column.'4', 'hello');
    // // --------------------------------------------------------------

    // $lastColumn = $worksheet->getHighestColumn();
    // $lastColumn++;
    // for ($column = 'A'; $column != $lastColumn; $column++) {
    //     $cell = $worksheet->getCell($column.$row);
    //     //  Do what you want with the cell
    // }
    // --------------------------------------------------------------


    date_default_timezone_set('UTC');

    $year_part = '2017-0';
    $begining_day = '-01';
    $end_day = '-31';

    $objPHPExcel->setActiveSheetIndex($sheetIndex);

    // Looping through the months of the year
    for ($j=1; $j <= 12; $j++) {

        $n_u12_m_total_month = 0;
        $n_u12_w_total_month = 0;
        $n_u14_m_total_month = 0;
        $n_u14_w_total_month = 0;
        $n_u14_17_m_total_month = 0;
        $n_u14_17_w_total_month = 0;
        $n_u18_20_m_total_month = 0;
        $n_u18_20_w_total_month = 0;
        $n_u21_24_m_total_month = 0;
        $n_u21_24_w_total_month = 0;
        $n_u25_26_m_total_month = 0;
        $n_u25_26_w_total_month = 0;
        $n_o27_m_total_month = 0;
        $n_o27_w_total_month = 0;
        $male_total_month = 0;
        $female_total_month = 0;
        $n_bekannt_total_month = 0;
        $n_unbekannt_total_month = 0;
        $th_sucht_total_month = 0;
        $th_gewalt_total_month = 0;
        $th_kriminalitat_total_month = 0;
        $th_gesundheit_total_month = 0;
        $th_freizeit_total_month = 0;
        $th_identitat_total_month = 0;
        $th_wohnraum_total_month = 0;
        $th_familie_total_month = 0;
        $th_behorden_total_month = 0;
        $th_finanzielle_total_month = 0;
        $th_arbeit_total_month = 0;
        $th_beziehung_total_month = 0;
        $th_politische_themen_total_month = 0;
        $th_digitales_total_month = 0;
        $th_vorstellung_total_month = 0;
        
        //Loop declares the days quantity of the month
        if ($j==1 || $j==3 || $j==5 || $j==7 || $j==8 || $j==10 || $j==12) {
            $end_day = '-31';
            if ($j >= 9) {
                $year_part = '2017-';
            }
        }
        if ($j >= 10) {
            $year_part = '2017-';
        }
        if ($j==4 || $j==6 || $j==9 || $j==11) {
            $end_day = '-30';
            if ($j >= 10) {
                $year_part = '2017-';
            }
        }
        if ($j==2){
            $end_day = '-28';
        }
        
        $date = $year_part.$j.$begining_day;
        $end_date = $year_part.$j.$end_day;

        /**
         * Assigning the months to the columns
         */
        if($j==1){$column='D';}else if($j==2){$column='E';}else if($j==3){$column='F';}else if($j==4){$column='G';}else if($j==5){$column='H';}else if($j==6){$column='I';}else if($j==7){$column='J';}else if($j==8){$column='K';}else if($j==9){$column='L';}else if($j==10){$column='M';}else if($j==11){$column='N';}else if($j==12){$column='O';}
        
        while (strtotime($date) <= strtotime($end_date)) {

            /**
             * Loop over number of people met
             */
            $n_u12_m_all = 0;
            $n_u12_w_all = 0;
            $n_u14_m_all = 0;
            $n_u14_w_all = 0;
            $n_u14_17_m_all = 0;
            $n_u14_17_w_all = 0;
            $n_u18_20_m_all = 0;
            $n_u18_20_w_all = 0;
            $n_u21_24_m_all = 0;
            $n_u21_24_w_all = 0;
            $n_u25_26_m_all = 0;
            $n_u25_26_w_all = 0;
            $n_o27_m_all = 0;
            $n_o27_w_all = 0;
            $male_all = 0;
            $female_all = 0;

            $all_works_record_query = mysqli_query($dbc, $all_works_record_request);

            while ($all_works_record_array = mysqli_fetch_array($all_works_record_query)) {

                if ($all_works_record_array['datum']==$date) {

                    $n_u12_m = $all_works_record_array['n_u12_m'];
                    $n_u12_w = $all_works_record_array['n_u12_w'];
                    $n_u14_m = $all_works_record_array['n_u14_m'];
                    $n_u14_w = $all_works_record_array['n_u14_w'];
                    $n_u14_17_m = $all_works_record_array['n_u14_17_m'];
                    $n_u14_17_w = $all_works_record_array['n_u14_17_w'];
                    $n_u18_20_m = $all_works_record_array['n_u18_20_m'];
                    $n_u18_20_w = $all_works_record_array['n_u18_20_w'];
                    $n_u21_24_m = $all_works_record_array['n_u21_24_m'];
                    $n_u21_24_w = $all_works_record_array['n_u21_24_w'];
                    $n_u25_26_m = $all_works_record_array['n_u25_26_m'];
                    $n_u25_26_w = $all_works_record_array['n_u25_26_w'];
                    $n_o27_m = $all_works_record_array['n_o27_m'];
                    $n_o27_w = $all_works_record_array['n_o27_w'];

                    $male = $n_u12_m + $n_u14_m + $n_u14_17_m + $n_u18_20_m + $n_u21_24_m + $n_u25_26_m + $n_o27_m;
                    $female = $n_u12_w + $n_u14_w + $n_u14_17_w + $n_u18_20_w + $n_u21_24_w + $n_u25_26_w + $n_o27_w;

                    $n_u12_m_all = $n_u12_m_all + $n_u12_m;
                    $n_u12_w_all = $n_u12_w_all + $n_u12_w;
                    $n_u14_m_all = $n_u14_m_all + $n_u14_m;
                    $n_u14_w_all = $n_u14_w_all + $n_u14_w;
                    $n_u14_17_m_all = $n_u14_17_m_all + $n_u14_17_m;
                    $n_u14_17_w_all = $n_u14_17_w_all + $n_u14_17_w;
                    $n_u18_20_m_all = $n_u18_20_m_all + $n_u18_20_m;
                    $n_u18_20_w_all = $n_u18_20_w_all + $n_u18_20_w;
                    $n_u21_24_m_all = $n_u21_24_m_all + $n_u21_24_m;
                    $n_u21_24_w_all = $n_u21_24_w_all + $n_u21_24_w;
                    $n_u25_26_m_all = $n_u25_26_m_all + $n_u25_26_m;
                    $n_u25_26_w_all = $n_u25_26_w_all + $n_u25_26_w;
                    $n_o27_m_all = $n_o27_m_all + $n_o27_m;
                    $n_o27_w_all = $n_o27_w_all + $n_o27_w;
                    $male_all = $male_all + $male;
                    $female_all = $female_all + $female;
                }
            }

            $n_u12_m_total_month = $n_u12_m_total_month + $n_u12_m_all;
            $n_u12_w_total_month = $n_u12_w_total_month + $n_u12_w_all;
            $n_u14_m_total_month = $n_u14_m_total_month + $n_u14_m_all;
            $n_u14_w_total_month = $n_u14_w_total_month + $n_u14_w_all;
            $n_u14_17_m_total_month = $n_u14_17_m_total_month + $n_u14_17_m_all;
            $n_u14_17_w_total_month = $n_u14_17_w_total_month + $n_u14_17_w_all;
            $n_u18_20_m_total_month = $n_u18_20_m_total_month + $n_u18_20_m_all;
            $n_u18_20_w_total_month = $n_u18_20_w_total_month + $n_u18_20_w_all;
            $n_u21_24_m_total_month = $n_u21_24_m_total_month + $n_u21_24_m_all;
            $n_u21_24_w_total_month = $n_u21_24_w_total_month + $n_u21_24_w_all;
            $n_u25_26_m_total_month = $n_u25_26_m_total_month + $n_u25_26_m_all;
            $n_u25_26_w_total_month = $n_u25_26_w_total_month + $n_u25_26_w_all;
            $n_o27_m_total_month = $n_o27_m_total_month + $n_o27_m_all;
            $n_o27_w_total_month = $n_o27_w_total_month + $n_o27_w_all;
            $male_total_month = $male_total_month + $male_all;
            $female_total_month = $female_total_month + $female_all;      

            if ($workpartnum != 4){
                /**
                 * Loop for bekannt & unbekannt table
                 */
                $n_bekannt_all = 0;
                $n_unbekannt_all = 0;

                $all_bekannt_unbekannt_record_query = mysqli_query($dbc, $all_bekannt_unbekannt_record_request)or die("MySQL error: " . mysqli_error($dbc) . "<hr>\nQuery: $all_bekannt_unbekannt_record_request");;

                while ($all_bekannt_unbekannt_record_array = mysqli_fetch_array($all_bekannt_unbekannt_record_query)) {
                    if ($all_bekannt_unbekannt_record_array['datum']==$date) {
                        $n_bekannt = $all_bekannt_unbekannt_record_array['n_bekannt'];
                        $n_unbekannt = $all_bekannt_unbekannt_record_array['n_unbekannt'];

                        $n_bekannt_all = $n_bekannt_all + $n_bekannt;
                        $n_unbekannt_all = $n_unbekannt_all + $n_unbekannt;
                    }
                }

                $n_bekannt_total_month = $n_bekannt_total_month + $n_bekannt_all;
                $n_unbekannt_total_month = $n_unbekannt_total_month + $n_unbekannt_all;
            }

            /**
             * Loop over themes (sucht; gewalt, kriminalitat ...)
             */
            $th_sucht_all = 0;
            $th_gewalt_all = 0;
            $th_kriminalitat_all = 0;
            $th_gesundheit_all = 0;
            $th_freizeit_all = 0;
            $th_identitat_all = 0;
            $th_wohnraum_all = 0;
            $th_familie_all = 0;
            $th_behorden_all = 0;
            $th_finanzielle_all = 0;
            $th_arbeit_all = 0;
            $th_beziehung_all = 0;
            $th_politische_themen_all = 0;
            $th_digitales_all = 0;
            $th_vorstellung_all = 0;

            $all_themes_record_query = mysqli_query($dbc, $all_themes_record_request);

            while ($all_themes_record_array = mysqli_fetch_array($all_themes_record_query)) {
                if ($all_themes_record_array['datum']==$date) {

                    $th_sucht_all = $th_sucht_all + $all_themes_record_array['th_sucht'];
                    $th_gewalt_all = $th_gewalt_all + $all_themes_record_array['th_gewalt'];
                    $th_kriminalitat_all = $th_kriminalitat_all + $all_themes_record_array['th_kriminalitat'];
                    $th_gesundheit_all = $th_gesundheit_all + $all_themes_record_array['th_gesundheit'];
                    $th_freizeit_all = $th_freizeit_all + $all_themes_record_array['th_freizeit'];
                    $th_identitat_all = $th_identitat_all + $all_themes_record_array['th_identitat'];
                    $th_wohnraum_all = $th_wohnraum_all + $all_themes_record_array['th_wohnraum'];
                    $th_familie_all = $th_familie_all + $all_themes_record_array['th_familie'];
                    $th_behorden_all = $th_behorden_all + $all_themes_record_array['th_behorden'];
                    $th_finanzielle_all = $th_finanzielle_all + $all_themes_record_array['th_finanzielle'];
                    $th_arbeit_all = $th_arbeit_all + $all_themes_record_array['th_arbeit'];
                    $th_beziehung_all = $th_beziehung_all + $all_themes_record_array['th_beziehung'];
                    $th_politische_themen_all = $th_politische_themen_all + $all_themes_record_array['th_politische_themen'];
                    $th_digitales_all = $th_digitales_all + $all_themes_record_array['th_digitales'];
                    $th_vorstellung_all = $th_vorstellung_all + $all_themes_record_array['th_vorstellung'];
                }
            }

            $th_sucht_total_month = $th_sucht_total_month + $th_sucht_all;
            $th_gewalt_total_month = $th_gewalt_total_month + $th_gewalt_all;
            $th_kriminalitat_total_month = $th_kriminalitat_total_month + $th_kriminalitat_all;
            $th_gesundheit_total_month = $th_gesundheit_total_month + $th_gesundheit_all;
            $th_freizeit_total_month = $th_freizeit_total_month + $th_freizeit_all;
            $th_identitat_total_month = $th_identitat_total_month + $th_identitat_all;
            $th_wohnraum_total_month = $th_wohnraum_total_month + $th_wohnraum_all;
            $th_familie_total_month = $th_familie_total_month + $th_familie_all;
            $th_behorden_total_month = $th_behorden_total_month + $th_behorden_all;
            $th_finanzielle_total_month = $th_finanzielle_total_month + $th_finanzielle_all;
            $th_arbeit_total_month = $th_arbeit_total_month + $th_arbeit_all;
            $th_beziehung_total_month = $th_beziehung_total_month + $th_beziehung_all;
            $th_politische_themen_total_month = $th_politische_themen_total_month + $th_politische_themen_all;
            $th_digitales_total_month = $th_digitales_total_month + $th_digitales_all;
            $th_vorstellung_total_month = $th_vorstellung_total_month + $th_vorstellung_all;

            $date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
        }

            $objPHPExcel->getActiveSheet()->SetCellValue($column.$row12_m, $n_u12_m_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$row12_w, $n_u12_w_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$row14_m, $n_u14_m_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$row14_w, $n_u14_w_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$row14_17_m, $n_u14_17_m_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$row14_17_w, $n_u14_17_w_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$row18_20_m, $n_u18_20_m_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$row18_20_w, $n_u18_20_w_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$row21_24_m, $n_u21_24_m_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$row21_24_w, $n_u21_24_w_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$row25_26_m, $n_u25_26_m_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$row25_26_w, $n_u25_26_w_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$row27_m, $n_o27_m_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$row27_w, $n_o27_w_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowmale, $male_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowfemale, $female_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowbekannt, $n_bekannt_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowunbekannt, $n_unbekannt_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowsucht, $th_sucht_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowgewalt, $th_gewalt_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowkriminalitat, $th_kriminalitat_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowgesundheit, $th_gesundheit_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowfreizeit, $th_freizeit_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowidentitat, $th_identitat_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowwohnraum, $th_wohnraum_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowfamilie, $th_familie_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowbehorden, $th_behorden_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowfinanzielle, $th_finanzielle_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowarbeit, $th_arbeit_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowbeziehung, $th_beziehung_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowpolitische_themen, $th_politische_themen_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowdigitales, $th_digitales_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowvorstellung, $th_vorstellung_total_month);
    }


    /**
     * Filling wetter type column
     */

    if ($workpartnum == 1 || $workpartnum == 3){
        $wetterTypes = array();
        $wetterTypeRow = 4;

        $wetterQuery = mysqli_query($dbc, $wetterRequest);

        while($wetterArray = mysqli_fetch_array($wetterQuery)){

            if($wetterArray['wetter']!=$wetterType){
                $wetterType = $wetterArray['wetter'];
                $wetterTypeRow++;
                $wetterTypes[] = $wetterArray['wetter'];
            }
            $objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('R4'),'R'.$wetterTypeRow);
            $objPHPExcel->getActiveSheet()->SetCellValue('R'.$wetterTypeRow, $wetterType);
            $objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('T5'), 'S'.$wetterTypeRow);
            $objPHPExcel->getActiveSheet()->setCellValue('S'.$wetterTypeRow,'=SUM(T'.$wetterTypeRow.':AE'.$wetterTypeRow.')');
        }

        for($m = 1; $m <=12; $m++){

            $wetterTypeRow = 5;
            $wetterTypeCounter = 0;
            if($m==1){$column='T';}else if($m==2){$column='U';}else if($m==3){$column='V';}else if($m==4){$column='W';}else if($m==5){$column='X';}else if($m==6){$column='Y';}else if($m==7){$column='Z';}else if($m==8){$column='AA';}else if($m==9){$column='AB';}else if($m==10){$column='AC';}else if($m==11){$column='AD';}else if($m==12){$column='AE';}

            while($wetterTypeCounter<sizeof($wetterTypes)) {
                
                $numPeopleAllWetter = 0;

                $wetterQuery = mysqli_query($dbc, $wetterRequest);

                while ($wetterArray = mysqli_fetch_array($wetterQuery)) {
                    if ($wetterArray['wetter'] == $wetterTypes[$wetterTypeCounter]) {
                        if ($m == $wetterArray['month']) {
                            $numPeopleAllWetter = $numPeopleAllWetter + $wetterArray['n_u12_m'] + $wetterArray['n_u12_w'] + $wetterArray['n_u14_m'] + $wetterArray['n_u14_w'] + $wetterArray['n_u14_17_m'] + $wetterArray['n_u14_17_w'] + $wetterArray['n_u18_20_m'] + $wetterArray['n_u18_20_w'] + $wetterArray['n_u21_24_m'] + $wetterArray['n_u21_24_w'] + $wetterArray['n_u25_26_m'] + $wetterArray['n_u25_26_w'] + $wetterArray['n_o27_m'] + $wetterArray['n_o27_w'];
                        }
                    }
                }
                $objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('T5'), $column.$wetterTypeRow);
                $objPHPExcel->getActiveSheet()->SetCellValue($column.$wetterTypeRow, $numPeopleAllWetter);
                $wetterTypeCounter++;
                $wetterTypeRow++;
            }
        }
    }  
}

/* -------------------- This is the end of main 4 forms filling func ---------------------------- */

/**
 * Stadteil spreadsheet declaration
 */
    $year_part = '2017-0';
    $begining_day = '-01';
    $end_day = '-31';

    $stadtteilTypes = array();
    $stadtteilType = "";

    $stadtteilRequest = "SELECT * FROM platz ORDER BY stadtteil_name";

    $stadtteilQuery = mysqli_query($dbc, $stadtteilRequest);

    while($stadtteilArray = mysqli_fetch_array($stadtteilQuery)){

        if($stadtteilArray['stadtteil_name']!=$stadtteilType){
            $stadtteilType = $stadtteilArray['stadtteil_name'];
            $stadtteilTypes[] = $stadtteilArray['stadtteil_name'];
        }
    }

    foreach($stadtteilTypes as $value){
        $clonedWorksheet = clone $objPHPExcel->getSheetByName('Streetwork_extra');
        $clonedWorksheet->setTitle($value);
        $objPHPExcel->addSheet($clonedWorksheet);
        $objPHPExcel->getSheetByName($value)->SetCellValue("B1", $value." streetwork");

        $objPHPExcel->setActiveSheetIndexByName($value);

        // Loop through mounths
        for ($j=1; $j <= 12; $j++) {

            $n_u12_m_total_month = 0;
            $n_u12_w_total_month = 0;
            $n_u14_m_total_month = 0;
            $n_u14_w_total_month = 0;
            $n_u14_17_m_total_month = 0;
            $n_u14_17_w_total_month = 0;
            $n_u18_20_m_total_month = 0;
            $n_u18_20_w_total_month = 0;
            $n_u21_24_m_total_month = 0;
            $n_u21_24_w_total_month = 0;
            $n_u25_26_m_total_month = 0;
            $n_u25_26_w_total_month = 0;
            $n_o27_m_total_month = 0;
            $n_o27_w_total_month = 0;
            $male_total_month = 0;
            $female_total_month = 0;
            $n_bekannt_total_month = 0;
            $n_unbekannt_total_month = 0;
            $th_sucht_total_month = 0;
            $th_gewalt_total_month = 0;
            $th_kriminalitat_total_month = 0;
            $th_gesundheit_total_month = 0;
            $th_freizeit_total_month = 0;
            $th_identitat_total_month = 0;
            $th_wohnraum_total_month = 0;
            $th_familie_total_month = 0;
            $th_behorden_total_month = 0;
            $th_finanzielle_total_month = 0;
            $th_arbeit_total_month = 0;
            $th_beziehung_total_month = 0;
            $th_politische_themen_total_month = 0;
            $th_digitales_total_month = 0;
            $th_vorstellung_total_month = 0;

            /**
             * Assigning the months to the columns
             */
            if($j==1){$column='D';}else if($j==2){$column='E';}else if($j==3){$column='F';}else if($j==4){$column='G';}else if($j==5){$column='H';}else if($j==6){$column='I';}else if($j==7){$column='J';}else if($j==8){$column='K';}else if($j==9){$column='L';}else if($j==10){$column='M';}else if($j==11){$column='N';}else if($j==12){$column='O';}
        
            $streetworkTimeRow12 = 5;
            $streetworkTimeRow14 = 21;
            $streetworkTimeRow14_17 = 37;
            $streetworkTimeRow18_26 = 53;
            $streetworkTimeRow27 = 69;
            // $hourPart = 12;
        
            for ($hourPart = 12; $hourPart <= 23;) {
        
                $n12 = 0;
                $n14 = 0;
                $n14_17 = 0;
                $n18_26 = 0;
                $n27 = 0;
        
                $beginningTime = $hourPart.':00';
                $endTime = $hourPart.':59';
        
                $beginningTimeFormated = new DateTime($beginningTime);
                $endTimeFormated = new DateTime($endTime);
        
                $allWorksStreetworkTimeingRequest = "SELECT month(datum) AS month, uhrzeit, n_u12_m, n_u12_w, n_u14_m, n_u14_w, n_u14_17_m, n_u14_17_w, n_u18_20_m, n_u18_20_w, n_u21_24_m, n_u21_24_w,  n_u25_26_m, n_u25_26_w, n_o27_m, n_o27_w, stadtteil_name FROM streetwork_record left join platz ON streetwork_record.platz = platz.platz_name where stadtteil_name='$value' and year(datum)='2017'";
                
                $allWorksStreetworkTimeingResponse = mysqli_query($dbc, $allWorksStreetworkTimeingRequest);
        
                while ($allWorksStreetworkTimeingArray = mysqli_fetch_array($allWorksStreetworkTimeingResponse)) {
                    if($allWorksStreetworkTimeingArray['month']==$j){
                        $streetworkTime = new DateTime($allWorksStreetworkTimeingArray['uhrzeit']);
                        if ($streetworkTime>=$beginningTimeFormated && $streetworkTime<=$endTimeFormated) {
        
                            $n12 = $n12 + $allWorksStreetworkTimeingArray['n_u12_m'] + $allWorksStreetworkTimeingArray['n_u12_w'];
                            $n14 = $n14 + $allWorksStreetworkTimeingArray['n_u14_m'] + $allWorksStreetworkTimeingArray['n_u14_w'];
                            $n14_17 = $n14_17 + $allWorksStreetworkTimeingArray['n_u14_17_m'] + $allWorksStreetworkTimeingArray['n_u14_17_w'];
                            $n18_26 = $n18_26 + $allWorksStreetworkTimeingArray['n_u18_20_m'] + $allWorksStreetworkTimeingArray['n_u18_20_w'] + $allWorksStreetworkTimeingArray['n_u21_24_m'] + $allWorksStreetworkTimeingArray['n_u21_24_w'] + $allWorksStreetworkTimeingArray['n_u25_26_m'] + $allWorksStreetworkTimeingArray['n_u25_26_w'];
                            $n27 = $n27 + $allWorksStreetworkTimeingArray['n_o27_m'] + $allWorksStreetworkTimeingArray['n_o27_w'];
                        }
                    }
                }
                $objPHPExcel->getActiveSheet()->SetCellValue($column.$streetworkTimeRow12, $n12);
                $objPHPExcel->getActiveSheet()->SetCellValue($column.$streetworkTimeRow14, $n14);
                $objPHPExcel->getActiveSheet()->SetCellValue($column.$streetworkTimeRow14_17, $n14_17);
                $objPHPExcel->getActiveSheet()->SetCellValue($column.$streetworkTimeRow18_26, $n18_26);
                $objPHPExcel->getActiveSheet()->SetCellValue($column.$streetworkTimeRow27, $n27);
        
                
                $streetworkTimeRow12++;
                $streetworkTimeRow14++;
                $streetworkTimeRow14_17++;
                $streetworkTimeRow18_26++;
                $streetworkTimeRow27++;
                $hourPart++;
            }

            //$all_works_record_request = "SELECT datum, n_u12_m, n_u12_w, n_u14_m, n_u14_w, n_u14_17_m, n_u14_17_w, n_u18_20_m, n_u18_20_w, n_u21_24_m, n_u21_24_w, n_u25_26_m, n_u25_26_w, n_o27_m, n_o27_w FROM streetwork_record WHERE year(datum)='2017'";

            //$all_bekannt_unbekannt_record_request = "SELECT datum, n_bekannt, n_unbekannt FROM streetwork_record WHERE year(datum)='2017'";

            //$all_themes_record_request = "SELECT datum, th_sucht, th_gewalt, th_kriminalitat, th_gesundheit, th_freizeit, th_identitat, th_wohnraum, th_familie, th_behorden, th_finanzielle, th_arbeit, th_beziehung, th_politische_themen, th_digitales, th_vorstellung FROM streetwork_record WHERE year(datum)='2017'";

            // $wetterRequest = "SELECT month(datum) AS month, n_u12_m, n_u12_w, n_u14_m, n_u14_w, n_u14_17_m, n_u14_17_w, n_u18_20_m, n_u18_20_w, n_u21_24_m, n_u21_24_w,  n_u25_26_m, n_u25_26_w, n_o27_m, n_o27_w, wetter FROM streetwork_record WHERE year(datum)='2017' ORDER BY wetter";

            //Loop declares the days quantity of the month
            $year_part = '2017-0';
            $begining_day = '-01';
            $end_day = '-31';

            if ($j==1 || $j==3 || $j==5 || $j==7 || $j==8 || $j==10 || $j==12) {
                $end_day = '-31';
                if ($j >= 9) {
                    $year_part = '2017-';
                }
            }
            if ($j >= 10) {
                $year_part = '2017-';
            }
            if ($j==4 || $j==6 || $j==9 || $j==11) {
                $end_day = '-30';
                if ($j >= 10) {
                    $year_part = '2017-';
                }
            }
            if ($j==2){
                $end_day = '-28';
            }
            
            $date = $year_part.$j.$begining_day;
            $end_date = $year_part.$j.$end_day;

            /**
             * Assigning the months to the columns
             */
            if($j==1){$column='T';}else if($j==2){$column='U';}else if($j==3){$column='V';}else if($j==4){$column='W';}else if($j==5){$column='X';}else if($j==6){$column='Y';}else if($j==7){$column='Z';}else if($j==8){$column='AA';}else if($j==9){$column='AB';}else if($j==10){$column='AC';}else if($j==11){$column='AD';}else if($j==12){$column='AE';}
            
            while (strtotime($date) <= strtotime($end_date)) {

                /**
                 * Loop over number of people met
                 */
                $n_u12_m_all = 0;
                $n_u12_w_all = 0;
                $n_u14_m_all = 0;
                $n_u14_w_all = 0;
                $n_u14_17_m_all = 0;
                $n_u14_17_w_all = 0;
                $n_u18_20_m_all = 0;
                $n_u18_20_w_all = 0;
                $n_u21_24_m_all = 0;
                $n_u21_24_w_all = 0;
                $n_u25_26_m_all = 0;
                $n_u25_26_w_all = 0;
                $n_o27_m_all = 0;
                $n_o27_w_all = 0;
                $male_all = 0;
                $female_all = 0;
                
                $all_works_record_request = "SELECT datum, n_u12_m, n_u12_w, n_u14_m, n_u14_w, n_u14_17_m, n_u14_17_w, n_u18_20_m, n_u18_20_w, n_u21_24_m, n_u21_24_w, n_u25_26_m, n_u25_26_w, n_o27_m, n_o27_w, stadtteil_name FROM streetwork_record left join platz ON streetwork_record.platz = platz.platz_name where stadtteil_name='$value' and year(datum)='2017'";

                $all_works_record_query = mysqli_query($dbc, $all_works_record_request);
    
                while ($all_works_record_array = mysqli_fetch_array($all_works_record_query)) {
    
                    if ($all_works_record_array['datum']==$date) {
    
                        $n_u12_m = $all_works_record_array['n_u12_m'];
                        $n_u12_w = $all_works_record_array['n_u12_w'];
                        $n_u14_m = $all_works_record_array['n_u14_m'];
                        $n_u14_w = $all_works_record_array['n_u14_w'];
                        $n_u14_17_m = $all_works_record_array['n_u14_17_m'];
                        $n_u14_17_w = $all_works_record_array['n_u14_17_w'];
                        $n_u18_20_m = $all_works_record_array['n_u18_20_m'];
                        $n_u18_20_w = $all_works_record_array['n_u18_20_w'];
                        $n_u21_24_m = $all_works_record_array['n_u21_24_m'];
                        $n_u21_24_w = $all_works_record_array['n_u21_24_w'];
                        $n_u25_26_m = $all_works_record_array['n_u25_26_m'];
                        $n_u25_26_w = $all_works_record_array['n_u25_26_w'];
                        $n_o27_m = $all_works_record_array['n_o27_m'];
                        $n_o27_w = $all_works_record_array['n_o27_w'];
    
                        $male = $n_u12_m + $n_u14_m + $n_u14_17_m + $n_u18_20_m + $n_u21_24_m + $n_u25_26_m + $n_o27_m;
                        $female = $n_u12_w + $n_u14_w + $n_u14_17_w + $n_u18_20_w + $n_u21_24_w + $n_u25_26_w + $n_o27_w;
    
                        $n_u12_m_all = $n_u12_m_all + $n_u12_m;
                        $n_u12_w_all = $n_u12_w_all + $n_u12_w;
                        $n_u14_m_all = $n_u14_m_all + $n_u14_m;
                        $n_u14_w_all = $n_u14_w_all + $n_u14_w;
                        $n_u14_17_m_all = $n_u14_17_m_all + $n_u14_17_m;
                        $n_u14_17_w_all = $n_u14_17_w_all + $n_u14_17_w;
                        $n_u18_20_m_all = $n_u18_20_m_all + $n_u18_20_m;
                        $n_u18_20_w_all = $n_u18_20_w_all + $n_u18_20_w;
                        $n_u21_24_m_all = $n_u21_24_m_all + $n_u21_24_m;
                        $n_u21_24_w_all = $n_u21_24_w_all + $n_u21_24_w;
                        $n_u25_26_m_all = $n_u25_26_m_all + $n_u25_26_m;
                        $n_u25_26_w_all = $n_u25_26_w_all + $n_u25_26_w;
                        $n_o27_m_all = $n_o27_m_all + $n_o27_m;
                        $n_o27_w_all = $n_o27_w_all + $n_o27_w;
                        $male_all = $male_all + $male;
                        $female_all = $female_all + $female;
                    }
                }
    
                $n_u12_m_total_month = $n_u12_m_total_month + $n_u12_m_all;
                $n_u12_w_total_month = $n_u12_w_total_month + $n_u12_w_all;
                $n_u14_m_total_month = $n_u14_m_total_month + $n_u14_m_all;
                $n_u14_w_total_month = $n_u14_w_total_month + $n_u14_w_all;
                $n_u14_17_m_total_month = $n_u14_17_m_total_month + $n_u14_17_m_all;
                $n_u14_17_w_total_month = $n_u14_17_w_total_month + $n_u14_17_w_all;
                $n_u18_20_m_total_month = $n_u18_20_m_total_month + $n_u18_20_m_all;
                $n_u18_20_w_total_month = $n_u18_20_w_total_month + $n_u18_20_w_all;
                $n_u21_24_m_total_month = $n_u21_24_m_total_month + $n_u21_24_m_all;
                $n_u21_24_w_total_month = $n_u21_24_w_total_month + $n_u21_24_w_all;
                $n_u25_26_m_total_month = $n_u25_26_m_total_month + $n_u25_26_m_all;
                $n_u25_26_w_total_month = $n_u25_26_w_total_month + $n_u25_26_w_all;
                $n_o27_m_total_month = $n_o27_m_total_month + $n_o27_m_all;
                $n_o27_w_total_month = $n_o27_w_total_month + $n_o27_w_all;
                $male_total_month = $male_total_month + $male_all;
                $female_total_month = $female_total_month + $female_all;      
    
                if ($workpartnum != 4){
                    /**
                     * Loop for bekannt & unbekannt table
                     */
                    $n_bekannt_all = 0;
                    $n_unbekannt_all = 0;
                    
                    $all_bekannt_unbekannt_record_request = "SELECT datum, n_bekannt, n_unbekannt, stadtteil_name FROM streetwork_record left join platz ON streetwork_record.platz = platz.platz_name where stadtteil_name='$value' and year(datum)='2017'";

                    $all_bekannt_unbekannt_record_query = mysqli_query($dbc, $all_bekannt_unbekannt_record_request)or die("MySQL error: " . mysqli_error($dbc) . "<hr>\nQuery: $all_bekannt_unbekannt_record_request");;
    
                    while ($all_bekannt_unbekannt_record_array = mysqli_fetch_array($all_bekannt_unbekannt_record_query)) {
                        if ($all_bekannt_unbekannt_record_array['datum']==$date) {
                            $n_bekannt = $all_bekannt_unbekannt_record_array['n_bekannt'];
                            $n_unbekannt = $all_bekannt_unbekannt_record_array['n_unbekannt'];
    
                            $n_bekannt_all = $n_bekannt_all + $n_bekannt;
                            $n_unbekannt_all = $n_unbekannt_all + $n_unbekannt;
                        }
                    }
    
                    $n_bekannt_total_month = $n_bekannt_total_month + $n_bekannt_all;
                    $n_unbekannt_total_month = $n_unbekannt_total_month + $n_unbekannt_all;
                }
    
                /**
                 * Loop over themes (sucht; gewalt, kriminalitat ...)
                 */
                $th_sucht_all = 0;
                $th_gewalt_all = 0;
                $th_kriminalitat_all = 0;
                $th_gesundheit_all = 0;
                $th_freizeit_all = 0;
                $th_identitat_all = 0;
                $th_wohnraum_all = 0;
                $th_familie_all = 0;
                $th_behorden_all = 0;
                $th_finanzielle_all = 0;
                $th_arbeit_all = 0;
                $th_beziehung_all = 0;
                $th_politische_themen_all = 0;
                $th_digitales_all = 0;
                $th_vorstellung_all = 0;
    
                $all_themes_record_request = "SELECT datum, th_sucht, th_gewalt, th_kriminalitat, th_gesundheit, th_freizeit, th_identitat, th_wohnraum, th_familie, th_behorden, th_finanzielle, th_arbeit, th_beziehung, th_politische_themen, th_digitales, th_vorstellung, stadtteil_name FROM streetwork_record left join platz ON streetwork_record.platz = platz.platz_name where stadtteil_name='$value' and year(datum)='2017'";

                $all_themes_record_query = mysqli_query($dbc, $all_themes_record_request);
    
                while ($all_themes_record_array = mysqli_fetch_array($all_themes_record_query)) {
                    if ($all_themes_record_array['datum']==$date) {
    
                        $th_sucht_all = $th_sucht_all + $all_themes_record_array['th_sucht'];
                        $th_gewalt_all = $th_gewalt_all + $all_themes_record_array['th_gewalt'];
                        $th_kriminalitat_all = $th_kriminalitat_all + $all_themes_record_array['th_kriminalitat'];
                        $th_gesundheit_all = $th_gesundheit_all + $all_themes_record_array['th_gesundheit'];
                        $th_freizeit_all = $th_freizeit_all + $all_themes_record_array['th_freizeit'];
                        $th_identitat_all = $th_identitat_all + $all_themes_record_array['th_identitat'];
                        $th_wohnraum_all = $th_wohnraum_all + $all_themes_record_array['th_wohnraum'];
                        $th_familie_all = $th_familie_all + $all_themes_record_array['th_familie'];
                        $th_behorden_all = $th_behorden_all + $all_themes_record_array['th_behorden'];
                        $th_finanzielle_all = $th_finanzielle_all + $all_themes_record_array['th_finanzielle'];
                        $th_arbeit_all = $th_arbeit_all + $all_themes_record_array['th_arbeit'];
                        $th_beziehung_all = $th_beziehung_all + $all_themes_record_array['th_beziehung'];
                        $th_politische_themen_all = $th_politische_themen_all + $all_themes_record_array['th_politische_themen'];
                        $th_digitales_all = $th_digitales_all + $all_themes_record_array['th_digitales'];
                        $th_vorstellung_all = $th_vorstellung_all + $all_themes_record_array['th_vorstellung'];
                    }
                }
    
                $th_sucht_total_month = $th_sucht_total_month + $th_sucht_all;
                $th_gewalt_total_month = $th_gewalt_total_month + $th_gewalt_all;
                $th_kriminalitat_total_month = $th_kriminalitat_total_month + $th_kriminalitat_all;
                $th_gesundheit_total_month = $th_gesundheit_total_month + $th_gesundheit_all;
                $th_freizeit_total_month = $th_freizeit_total_month + $th_freizeit_all;
                $th_identitat_total_month = $th_identitat_total_month + $th_identitat_all;
                $th_wohnraum_total_month = $th_wohnraum_total_month + $th_wohnraum_all;
                $th_familie_total_month = $th_familie_total_month + $th_familie_all;
                $th_behorden_total_month = $th_behorden_total_month + $th_behorden_all;
                $th_finanzielle_total_month = $th_finanzielle_total_month + $th_finanzielle_all;
                $th_arbeit_total_month = $th_arbeit_total_month + $th_arbeit_all;
                $th_beziehung_total_month = $th_beziehung_total_month + $th_beziehung_all;
                $th_politische_themen_total_month = $th_politische_themen_total_month + $th_politische_themen_all;
                $th_digitales_total_month = $th_digitales_total_month + $th_digitales_all;
                $th_vorstellung_total_month = $th_vorstellung_total_month + $th_vorstellung_all;
    
                $date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
            }
    
                $objPHPExcel->getActiveSheet()->SetCellValue($column.$row12_m, $n_u12_m_total_month);
                $objPHPExcel->getActiveSheet()->SetCellValue($column.$row12_w, $n_u12_w_total_month);
                $objPHPExcel->getActiveSheet()->SetCellValue($column.$row14_m, $n_u14_m_total_month);
                $objPHPExcel->getActiveSheet()->SetCellValue($column.$row14_w, $n_u14_w_total_month);
                $objPHPExcel->getActiveSheet()->SetCellValue($column.$row14_17_m, $n_u14_17_m_total_month);
                $objPHPExcel->getActiveSheet()->SetCellValue($column.$row14_17_w, $n_u14_17_w_total_month);
                $objPHPExcel->getActiveSheet()->SetCellValue($column.$row18_20_m, $n_u18_20_m_total_month);
                $objPHPExcel->getActiveSheet()->SetCellValue($column.$row18_20_w, $n_u18_20_w_total_month);
                $objPHPExcel->getActiveSheet()->SetCellValue($column.$row21_24_m, $n_u21_24_m_total_month);
                $objPHPExcel->getActiveSheet()->SetCellValue($column.$row21_24_w, $n_u21_24_w_total_month);
                $objPHPExcel->getActiveSheet()->SetCellValue($column.$row25_26_m, $n_u25_26_m_total_month);
                $objPHPExcel->getActiveSheet()->SetCellValue($column.$row25_26_w, $n_u25_26_w_total_month);
                $objPHPExcel->getActiveSheet()->SetCellValue($column.$row27_m, $n_o27_m_total_month);
                $objPHPExcel->getActiveSheet()->SetCellValue($column.$row27_w, $n_o27_w_total_month);
                $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowmale, $male_total_month);
                $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowfemale, $female_total_month);
                $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowbekannt, $n_bekannt_total_month);
                $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowunbekannt, $n_unbekannt_total_month);
                $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowsucht, $th_sucht_total_month);
                $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowgewalt, $th_gewalt_total_month);
                $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowkriminalitat, $th_kriminalitat_total_month);
                $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowgesundheit, $th_gesundheit_total_month);
                $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowfreizeit, $th_freizeit_total_month);
                $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowidentitat, $th_identitat_total_month);
                $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowwohnraum, $th_wohnraum_total_month);
                $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowfamilie, $th_familie_total_month);
                $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowbehorden, $th_behorden_total_month);
                $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowfinanzielle, $th_finanzielle_total_month);
                $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowarbeit, $th_arbeit_total_month);
                $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowbeziehung, $th_beziehung_total_month);
                $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowpolitische_themen, $th_politische_themen_total_month);
                $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowdigitales, $th_digitales_total_month);
                $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowvorstellung, $th_vorstellung_total_month);
        }

    /**
     * Filling ausfallgrund type column
     */
    
    $ausfallgrundTypes = array();
    $ausfallgrundTypeRow = 55;
    $ausfallgrundType = '';

    $ausfallgrundRequest = "SELECT month(datum) AS month, n_u12_m, n_u12_w, n_u14_m, n_u14_w, n_u14_17_m, n_u14_17_w, n_u18_20_m, n_u18_20_w, n_u21_24_m, n_u21_24_w, n_u25_26_m, n_u25_26_w, n_o27_m, n_o27_w, ausfallgrund, stadtteil_name FROM streetwork_record left join platz ON streetwork_record.platz = platz.platz_name WHERE year(datum)='2017' and stadtteil_name = '$value' ORDER BY ausfallgrund";

    $ausfallgrundQuery = mysqli_query($dbc, $ausfallgrundRequest);

    while($ausfallgrundArray = mysqli_fetch_array($ausfallgrundQuery)){

        if($ausfallgrundArray['ausfallgrund']!=$ausfallgrundType){
            $ausfallgrundType = $ausfallgrundArray['ausfallgrund'];
            $ausfallgrundTypes[] = $ausfallgrundArray['ausfallgrund'];
            $objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('R54'),'R'.$ausfallgrundTypeRow);
            $objPHPExcel->getActiveSheet()->SetCellValue('R'.$ausfallgrundTypeRow, $ausfallgrundType);
            $objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('S42'), 'S'.$ausfallgrundTypeRow);
            $objPHPExcel->getActiveSheet()->setCellValue('S'.$ausfallgrundTypeRow,'=SUM(T'.$ausfallgrundTypeRow.':AE'.$ausfallgrundTypeRow.')');
            $ausfallgrundTypeRow++;
        }  
    }

    for($m = 1; $m <=12; $m++){

        $ausfallgrundTypeRow = 55;
        $ausfallgrundTypeCounter = 0;
        if($m==1){$column='T';}else if($m==2){$column='U';}else if($m==3){$column='V';}else if($m==4){$column='W';}else if($m==5){$column='X';}else if($m==6){$column='Y';}else if($m==7){$column='Z';}else if($m==8){$column='AA';}else if($m==9){$column='AB';}else if($m==10){$column='AC';}else if($m==11){$column='AD';}else if($m==12){$column='AE';}

        while($ausfallgrundTypeCounter<sizeof($ausfallgrundTypes)) {
            
            $numAllAusfallgrund = 0;
            $ausfallgrundDate = "";

            $ausfallgrundQuery = mysqli_query($dbc, $ausfallgrundRequest);

            while ($ausfallgrundArray = mysqli_fetch_array($ausfallgrundQuery)) {
                if ($ausfallgrundArray['ausfallgrund'] == $ausfallgrundTypes[$ausfallgrundTypeCounter]) {
                    if ($m == $ausfallgrundArray['month']) {
                        $numAllAusfallgrund++;
                    }
                }
            }
            $objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('T5'), $column.$ausfallgrundTypeRow);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$ausfallgrundTypeRow, $numAllAusfallgrund);
            $ausfallgrundTypeCounter++;
            $ausfallgrundTypeRow++;
        }
    }

/**
 * Filling mobilitat type column
 */

$mobilitatTypes = array();
$mobilitatTypeRow = $ausfallgrundTypeRow + 4;
$mobilitatTypeRowHeader = $ausfallgrundTypeRow + 3;
$mobilitatTypeRowTitle = $ausfallgrundTypeRow + 2;
$mobilitatType = '';
// copyRange($sheetIndex, 'R4:AE4', 'R68');

$objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('R3'), 'R'.$mobilitatTypeRowTitle);
$objPHPExcel->getActiveSheet()->setCellValue('R'.$mobilitatTypeRowTitle,'Mobilitat');
$objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('R4'), 'R'.$mobilitatTypeRowHeader);
$objPHPExcel->getActiveSheet()->setCellValue('R'.$mobilitatTypeRowHeader,'Month');
$objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('R4'), 'S'.$mobilitatTypeRowHeader);
$objPHPExcel->getActiveSheet()->setCellValue('S'.$mobilitatTypeRowHeader,'Year (all)');
$objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('R4'), 'T'.$mobilitatTypeRowHeader);
$objPHPExcel->getActiveSheet()->setCellValue('T'.$mobilitatTypeRowHeader,'January');
$objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('R4'), 'U'.$mobilitatTypeRowHeader);
$objPHPExcel->getActiveSheet()->setCellValue('U'.$mobilitatTypeRowHeader,'February');
$objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('R4'), 'V'.$mobilitatTypeRowHeader);
$objPHPExcel->getActiveSheet()->setCellValue('V'.$mobilitatTypeRowHeader,'March');
$objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('R4'), 'W'.$mobilitatTypeRowHeader);
$objPHPExcel->getActiveSheet()->setCellValue('W'.$mobilitatTypeRowHeader,'April');
$objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('R4'), 'X'.$mobilitatTypeRowHeader);
$objPHPExcel->getActiveSheet()->setCellValue('X'.$mobilitatTypeRowHeader,'May');
$objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('R4'), 'Y'.$mobilitatTypeRowHeader);
$objPHPExcel->getActiveSheet()->setCellValue('Y'.$mobilitatTypeRowHeader,'June');
$objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('R4'), 'Z'.$mobilitatTypeRowHeader);
$objPHPExcel->getActiveSheet()->setCellValue('Z'.$mobilitatTypeRowHeader,'July');
$objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('R4'), 'AA'.$mobilitatTypeRowHeader);
$objPHPExcel->getActiveSheet()->setCellValue('AA'.$mobilitatTypeRowHeader,'August');
$objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('R4'), 'AB'.$mobilitatTypeRowHeader);
$objPHPExcel->getActiveSheet()->setCellValue('AB'.$mobilitatTypeRowHeader,'September');
$objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('R4'), 'AC'.$mobilitatTypeRowHeader);
$objPHPExcel->getActiveSheet()->setCellValue('AC'.$mobilitatTypeRowHeader,'October');
$objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('R4'), 'AD'.$mobilitatTypeRowHeader);
$objPHPExcel->getActiveSheet()->setCellValue('AD'.$mobilitatTypeRowHeader,'November');
$objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('R4'), 'AE'.$mobilitatTypeRowHeader);
$objPHPExcel->getActiveSheet()->setCellValue('AE'.$mobilitatTypeRowHeader,'December');

$mobilitatRequest = "SELECT month(datum) AS month, n_u12_m, n_u12_w, n_u14_m, n_u14_w, n_u14_17_m, n_u14_17_w, n_u18_20_m, n_u18_20_w, n_u21_24_m, n_u21_24_w, n_u25_26_m, n_u25_26_w, n_o27_m, n_o27_w, mobilitat, stadtteil_name FROM streetwork_record left join platz ON streetwork_record.platz = platz.platz_name WHERE year(datum)='2017' and stadtteil_name = '$value' ORDER BY mobilitat";

$mobilitatQuery = mysqli_query($dbc, $mobilitatRequest);

while($mobilitatArray = mysqli_fetch_array($mobilitatQuery)){

    if($mobilitatArray['mobilitat']!=$mobilitatType){
        $mobilitatType = $mobilitatArray['mobilitat'];
        $mobilitatTypes[] = $mobilitatArray['mobilitat'];
        $objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('R54'),'R'.$mobilitatTypeRow);
        $objPHPExcel->getActiveSheet()->SetCellValue('R'.$mobilitatTypeRow, $mobilitatType);
        $objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('S42'), 'S'.$mobilitatTypeRow);
        $objPHPExcel->getActiveSheet()->setCellValue('S'.$mobilitatTypeRow,'=SUM(T'.$mobilitatTypeRow.':AE'.$mobilitatTypeRow.')');
        $mobilitatTypeRow++;
    }  
}

for($m = 1; $m <=12; $m++){

    $mobilitatTypeRow = $ausfallgrundTypeRow + 4;
    $mobilitatTypeCounter = 0;
    if($m==1){$column='T';}else if($m==2){$column='U';}else if($m==3){$column='V';}else if($m==4){$column='W';}else if($m==5){$column='X';}else if($m==6){$column='Y';}else if($m==7){$column='Z';}else if($m==8){$column='AA';}else if($m==9){$column='AB';}else if($m==10){$column='AC';}else if($m==11){$column='AD';}else if($m==12){$column='AE';}

    while($mobilitatTypeCounter<sizeof($mobilitatTypes)) {
        
        $numAllMobilitat = 0;
        $mobilitatDate = "";

        $mobilitatQuery = mysqli_query($dbc, $mobilitatRequest);

        while ($mobilitatArray = mysqli_fetch_array($mobilitatQuery)) {
            if ($mobilitatArray['mobilitat'] == $mobilitatTypes[$mobilitatTypeCounter]) {
                if ($m == $mobilitatArray['month']) {
                    $numAllMobilitat++;
                }
            }
        }
        $objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('T5'), $column.$mobilitatTypeRow);
        $objPHPExcel->getActiveSheet()->SetCellValue($column.$mobilitatTypeRow, $numAllMobilitat);
        $mobilitatTypeCounter++;
        $mobilitatTypeRow++;
    }
}

}


/**
 * Streetwork extra spreadsheet
 */
$year_part = '2017-0';
$begining_day = '-01';
$end_day = '-31';

$sheetIndex = 6;
    
$objPHPExcel->setActiveSheetIndex($sheetIndex);

// Loop through mounths
for ($j=1; $j <= 12; $j++) {

    $n_u12_m_total_month = 0;
    $n_u12_w_total_month = 0;
    $n_u14_m_total_month = 0;
    $n_u14_w_total_month = 0;
    $n_u14_17_m_total_month = 0;
    $n_u14_17_w_total_month = 0;
    $n_u18_20_m_total_month = 0;
    $n_u18_20_w_total_month = 0;
    $n_u21_24_m_total_month = 0;
    $n_u21_24_w_total_month = 0;
    $n_u25_26_m_total_month = 0;
    $n_u25_26_w_total_month = 0;
    $n_o27_m_total_month = 0;
    $n_o27_w_total_month = 0;
    $male_total_month = 0;
    $female_total_month = 0;
    $n_bekannt_total_month = 0;
    $n_unbekannt_total_month = 0;
    $th_sucht_total_month = 0;
    $th_gewalt_total_month = 0;
    $th_kriminalitat_total_month = 0;
    $th_gesundheit_total_month = 0;
    $th_freizeit_total_month = 0;
    $th_identitat_total_month = 0;
    $th_wohnraum_total_month = 0;
    $th_familie_total_month = 0;
    $th_behorden_total_month = 0;
    $th_finanzielle_total_month = 0;
    $th_arbeit_total_month = 0;
    $th_beziehung_total_month = 0;
    $th_politische_themen_total_month = 0;
    $th_digitales_total_month = 0;
    $th_vorstellung_total_month = 0;

    /**
     * Assigning the months to the columns
     */
    if($j==1){$column='D';}else if($j==2){$column='E';}else if($j==3){$column='F';}else if($j==4){$column='G';}else if($j==5){$column='H';}else if($j==6){$column='I';}else if($j==7){$column='J';}else if($j==8){$column='K';}else if($j==9){$column='L';}else if($j==10){$column='M';}else if($j==11){$column='N';}else if($j==12){$column='O';}

    $streetworkTimeRow12 = 5;
    $streetworkTimeRow14 = 21;
    $streetworkTimeRow14_17 = 37;
    $streetworkTimeRow18_26 = 53;
    $streetworkTimeRow27 = 69;
    // $hourPart = 12;
    
    for ($hourPart = 12; $hourPart <= 23;) {
    
        $n12 = 0;
        $n14 = 0;
        $n14_17 = 0;
        $n18_26 = 0;
        $n27 = 0;

        $beginningTime = $hourPart.':00';
        $endTime = $hourPart.':59';

        $beginningTimeFormated = new DateTime($beginningTime);
        $endTimeFormated = new DateTime($endTime);

        $allWorksStreetworkTimeingRequest = "SELECT month(datum) AS month, uhrzeit, n_u12_m, n_u12_w, n_u14_m, n_u14_w, n_u14_17_m, n_u14_17_w, n_u18_20_m, n_u18_20_w, n_u21_24_m, n_u21_24_w,  n_u25_26_m, n_u25_26_w, n_o27_m, n_o27_w FROM streetwork_record WHERE year(datum)='2017'";
            
            $allWorksStreetworkTimeingResponse = mysqli_query($dbc, $allWorksStreetworkTimeingRequest);
    
            while ($allWorksStreetworkTimeingArray = mysqli_fetch_array($allWorksStreetworkTimeingResponse)) {
                if($allWorksStreetworkTimeingArray['month']==$j){
                    $streetworkTime = new DateTime($allWorksStreetworkTimeingArray['uhrzeit']);
                    if ($streetworkTime>=$beginningTimeFormated && $streetworkTime<=$endTimeFormated) {
    
                        $n12 = $n12 + $allWorksStreetworkTimeingArray['n_u12_m'] + $allWorksStreetworkTimeingArray['n_u12_w'];
                        $n14 = $n14 + $allWorksStreetworkTimeingArray['n_u14_m'] + $allWorksStreetworkTimeingArray['n_u14_w'];
                        $n14_17 = $n14_17 + $allWorksStreetworkTimeingArray['n_u14_17_m'] + $allWorksStreetworkTimeingArray['n_u14_17_w'];
                        $n18_26 = $n18_26 + $allWorksStreetworkTimeingArray['n_u18_20_m'] + $allWorksStreetworkTimeingArray['n_u18_20_w'] + $allWorksStreetworkTimeingArray['n_u21_24_m'] + $allWorksStreetworkTimeingArray['n_u21_24_w'] + $allWorksStreetworkTimeingArray['n_u25_26_m'] + $allWorksStreetworkTimeingArray['n_u25_26_w'];
                        $n27 = $n27 + $allWorksStreetworkTimeingArray['n_o27_m'] + $allWorksStreetworkTimeingArray['n_o27_w'];
                    }
                }
            }
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$streetworkTimeRow12, $n12);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$streetworkTimeRow14, $n14);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$streetworkTimeRow14_17, $n14_17);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$streetworkTimeRow18_26, $n18_26);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$streetworkTimeRow27, $n27);
    
            
            $streetworkTimeRow12++;
            $streetworkTimeRow14++;
            $streetworkTimeRow14_17++;
            $streetworkTimeRow18_26++;
            $streetworkTimeRow27++;
            $hourPart++;
        }

        //Loop declares the days quantity of the month
        $year_part = '2017-0';
        $begining_day = '-01';
        $end_day = '-31';

        if ($j==1 || $j==3 || $j==5 || $j==7 || $j==8 || $j==10 || $j==12) {
            $end_day = '-31';
            if ($j >= 9) {
                $year_part = '2017-';
            }
        }
        if ($j >= 10) {
            $year_part = '2017-';
        }
        if ($j==4 || $j==6 || $j==9 || $j==11) {
            $end_day = '-30';
            if ($j >= 10) {
                $year_part = '2017-';
            }
        }
        if ($j==2){
            $end_day = '-28';
        }
        
        $date = $year_part.$j.$begining_day;
        $end_date = $year_part.$j.$end_day;

        /**
         * Assigning the months to the columns
         */
        if($j==1){$column='T';}else if($j==2){$column='U';}else if($j==3){$column='V';}else if($j==4){$column='W';}else if($j==5){$column='X';}else if($j==6){$column='Y';}else if($j==7){$column='Z';}else if($j==8){$column='AA';}else if($j==9){$column='AB';}else if($j==10){$column='AC';}else if($j==11){$column='AD';}else if($j==12){$column='AE';}
        
        while (strtotime($date) <= strtotime($end_date)) {

            /**
             * Loop over number of people met
             */
            $n_u12_m_all = 0;
            $n_u12_w_all = 0;
            $n_u14_m_all = 0;
            $n_u14_w_all = 0;
            $n_u14_17_m_all = 0;
            $n_u14_17_w_all = 0;
            $n_u18_20_m_all = 0;
            $n_u18_20_w_all = 0;
            $n_u21_24_m_all = 0;
            $n_u21_24_w_all = 0;
            $n_u25_26_m_all = 0;
            $n_u25_26_w_all = 0;
            $n_o27_m_all = 0;
            $n_o27_w_all = 0;
            $male_all = 0;
            $female_all = 0;
            
            $all_works_record_request = "SELECT datum, n_u12_m, n_u12_w, n_u14_m, n_u14_w, n_u14_17_m, n_u14_17_w, n_u18_20_m, n_u18_20_w, n_u21_24_m, n_u21_24_w, n_u25_26_m, n_u25_26_w, n_o27_m, n_o27_w FROM streetwork_record where year(datum)='2017'";

            $all_works_record_query = mysqli_query($dbc, $all_works_record_request);

            while ($all_works_record_array = mysqli_fetch_array($all_works_record_query)) {

                if ($all_works_record_array['datum']==$date) {

                    $n_u12_m = $all_works_record_array['n_u12_m'];
                    $n_u12_w = $all_works_record_array['n_u12_w'];
                    $n_u14_m = $all_works_record_array['n_u14_m'];
                    $n_u14_w = $all_works_record_array['n_u14_w'];
                    $n_u14_17_m = $all_works_record_array['n_u14_17_m'];
                    $n_u14_17_w = $all_works_record_array['n_u14_17_w'];
                    $n_u18_20_m = $all_works_record_array['n_u18_20_m'];
                    $n_u18_20_w = $all_works_record_array['n_u18_20_w'];
                    $n_u21_24_m = $all_works_record_array['n_u21_24_m'];
                    $n_u21_24_w = $all_works_record_array['n_u21_24_w'];
                    $n_u25_26_m = $all_works_record_array['n_u25_26_m'];
                    $n_u25_26_w = $all_works_record_array['n_u25_26_w'];
                    $n_o27_m = $all_works_record_array['n_o27_m'];
                    $n_o27_w = $all_works_record_array['n_o27_w'];

                    $male = $n_u12_m + $n_u14_m + $n_u14_17_m + $n_u18_20_m + $n_u21_24_m + $n_u25_26_m + $n_o27_m;
                    $female = $n_u12_w + $n_u14_w + $n_u14_17_w + $n_u18_20_w + $n_u21_24_w + $n_u25_26_w + $n_o27_w;

                    $n_u12_m_all = $n_u12_m_all + $n_u12_m;
                    $n_u12_w_all = $n_u12_w_all + $n_u12_w;
                    $n_u14_m_all = $n_u14_m_all + $n_u14_m;
                    $n_u14_w_all = $n_u14_w_all + $n_u14_w;
                    $n_u14_17_m_all = $n_u14_17_m_all + $n_u14_17_m;
                    $n_u14_17_w_all = $n_u14_17_w_all + $n_u14_17_w;
                    $n_u18_20_m_all = $n_u18_20_m_all + $n_u18_20_m;
                    $n_u18_20_w_all = $n_u18_20_w_all + $n_u18_20_w;
                    $n_u21_24_m_all = $n_u21_24_m_all + $n_u21_24_m;
                    $n_u21_24_w_all = $n_u21_24_w_all + $n_u21_24_w;
                    $n_u25_26_m_all = $n_u25_26_m_all + $n_u25_26_m;
                    $n_u25_26_w_all = $n_u25_26_w_all + $n_u25_26_w;
                    $n_o27_m_all = $n_o27_m_all + $n_o27_m;
                    $n_o27_w_all = $n_o27_w_all + $n_o27_w;
                    $male_all = $male_all + $male;
                    $female_all = $female_all + $female;
                }
            }

            $n_u12_m_total_month = $n_u12_m_total_month + $n_u12_m_all;
            $n_u12_w_total_month = $n_u12_w_total_month + $n_u12_w_all;
            $n_u14_m_total_month = $n_u14_m_total_month + $n_u14_m_all;
            $n_u14_w_total_month = $n_u14_w_total_month + $n_u14_w_all;
            $n_u14_17_m_total_month = $n_u14_17_m_total_month + $n_u14_17_m_all;
            $n_u14_17_w_total_month = $n_u14_17_w_total_month + $n_u14_17_w_all;
            $n_u18_20_m_total_month = $n_u18_20_m_total_month + $n_u18_20_m_all;
            $n_u18_20_w_total_month = $n_u18_20_w_total_month + $n_u18_20_w_all;
            $n_u21_24_m_total_month = $n_u21_24_m_total_month + $n_u21_24_m_all;
            $n_u21_24_w_total_month = $n_u21_24_w_total_month + $n_u21_24_w_all;
            $n_u25_26_m_total_month = $n_u25_26_m_total_month + $n_u25_26_m_all;
            $n_u25_26_w_total_month = $n_u25_26_w_total_month + $n_u25_26_w_all;
            $n_o27_m_total_month = $n_o27_m_total_month + $n_o27_m_all;
            $n_o27_w_total_month = $n_o27_w_total_month + $n_o27_w_all;
            $male_total_month = $male_total_month + $male_all;
            $female_total_month = $female_total_month + $female_all;      

            if ($workpartnum != 4){
                /**
                 * Loop for bekannt & unbekannt table
                 */
                $n_bekannt_all = 0;
                $n_unbekannt_all = 0;
                
                $all_bekannt_unbekannt_record_request = "SELECT datum, n_bekannt, n_unbekannt FROM streetwork_record where year(datum)='2017'";

                $all_bekannt_unbekannt_record_query = mysqli_query($dbc, $all_bekannt_unbekannt_record_request)or die("MySQL error: " . mysqli_error($dbc) . "<hr>\nQuery: $all_bekannt_unbekannt_record_request");;

                while ($all_bekannt_unbekannt_record_array = mysqli_fetch_array($all_bekannt_unbekannt_record_query)) {
                    if ($all_bekannt_unbekannt_record_array['datum']==$date) {
                        $n_bekannt = $all_bekannt_unbekannt_record_array['n_bekannt'];
                        $n_unbekannt = $all_bekannt_unbekannt_record_array['n_unbekannt'];

                        $n_bekannt_all = $n_bekannt_all + $n_bekannt;
                        $n_unbekannt_all = $n_unbekannt_all + $n_unbekannt;
                    }
                }

                $n_bekannt_total_month = $n_bekannt_total_month + $n_bekannt_all;
                $n_unbekannt_total_month = $n_unbekannt_total_month + $n_unbekannt_all;
            }

            /**
             * Loop over themes (sucht; gewalt, kriminalitat ...)
             */
            $th_sucht_all = 0;
            $th_gewalt_all = 0;
            $th_kriminalitat_all = 0;
            $th_gesundheit_all = 0;
            $th_freizeit_all = 0;
            $th_identitat_all = 0;
            $th_wohnraum_all = 0;
            $th_familie_all = 0;
            $th_behorden_all = 0;
            $th_finanzielle_all = 0;
            $th_arbeit_all = 0;
            $th_beziehung_all = 0;
            $th_politische_themen_all = 0;
            $th_digitales_all = 0;
            $th_vorstellung_all = 0;

            $all_themes_record_request = "SELECT datum, th_sucht, th_gewalt, th_kriminalitat, th_gesundheit, th_freizeit, th_identitat, th_wohnraum, th_familie, th_behorden, th_finanzielle, th_arbeit, th_beziehung, th_politische_themen, th_digitales, th_vorstellung FROM streetwork_record where year(datum)='2017'";

            $all_themes_record_query = mysqli_query($dbc, $all_themes_record_request);

            while ($all_themes_record_array = mysqli_fetch_array($all_themes_record_query)) {
                if ($all_themes_record_array['datum']==$date) {

                    $th_sucht_all = $th_sucht_all + $all_themes_record_array['th_sucht'];
                    $th_gewalt_all = $th_gewalt_all + $all_themes_record_array['th_gewalt'];
                    $th_kriminalitat_all = $th_kriminalitat_all + $all_themes_record_array['th_kriminalitat'];
                    $th_gesundheit_all = $th_gesundheit_all + $all_themes_record_array['th_gesundheit'];
                    $th_freizeit_all = $th_freizeit_all + $all_themes_record_array['th_freizeit'];
                    $th_identitat_all = $th_identitat_all + $all_themes_record_array['th_identitat'];
                    $th_wohnraum_all = $th_wohnraum_all + $all_themes_record_array['th_wohnraum'];
                    $th_familie_all = $th_familie_all + $all_themes_record_array['th_familie'];
                    $th_behorden_all = $th_behorden_all + $all_themes_record_array['th_behorden'];
                    $th_finanzielle_all = $th_finanzielle_all + $all_themes_record_array['th_finanzielle'];
                    $th_arbeit_all = $th_arbeit_all + $all_themes_record_array['th_arbeit'];
                    $th_beziehung_all = $th_beziehung_all + $all_themes_record_array['th_beziehung'];
                    $th_politische_themen_all = $th_politische_themen_all + $all_themes_record_array['th_politische_themen'];
                    $th_digitales_all = $th_digitales_all + $all_themes_record_array['th_digitales'];
                    $th_vorstellung_all = $th_vorstellung_all + $all_themes_record_array['th_vorstellung'];
                }
            }

            $th_sucht_total_month = $th_sucht_total_month + $th_sucht_all;
            $th_gewalt_total_month = $th_gewalt_total_month + $th_gewalt_all;
            $th_kriminalitat_total_month = $th_kriminalitat_total_month + $th_kriminalitat_all;
            $th_gesundheit_total_month = $th_gesundheit_total_month + $th_gesundheit_all;
            $th_freizeit_total_month = $th_freizeit_total_month + $th_freizeit_all;
            $th_identitat_total_month = $th_identitat_total_month + $th_identitat_all;
            $th_wohnraum_total_month = $th_wohnraum_total_month + $th_wohnraum_all;
            $th_familie_total_month = $th_familie_total_month + $th_familie_all;
            $th_behorden_total_month = $th_behorden_total_month + $th_behorden_all;
            $th_finanzielle_total_month = $th_finanzielle_total_month + $th_finanzielle_all;
            $th_arbeit_total_month = $th_arbeit_total_month + $th_arbeit_all;
            $th_beziehung_total_month = $th_beziehung_total_month + $th_beziehung_all;
            $th_politische_themen_total_month = $th_politische_themen_total_month + $th_politische_themen_all;
            $th_digitales_total_month = $th_digitales_total_month + $th_digitales_all;
            $th_vorstellung_total_month = $th_vorstellung_total_month + $th_vorstellung_all;

            $date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
        }

            $objPHPExcel->getActiveSheet()->SetCellValue($column.$row12_m, $n_u12_m_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$row12_w, $n_u12_w_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$row14_m, $n_u14_m_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$row14_w, $n_u14_w_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$row14_17_m, $n_u14_17_m_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$row14_17_w, $n_u14_17_w_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$row18_20_m, $n_u18_20_m_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$row18_20_w, $n_u18_20_w_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$row21_24_m, $n_u21_24_m_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$row21_24_w, $n_u21_24_w_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$row25_26_m, $n_u25_26_m_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$row25_26_w, $n_u25_26_w_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$row27_m, $n_o27_m_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$row27_w, $n_o27_w_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowmale, $male_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowfemale, $female_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowbekannt, $n_bekannt_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowunbekannt, $n_unbekannt_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowsucht, $th_sucht_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowgewalt, $th_gewalt_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowkriminalitat, $th_kriminalitat_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowgesundheit, $th_gesundheit_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowfreizeit, $th_freizeit_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowidentitat, $th_identitat_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowwohnraum, $th_wohnraum_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowfamilie, $th_familie_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowbehorden, $th_behorden_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowfinanzielle, $th_finanzielle_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowarbeit, $th_arbeit_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowbeziehung, $th_beziehung_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowpolitische_themen, $th_politische_themen_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowdigitales, $th_digitales_total_month);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$rowvorstellung, $th_vorstellung_total_month);
    }

/**
 * Filling ausfallgrund type column
 */

$ausfallgrundTypes = array();
$ausfallgrundTypeRow = 55;
$ausfallgrundType = '';

$ausfallgrundRequest = "SELECT month(datum) AS month, n_u12_m, n_u12_w, n_u14_m, n_u14_w, n_u14_17_m, n_u14_17_w, n_u18_20_m, n_u18_20_w, n_u21_24_m, n_u21_24_w, n_u25_26_m, n_u25_26_w, n_o27_m, n_o27_w, ausfallgrund FROM streetwork_record WHERE year(datum)='2017' ORDER BY ausfallgrund";

$ausfallgrundQuery = mysqli_query($dbc, $ausfallgrundRequest);

while($ausfallgrundArray = mysqli_fetch_array($ausfallgrundQuery)){

    if($ausfallgrundArray['ausfallgrund']!=$ausfallgrundType){
        $ausfallgrundType = $ausfallgrundArray['ausfallgrund'];
        $ausfallgrundTypes[] = $ausfallgrundArray['ausfallgrund'];
        $objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('R54'),'R'.$ausfallgrundTypeRow);
        $objPHPExcel->getActiveSheet()->SetCellValue('R'.$ausfallgrundTypeRow, $ausfallgrundType);
        $objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('S42'), 'S'.$ausfallgrundTypeRow);
        $objPHPExcel->getActiveSheet()->setCellValue('S'.$ausfallgrundTypeRow,'=SUM(T'.$ausfallgrundTypeRow.':AE'.$ausfallgrundTypeRow.')');
        $ausfallgrundTypeRow++;
    }  
}

for($m = 1; $m <=12; $m++){

    $ausfallgrundTypeRow = 55;
    $ausfallgrundTypeCounter = 0;
    if($m==1){$column='T';}else if($m==2){$column='U';}else if($m==3){$column='V';}else if($m==4){$column='W';}else if($m==5){$column='X';}else if($m==6){$column='Y';}else if($m==7){$column='Z';}else if($m==8){$column='AA';}else if($m==9){$column='AB';}else if($m==10){$column='AC';}else if($m==11){$column='AD';}else if($m==12){$column='AE';}

    while($ausfallgrundTypeCounter<sizeof($ausfallgrundTypes)) {
        
        $numAllAusfallgrund = 0;
        $ausfallgrundDate = "";

        $ausfallgrundQuery = mysqli_query($dbc, $ausfallgrundRequest);

        while ($ausfallgrundArray = mysqli_fetch_array($ausfallgrundQuery)) {
            if ($ausfallgrundArray['ausfallgrund'] == $ausfallgrundTypes[$ausfallgrundTypeCounter]) {
                if ($m == $ausfallgrundArray['month']) {
                    $numAllAusfallgrund++;
                }
            }
        }
        $objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('T5'), $column.$ausfallgrundTypeRow);
        $objPHPExcel->getActiveSheet()->SetCellValue($column.$ausfallgrundTypeRow, $numAllAusfallgrund);
        $ausfallgrundTypeCounter++;
        $ausfallgrundTypeRow++;
    }
}

/**
 * Filling mobilitat type column
 */

$mobilitatTypes = array();
$mobilitatTypeRow = $ausfallgrundTypeRow + 4;
$mobilitatTypeRowHeader = $ausfallgrundTypeRow + 3;
$mobilitatTypeRowTitle = $ausfallgrundTypeRow + 2;
$mobilitatType = '';
// copyRange($sheetIndex, 'R4:AE4', 'R68');

$objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('R3'), 'R'.$mobilitatTypeRowTitle);
$objPHPExcel->getActiveSheet()->setCellValue('R'.$mobilitatTypeRowTitle,'Mobilitat');
$objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('R4'), 'R'.$mobilitatTypeRowHeader);
$objPHPExcel->getActiveSheet()->setCellValue('R'.$mobilitatTypeRowHeader,'Month');
$objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('R4'), 'S'.$mobilitatTypeRowHeader);
$objPHPExcel->getActiveSheet()->setCellValue('S'.$mobilitatTypeRowHeader,'Year (all)');
$objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('R4'), 'T'.$mobilitatTypeRowHeader);
$objPHPExcel->getActiveSheet()->setCellValue('T'.$mobilitatTypeRowHeader,'January');
$objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('R4'), 'U'.$mobilitatTypeRowHeader);
$objPHPExcel->getActiveSheet()->setCellValue('U'.$mobilitatTypeRowHeader,'February');
$objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('R4'), 'V'.$mobilitatTypeRowHeader);
$objPHPExcel->getActiveSheet()->setCellValue('V'.$mobilitatTypeRowHeader,'March');
$objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('R4'), 'W'.$mobilitatTypeRowHeader);
$objPHPExcel->getActiveSheet()->setCellValue('W'.$mobilitatTypeRowHeader,'April');
$objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('R4'), 'X'.$mobilitatTypeRowHeader);
$objPHPExcel->getActiveSheet()->setCellValue('X'.$mobilitatTypeRowHeader,'May');
$objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('R4'), 'Y'.$mobilitatTypeRowHeader);
$objPHPExcel->getActiveSheet()->setCellValue('Y'.$mobilitatTypeRowHeader,'June');
$objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('R4'), 'Z'.$mobilitatTypeRowHeader);
$objPHPExcel->getActiveSheet()->setCellValue('Z'.$mobilitatTypeRowHeader,'July');
$objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('R4'), 'AA'.$mobilitatTypeRowHeader);
$objPHPExcel->getActiveSheet()->setCellValue('AA'.$mobilitatTypeRowHeader,'August');
$objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('R4'), 'AB'.$mobilitatTypeRowHeader);
$objPHPExcel->getActiveSheet()->setCellValue('AB'.$mobilitatTypeRowHeader,'September');
$objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('R4'), 'AC'.$mobilitatTypeRowHeader);
$objPHPExcel->getActiveSheet()->setCellValue('AC'.$mobilitatTypeRowHeader,'October');
$objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('R4'), 'AD'.$mobilitatTypeRowHeader);
$objPHPExcel->getActiveSheet()->setCellValue('AD'.$mobilitatTypeRowHeader,'November');
$objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('R4'), 'AE'.$mobilitatTypeRowHeader);
$objPHPExcel->getActiveSheet()->setCellValue('AE'.$mobilitatTypeRowHeader,'December');

$mobilitatRequest = "SELECT month(datum) AS month, n_u12_m, n_u12_w, n_u14_m, n_u14_w, n_u14_17_m, n_u14_17_w, n_u18_20_m, n_u18_20_w, n_u21_24_m, n_u21_24_w, n_u25_26_m, n_u25_26_w, n_o27_m, n_o27_w, mobilitat FROM streetwork_record WHERE year(datum)='2017' ORDER BY mobilitat";

$mobilitatQuery = mysqli_query($dbc, $mobilitatRequest);

while($mobilitatArray = mysqli_fetch_array($mobilitatQuery)){

    if($mobilitatArray['mobilitat']!=$mobilitatType){
        $mobilitatType = $mobilitatArray['mobilitat'];
        $mobilitatTypes[] = $mobilitatArray['mobilitat'];
        $objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('R54'),'R'.$mobilitatTypeRow);
        $objPHPExcel->getActiveSheet()->SetCellValue('R'.$mobilitatTypeRow, $mobilitatType);
        $objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('S42'), 'S'.$mobilitatTypeRow);
        $objPHPExcel->getActiveSheet()->setCellValue('S'.$mobilitatTypeRow,'=SUM(T'.$mobilitatTypeRow.':AE'.$mobilitatTypeRow.')');
        $mobilitatTypeRow++;
    }  
}

for($m = 1; $m <=12; $m++){

    $mobilitatTypeRow = $ausfallgrundTypeRow + 4;
    $mobilitatTypeCounter = 0;
    if($m==1){$column='T';}else if($m==2){$column='U';}else if($m==3){$column='V';}else if($m==4){$column='W';}else if($m==5){$column='X';}else if($m==6){$column='Y';}else if($m==7){$column='Z';}else if($m==8){$column='AA';}else if($m==9){$column='AB';}else if($m==10){$column='AC';}else if($m==11){$column='AD';}else if($m==12){$column='AE';}

    while($mobilitatTypeCounter<sizeof($mobilitatTypes)) {
        
        $numAllMobilitat = 0;
        $mobilitatDate = "";

        $mobilitatQuery = mysqli_query($dbc, $mobilitatRequest);

        while ($mobilitatArray = mysqli_fetch_array($mobilitatQuery)) {
            if ($mobilitatArray['mobilitat'] == $mobilitatTypes[$mobilitatTypeCounter]) {
                if ($m == $mobilitatArray['month']) {
                    $numAllMobilitat++;
                }
            }
        }
        $objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('T5'), $column.$mobilitatTypeRow);
        $objPHPExcel->getActiveSheet()->SetCellValue($column.$mobilitatTypeRow, $numAllMobilitat);
        $mobilitatTypeCounter++;
        $mobilitatTypeRow++;
    }
}


// /**
//  * Streetwork extra table with timing
//  */
// $sheetIndex = 6;
// $objPHPExcel->setActiveSheetIndex($sheetIndex);

// // Looping through the months of the year
// for ($j=1; $j <= 12; $j++) {
//     /**
//      * Assigning the months to the columns
//      */
//     if($j==1){$column='D';}else if($j==2){$column='E';}else if($j==3){$column='F';}else if($j==4){$column='G';}else if($j==5){$column='H';}else if($j==6){$column='I';}else if($j==7){$column='J';}else if($j==8){$column='K';}else if($j==9){$column='L';}else if($j==10){$column='M';}else if($j==11){$column='N';}else if($j==12){$column='O';}

//     $streetworkTimeRow12 = 5;
//     $streetworkTimeRow14 = 21;
//     $streetworkTimeRow14_17 = 37;
//     $streetworkTimeRow18_26 = 53;
//     $streetworkTimeRow27 = 69;

//     $hourPart = 12;

//     while ($hourPart <= 23) {

//         $n12 = 0;
//         $n14 = 0;
//         $n14_17 = 0;
//         $n18_26 = 0;
//         $n27 = 0;

//         $beginningTime = $hourPart.':00';
//         $endTime = $hourPart.':59';

//         $beginningTimeFormated = new DateTime($beginningTime);
//         $endTimeFormated = new DateTime($endTime);

//         $allWorksStreetworkTimeingRequest = "SELECT month(datum) AS month, uhrzeit, n_u12_m, n_u12_w, n_u14_m, n_u14_w, n_u14_17_m, n_u14_17_w, n_u18_20_m, n_u18_20_w, n_u21_24_m, n_u21_24_w,  n_u25_26_m, n_u25_26_w, n_o27_m, n_o27_w FROM streetwork_record WHERE year(datum)='2017'";
        
//         $allWorksStreetworkTimeingResponse = mysqli_query($dbc, $allWorksStreetworkTimeingRequest);

//         while ($allWorksStreetworkTimeingArray = mysqli_fetch_array($allWorksStreetworkTimeingResponse)) {
//             if($allWorksStreetworkTimeingArray['month']==$j){
//                 $streetworkTime = new DateTime($allWorksStreetworkTimeingArray['uhrzeit']);
//                 if ($streetworkTime>=$beginningTimeFormated && $streetworkTime<=$endTimeFormated) {

//                     $n12 = $n12 + $allWorksStreetworkTimeingArray['n_u12_m'] + $allWorksStreetworkTimeingArray['n_u12_w'];
//                     $n14 = $n14 + $allWorksStreetworkTimeingArray['n_u14_m'] + $allWorksStreetworkTimeingArray['n_u14_w'];
//                     $n14_17 = $n14_17 + $allWorksStreetworkTimeingArray['n_u14_17_m'] + $allWorksStreetworkTimeingArray['n_u14_17_w'];
//                     $n18_26 = $n18_26 + $allWorksStreetworkTimeingArray['n_u18_20_m'] + $allWorksStreetworkTimeingArray['n_u18_20_w'] + $allWorksStreetworkTimeingArray['n_u21_24_m'] + $allWorksStreetworkTimeingArray['n_u21_24_w'] + $allWorksStreetworkTimeingArray['n_u25_26_m'] + $allWorksStreetworkTimeingArray['n_u25_26_w'];
//                     $n27 = $n27 + $allWorksStreetworkTimeingArray['n_o27_m'] + $allWorksStreetworkTimeingArray['n_o27_w'];
//                 }
//             }
//         }
//         $objPHPExcel->getActiveSheet()->SetCellValue($column.$streetworkTimeRow12, $n12);
//         $objPHPExcel->getActiveSheet()->SetCellValue($column.$streetworkTimeRow14, $n14);
//         $objPHPExcel->getActiveSheet()->SetCellValue($column.$streetworkTimeRow14_17, $n14_17);
//         $objPHPExcel->getActiveSheet()->SetCellValue($column.$streetworkTimeRow18_26, $n18_26);
//         $objPHPExcel->getActiveSheet()->SetCellValue($column.$streetworkTimeRow27, $n27);

//         $hourPart++;
//         $streetworkTimeRow12++;
//         $streetworkTimeRow14++;
//         $streetworkTimeRow14_17++;
//         $streetworkTimeRow18_26++;
//         $streetworkTimeRow27++;
//     }
// }

// /**
//  * Filling ausfallgrund type column
//  */

//     $ausfallgrundTypes = array();
//     $ausfallgrundTypeRow = 4;

//     $ausfallgrundRequest = "SELECT month(datum) AS month, n_u12_m, n_u12_w, n_u14_m, n_u14_w, n_u14_17_m, n_u14_17_w, n_u18_20_m, n_u18_20_w, n_u21_24_m, n_u21_24_w,  n_u25_26_m, n_u25_26_w, n_o27_m, n_o27_w, ausfallgrund FROM streetwork_record WHERE year(datum)='2017' ORDER BY ausfallgrund";

//     $ausfallgrundQuery = mysqli_query($dbc, $ausfallgrundRequest);

//     while($ausfallgrundArray = mysqli_fetch_array($ausfallgrundQuery)){

//         if($ausfallgrundArray['ausfallgrund']!=$ausfallgrundType){
//             $ausfallgrundType = $ausfallgrundArray['ausfallgrund'];
//             $ausfallgrundTypeRow++;
//             $ausfallgrundTypes[] = $ausfallgrundArray['ausfallgrund'];
//         }
//         $objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('R4'),'R'.$ausfallgrundTypeRow);
//         $objPHPExcel->getActiveSheet()->SetCellValue('R'.$ausfallgrundTypeRow, $ausfallgrundType);
//         $objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('T5'), 'S'.$ausfallgrundTypeRow);
//         $objPHPExcel->getActiveSheet()->setCellValue('S'.$ausfallgrundTypeRow,'=SUM(T'.$ausfallgrundTypeRow.':AE'.$ausfallgrundTypeRow.')');
//     }

//     for($m = 1; $m <=12; $m++){

//         $ausfallgrundTypeRow = 5;
//         $ausfallgrundTypeCounter = 0;
//         if($m==1){$column='T';}else if($m==2){$column='U';}else if($m==3){$column='V';}else if($m==4){$column='W';}else if($m==5){$column='X';}else if($m==6){$column='Y';}else if($m==7){$column='Z';}else if($m==8){$column='AA';}else if($m==9){$column='AB';}else if($m==10){$column='AC';}else if($m==11){$column='AD';}else if($m==12){$column='AE';}

//         while($ausfallgrundTypeCounter<sizeof($ausfallgrundTypes)) {
            
//             $numAllAusfallgrund = 0;
//             $ausfallgrundDate = "";

//             $ausfallgrundQuery = mysqli_query($dbc, $ausfallgrundRequest);

//             while ($ausfallgrundArray = mysqli_fetch_array($ausfallgrundQuery)) {
//                 if ($ausfallgrundArray['ausfallgrund'] == $ausfallgrundTypes[$ausfallgrundTypeCounter]) {
//                     if ($m == $ausfallgrundArray['month']) {
//                         $numAllAusfallgrund++;
//                         // $numAllAusfallgrund = $numAllAusfallgrund + $ausfallgrundArray['n_u12_m'] + $ausfallgrundArray['n_u12_w'] + $ausfallgrundArray['n_u14_m'] + $ausfallgrundArray['n_u14_w'] + $ausfallgrundArray['n_u14_17_m'] + $ausfallgrundArray['n_u14_17_w'] + $ausfallgrundArray['n_u18_20_m'] + $ausfallgrundArray['n_u18_20_w'] + $ausfallgrundArray['n_u21_24_m'] + $ausfallgrundArray['n_u21_24_w'] + $ausfallgrundArray['n_u25_26_m'] + $ausfallgrundArray['n_u25_26_w'] + $ausfallgrundArray['n_o27_m'] + $ausfallgrundArray['n_o27_w'];
//                     }
//                 }
//             }
//             $objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('T5'), $column.$ausfallgrundTypeRow);
//             $objPHPExcel->getActiveSheet()->SetCellValue($column.$ausfallgrundTypeRow, $numAllAusfallgrund);
//             $ausfallgrundTypeCounter++;
//             $ausfallgrundTypeRow++;
//         }
//     }

// /**
//      * Filling ausfallgrund type column
//      */
    
//     $ausfallgrundTypes = array();
//     $ausfallgrundTypeRow = 55;
//     $ausfallgrundType = '';

//     $ausfallgrundRequest = "SELECT month(datum) AS month, n_u12_m, n_u12_w, n_u14_m, n_u14_w, n_u14_17_m, n_u14_17_w, n_u18_20_m, n_u18_20_w, n_u21_24_m, n_u21_24_w,  n_u25_26_m, n_u25_26_w, n_o27_m, n_o27_w, ausfallgrund FROM streetwork_record WHERE year(datum)='2017' ORDER BY ausfallgrund";

//     $ausfallgrundQuery = mysqli_query($dbc, $ausfallgrundRequest);

//     while($ausfallgrundArray = mysqli_fetch_array($ausfallgrundQuery)){

//         if($ausfallgrundArray['ausfallgrund']!=$ausfallgrundType){
//             $ausfallgrundType = $ausfallgrundArray['ausfallgrund'];
//             $ausfallgrundTypes[] = $ausfallgrundArray['ausfallgrund'];
//             $objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('R54'),'R'.$ausfallgrundTypeRow);
//             $objPHPExcel->getActiveSheet()->SetCellValue('R'.$ausfallgrundTypeRow, $ausfallgrundType);
//             $objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('S42'), 'S'.$ausfallgrundTypeRow);
//             $objPHPExcel->getActiveSheet()->setCellValue('S'.$ausfallgrundTypeRow,'=SUM(T'.$ausfallgrundTypeRow.':AE'.$ausfallgrundTypeRow.')');
//             $ausfallgrundTypeRow++;
//         }  
//     }

//     for($m = 1; $m <=12; $m++){

//         $ausfallgrundTypeRow = 55;
//         $ausfallgrundTypeCounter = 0;
//         if($m==1){$column='T';}else if($m==2){$column='U';}else if($m==3){$column='V';}else if($m==4){$column='W';}else if($m==5){$column='X';}else if($m==6){$column='Y';}else if($m==7){$column='Z';}else if($m==8){$column='AA';}else if($m==9){$column='AB';}else if($m==10){$column='AC';}else if($m==11){$column='AD';}else if($m==12){$column='AE';}

//         while($ausfallgrundTypeCounter<sizeof($ausfallgrundTypes)) {
            
//             $numAllAusfallgrund = 0;
//             $ausfallgrundDate = "";

//             $ausfallgrundQuery = mysqli_query($dbc, $ausfallgrundRequest);

//             while ($ausfallgrundArray = mysqli_fetch_array($ausfallgrundQuery)) {
//                 if ($ausfallgrundArray['ausfallgrund'] == $ausfallgrundTypes[$ausfallgrundTypeCounter]) {
//                     if ($m == $ausfallgrundArray['month']) {
//                         $numAllAusfallgrund++;
//                         // $numAllAusfallgrund = $numAllAusfallgrund + $ausfallgrundArray['n_u12_m'] + $ausfallgrundArray['n_u12_w'] + $ausfallgrundArray['n_u14_m'] + $ausfallgrundArray['n_u14_w'] + $ausfallgrundArray['n_u14_17_m'] + $ausfallgrundArray['n_u14_17_w'] + $ausfallgrundArray['n_u18_20_m'] + $ausfallgrundArray['n_u18_20_w'] + $ausfallgrundArray['n_u21_24_m'] + $ausfallgrundArray['n_u21_24_w'] + $ausfallgrundArray['n_u25_26_m'] + $ausfallgrundArray['n_u25_26_w'] + $ausfallgrundArray['n_o27_m'] + $ausfallgrundArray['n_o27_w'];
//                     }
//                 }
//             }
//             $objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('T5'), $column.$ausfallgrundTypeRow);
//             $objPHPExcel->getActiveSheet()->SetCellValue($column.$ausfallgrundTypeRow, $numAllAusfallgrund);
//             $ausfallgrundTypeCounter++;
//             $ausfallgrundTypeRow++;
//         }
//     }
    
    




// header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
// header('Content-Disposition: attachment;filename="Statistik_vorschlag.xlsx"');
// header('Cache-Control: max-age=0');

$writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
// $writer->save('php://output');


ob_start();
$writer->save('php://output');
$xlsData = ob_get_contents();
ob_end_clean();
$response =  array(
    'op' => 'ok',
    'file' => "data:application/vnd.ms-excel;base64,".base64_encode($xlsData)
);
die(json_encode($response));

/** 
 * Copy range in PHPSpreadsheet/PHPExcel including styles
 **/



// function copyRange( Worksheet $sheet, $srcRange, $dstCell) {
//     // Validate source range. Examples: A2:A3, A2:AB2, A27:B100
//     if( !preg_match('/^([A-Z]+)(\d+):([A-Z]+)(\d+)$/', $srcRange, $srcRangeMatch) ) {
//         // Wrong source range
//         return;
//     }
//     // Validate destination cell. Examples: A2, AB3, A27
//     if( !preg_match('/^([A-Z]+)(\d+)$/', $dstCell, $destCellMatch) ) {
//         // Wrong destination cell
//         return;
//     }

//     $srcColumnStart = $srcRangeMatch[1];
//     $srcRowStart = $srcRangeMatch[2];
//     $srcColumnEnd = $srcRangeMatch[3];
//     $srcRowEnd = $srcRangeMatch[4];

//     $destColumnStart = $destCellMatch[1];
//     $destRowStart = $destCellMatch[2];

//     // For looping purposes we need to convert the indexes instead
//     // Note: We need to subtract 1 since column are 0-based and not 1-based like this method acts.

//     $srcColumnStart = Cell::columnIndexFromString($srcColumnStart) - 1;
//     $srcColumnEnd = Cell::columnIndexFromString($srcColumnEnd) - 1;
//     $destColumnStart = Cell::columnIndexFromString($destColumnStart) - 1;

//     $rowCount = 0;
//     for ($row = $srcRowStart; $row <= $srcRowEnd; $row++) {
//         $colCount = 0;
//         for ($col = $srcColumnStart; $col <= $srcColumnEnd; $col++) {
//             $cell = $sheet->getCellByColumnAndRow($col, $row);
//             $style = $sheet->getStyleByColumnAndRow($col, $row);
//             $dstCell = Cell::stringFromColumnIndex($destColumnStart + $colCount) . (string)($destRowStart + $rowCount);
//             $sheet->setCellValue($dstCell, $cell->getValue());
//             $sheet->duplicateStyle($style, $dstCell);

//             // Set width of column, but only once per row
//             if ($rowCount === 0) {
//                 $w = $sheet->getColumnDimensionByColumn($col)->getWidth();
//                 $sheet->getColumnDimensionByColumn ($destColumnStart + $colCount)->setAutoSize(false);
//                 $sheet->getColumnDimensionByColumn ($destColumnStart + $colCount)->setWidth($w);
//             }

//             $colCount++;
//         }

//         $h = $sheet->getRowDimension($row)->getRowHeight();
//         $sheet->getRowDimension($destRowStart + $rowCount)->setRowHeight($h);

//         $rowCount++;
//     }

//     foreach ($sheet->getMergeCells() as $mergeCell) {
//         $mc = explode(":", $mergeCell);
//         $mergeColSrcStart = Cell::columnIndexFromString(preg_replace("/[0-9]*/", "", $mc[0])) - 1;
//         $mergeColSrcEnd = Cell::columnIndexFromString(preg_replace("/[0-9]*/", "", $mc[1])) - 1;
//         $mergeRowSrcStart = ((int)preg_replace("/[A-Z]*/", "", $mc[0]));
//         $mergeRowSrcEnd = ((int)preg_replace("/[A-Z]*/", "", $mc[1]));

//         $relativeColStart = $mergeColSrcStart - $srcColumnStart;
//         $relativeColEnd = $mergeColSrcEnd - $srcColumnStart;
//         $relativeRowStart = $mergeRowSrcStart - $srcRowStart;
//         $relativeRowEnd = $mergeRowSrcEnd - $srcRowStart;

//         if (0 <= $mergeRowSrcStart && $mergeRowSrcStart >= $srcRowStart && $mergeRowSrcEnd <= $srcRowEnd) {
//             $targetColStart = Cell::stringFromColumnIndex($destColumnStart + $relativeColStart);
//             $targetColEnd = Cell::stringFromColumnIndex($destColumnStart + $relativeColEnd);
//             $targetRowStart = $destRowStart + $relativeRowStart;
//             $targetRowEnd = $destRowStart + $relativeRowEnd;

//             $merge = (string)$targetColStart . (string)($targetRowStart) . ":" . (string)$targetColEnd . (string)($targetRowEnd);
//             //Merge target cells
//             $sheet->mergeCells($merge);
//         }
//     }
// }

?>