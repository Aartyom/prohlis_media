<?php

	require_once('../../../resources/dbconnection.php');

	$err = array();
	$response = array();

	if (trim($_POST['mitarbeiter_username']) == '') {
		$err[] = 'mitarbeiter_username';
	} else {
		$mitarbeiter_username = trim($_POST['mitarbeiter_username']);
	}

	if (trim($_POST['arbeiter_short_name']) == '') {
		$err[] = 'arbeiter_short_name';
	} else {
		$arbeiter_short_name = trim($_POST['arbeiter_short_name']);
	}

	if (trim($_POST['mitarbeiter_password']) == '') {
		$err[] = 'mitarbeiter_password';
	} else {
		$mitarbeiter_password = trim($_POST['mitarbeiter_password']);
	}

	if (trim($_POST['arbeiter_first_name']) == '') {
		$err[] = 'arbeiter_first_name';
	} else {
		$arbeiter_first_name = trim($_POST['arbeiter_first_name']);
	}

	if (trim($_POST['arbeiter_last_name']) == '') {
		$err[] = 'arbeiter_last_name';
	} else {
		$arbeiter_last_name = trim($_POST['arbeiter_last_name']);
	}

	if (trim($_POST['work_status']) == '') {
		$err[] = 'work_status';
	} else {
		$work_status = trim($_POST['work_status']);
	}

	

	 if (empty($err)){

		if($work_status == 'user'){

			mysqli_query($dbc,"SELECT username FROM user WHERE username = '$mitarbeiter_username'");
			$affected_rows = mysqli_affected_rows($dbc);
			
			if ($affected_rows == 1) {
				$response['message'] = $mitarbeiter_username;
				echo json_encode($response);
				mysqli_close($dbc);
			} else {
				mysqli_query($dbc,"SELECT short_name FROM user WHERE short_name = '$arbeiter_short_name'");
				$affected_rows = mysqli_affected_rows($dbc);
				if ($affected_rows == 1) {
					$response['message'] = $arbeiter_short_name;
					echo json_encode($response);
					mysqli_close($dbc);
				}else{
					$hash = password_hash($mitarbeiter_password, PASSWORD_DEFAULT);
					mysqli_query($dbc,"INSERT INTO user (username, password, first_name, last_name, short_name, work_status) VALUES ('$mitarbeiter_username', '$hash', '$arbeiter_first_name', '$arbeiter_last_name', '$arbeiter_short_name', '$work_status')");
					$affected_rows = mysqli_affected_rows($dbc);
					if ($affected_rows == 1) {
						$response['message'] = "New mitarbeiter added";
						echo json_encode($response);
						mysqli_close($dbc);
					}else{
						$response['message'] = "Error with request execution";
						echo json_encode($response);
						mysqli_close($dbc);
					}
				}
			}
		}else if($work_status == 'worker'){
			mysqli_query($dbc,"SELECT short_name FROM user WHERE short_name = '$arbeiter_short_name'");
			$affected_rows = mysqli_affected_rows($dbc);
			if ($affected_rows == 1) {
				$response['message'] = $arbeiter_short_name;
				echo json_encode($response);
				mysqli_close($dbc);
			}else{
				mysqli_query($dbc,"INSERT INTO user (username, password, first_name, last_name, short_name, work_status) VALUES ('$mitarbeiter_username', '$mitarbeiter_password', '$arbeiter_first_name', '$arbeiter_last_name', '$arbeiter_short_name', '$work_status')");
				$affected_rows = mysqli_affected_rows($dbc);
				if ($affected_rows == 1) {
					$response['message'] = "New mitarbeiter added";
					echo json_encode($response);
					mysqli_close($dbc);
				}else{
					$response['message'] = "Error with request execution";
					echo json_encode($response);
					mysqli_close($dbc);
				}
			}
		}
	} else {
		$response['message'] = "Data missing";
		echo json_encode($response);
		mysqli_close($dbc);
	}
?>