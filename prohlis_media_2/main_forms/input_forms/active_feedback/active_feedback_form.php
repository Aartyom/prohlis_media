<?php
require('../../../resources/dbconnection.php');
$mitarbeiter_query = mysqli_query($dbc, "SELECT * FROM user");
$aktion_query = mysqli_query($dbc, "SELECT * FROM aktion ORDER BY aktion_type ASC");
$angebot_query = mysqli_query($dbc, "SELECT * FROM angebot ORDER BY angebot_name ASC");
$art_des_feedback_query = mysqli_query($dbc, "SELECT * FROM art_des_feedback ORDER BY art_des_feedback_type ASC");
$short_name_query = mysqli_query($dbc, 'SELECT * FROM user');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Active feedback</title>
    <!-- Bootstrap core CSS -->
    <link href="../../../resources/frameworks/bootstrap4/css/bootstrap.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="active_feedback_stylesheet.css" rel="stylesheet">
    <link href="../common-files/common-styles.css" rel="stylesheet">
    <link href="../../../resources/frameworks/toastr-master/build/toastr.min.css" rel="stylesheet">
    <link href="../../overall_tables/common_files/loader-stylesheet.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
    <!-- <script type="text/javascript" src="../resources/frameworks/toastr-master/build/toastr.min.js"></script> -->
</head>
<body>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
        <a class="navbar-brand" href="../../main_menu/main_menu.php">PROHLIS MEDIA</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="../../main_menu/main_menu.php">Hauptmenü</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../../overall_tables/active_feedback_overall/active_feedback_overall.php">Tabelle der Datensätze</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="current_user_username" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?php
                        $username = $_COOKIE['username'];
                        while ($current_user = mysqli_fetch_array($short_name_query)) {
                            if ($current_user['username'] == $username){
                                echo $current_user['first_name']." ".$current_user['last_name'];
                            }
                        }
                        ?>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdown01">
                        <a class="dropdown-item" href="#">Edit settings</a>
                        <a class="dropdown-item" href="../../../../../logout.php">Log out</a>
                    </div>
                </li>
            </ul>
            <form class="form-inline my-2 my-lg-0">
            </form>
        </div>
    </nav>

    <div class="container container-input-form">
        <div class="row">
            <div class="w-100 text-center" style="margin:5px 0px;">
                <h2><span class="text-uppercase">NEW ACTIVE FEEDBACK</span></h2>
            </div>
        </div>
        <div class="row d-flex justify-content-center mb-3">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="angebot">Angebot</label>
                    <div class="input-group input-group-sm angebot_red_line">
                        <select name="angebot" id="angebot" data-default-value="defaultv" class="custom-select d-block w-100" required>
                            <option value="defaultv" disabled selected hidden>Choose angebot</option>
                            <?php
                                while ($angebot = mysqli_fetch_array($angebot_query)) {
                                    echo '<option value"'.$angebot['angebot_name'].'">'.$angebot['angebot_name'].'</option>';
                                }
                            ?>
                        </select>
                        <div class="input-group-btn">
                            <div class="input-group-btn">
                                <button class="btn btn-primary bg-grey" type="button" data-toggle="modal" data-target="#addAngebot">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </button>
                                <button class="btn btn-primary bg-grey" type="button" data-toggle="modal" data-target="#deleteAngebot">
                                    <i class="fa fa-minus" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="datum">Datum</label>
                    <input type="text" class="form-control" id="datum" name="datum" placeholder="-" value="<?php echo date('d.m.o'); ?>">
                </div>
                
                <div class="form-group">
                    <label for="mitarbeiter">Mitarbeiter in</label>
                    <div class="input-group input-group-sm mitarbeiter02_red_line">
                        <select name="mitarbeiter02" id="mitarbeiter02" data-default-value="defaultv" class="custom-select d-block w-100" required>
                            <option value="defaultv" disabled selected hidden>Choose mitarbeiter</option>
                            <?php
                                while ($worker = mysqli_fetch_array($mitarbeiter_query)) {
                                    echo '<option value="'.$worker['short_name'].'">'.$worker['first_name']." ".$worker['last_name'].'</option>';
                                }
                            ?>
                        </select>
                        <div class="input-group-btn">
                            <div class="input-group-btn">
                                <button class="btn btn-primary bg-grey" type="button" data-toggle="modal" data-target="#addMitarbeiter">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </button>
                                <button class="btn btn-primary bg-grey" type="button" data-toggle="modal" data-target="#deleteMitarbeiter">
                                    <i class="fa fa-minus" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="an_wen">An wen?</label>
                    <input type="text" class="form-control" id="an_wen" name="an_wen" placeholder="-">
                </div>
                <div class="form-group">
                    <label for="art_des_feedback">Art des Feedback</label>
                    <div class="input-group input-group-sm art_des_feedback_red_line">
                        <select name="art_des_feedback" id="art_des_feedback" data-default-value="defaultv" class="custom-select d-block w-100" required>
                            <option value="defaultv" disabled selected hidden>Select art des feedback</option>
                            <?php
                                while ($art_des_feedback = mysqli_fetch_array($art_des_feedback_query)) {
                                    echo "<option value='".$art_des_feedback['art_des_feedback_type']."'>".$art_des_feedback['art_des_feedback_type']."</option>";
                                }
                            ?>
                        </select>
                        <div class="input-group-btn">
                            <div class="input-group-btn">
                                <button class="btn btn-primary bg-grey" type="button" data-toggle="modal" data-target="#addArtDesFeedback">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </button>
                                <button class="btn btn-primary bg-grey" type="button" data-toggle="modal" data-target="#deleteArtDesFeedback">
                                    <i class="fa fa-minus" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group h-50 mb-1">
                    <label for="inhalt_des_feedback">Inhalt des feedback</label>
                    <textarea class="form-control font-size-sm h-75" rows="3" id="inhalt_des_feedback" name="inhalt_des_feedback" placeholder="Write your feedback..."></textarea>
                </div>
                <div class="form-group h-50 mb-1">
                    <label for="weiterbearbeitung">Weiterbearbeitung/Zeitschiene</label>
                    <textarea class="form-control font-size-sm h-75" rows="3" id="weiterbearbeitung" name="weiterbearbeitung" placeholder="Weiterbearbeitung..."></textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col d-flex align-items-center justify-content-center">
                <div role="group" aria-label="Basic example" class="btn-group btn-group-lg">
                    <button type="button" class="btn btn-secondary trans-up" data-toggle="modal" data-target="#deleteLastRecord">rückgängig</button>
                    <button type="button" class="btn btn-secondary trans-up js_main_form_submit_button">Einreichen</button>
                    <button type="button" class="btn btn-secondary trans-up js_main_form_download_record_button">download</button>
                    <button type="button" class="btn btn-secondary trans-up js_main_form_new_record_button">neuer Rekord</button>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal angebot -->
    <div class="modal fade" id="addAngebot" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header text-center modal-header-custom">
                    <h3 class="modal-title w-100" id="exampleModalLabel">NEUE angebot</h3>
                </div>
                <div class="modal-body modal-body-custom">
                        <input type="text" name="angebot_type" class="form-control" id="angebot_type" placeholder="Angebot type..">
                </div>
                <div class="modal-footer d-flex justify-content-center modal-footer-custom">
                    <div role="group" aria-label="Basic example" class="btn-group btn-group-md">
                        <button type="button" class="btn btn-secondary trans-up" data-dismiss="modal">Abbrechen</button>
                        <button type="button" class="btn btn-secondary trans-up js_angebot_subform_submit_button">Einreichen</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Art Des Feedback -->
    <div class="modal fade" id="addArtDesFeedback" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header text-center modal-header-custom">
                    <h3 class="modal-title w-100" id="exampleModalLabel">NEUE Art Des Feedback</h3>
                </div>
                <div class="modal-body modal-body-custom">
                        <input type="text" name="art_des_feedback_type" class="form-control" id="art_des_feedback_type" placeholder="Art des feedback type..">
                </div>
                <div class="modal-footer d-flex justify-content-center modal-footer-custom">
                    <div role="group" aria-label="Basic example" class="btn-group btn-group-md">
                        <button type="button" class="btn btn-secondary trans-up" data-dismiss="modal">Abbrechen</button>
                        <button type="button" class="btn btn-secondary trans-up js_art_des_feedback_subform_submit_button">Einreichen</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

     <!-- Modal mitarbeiter -->
    <div class="modal fade" id="addMitarbeiter" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header text-center modal-header-custom">
                    <h3 class="modal-title w-100" id="exampleModalLabel">NEUE mitarbeiter</h3>
                </div>
                <div class="modal-body modal-body-custom">
                <div class="row mitarbeiter-subform-row">
                    <div class="col">
                        <p>To add new user, please fill in the selected fields, if you need to define person without access to the system, please select the "worker" option <div class="form-check">
                            <label class="custom-control custom-checkbox">
                                <input class="custom-control-input" type="checkbox" id="worker_mitarbeiter_check" name="worker_mitarbeiter_check">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description font-size-sm">WORKER</span>
                            </label>
                        </div></p>
                    </div>
                </div>
                    <div class="row mitarbeiter-subform-row">
                        <div class="col">
                        <div class="form-group">
                            <label for="mitarbeiter_username">Username</label>
                            <input type="text" class="form-control" id="mitarbeiter_username" name="mitarbeiter_username" placeholder="-">
                        </div>
                        </div>
                    </div>
                    <div class="row mitarbeiter-subform-row">
                        <div class="col">
                        <div class="form-group">
                            <label for="mitarbeiter_password">Password</label>
                            <input type="password" class="form-control" id="mitarbeiter_password" name="mitarbeiter_password" placeholder="-">
                        </div>
                        </div>
                        <div class="col">
                        <div class="form-group">
                            <label for="mitarbeiter_repeat_password">Repeat password</label>
                            <input type="password" class="form-control" id="mitarbeiter_repeat_password" name="mitarbeiter_repeat_password" placeholder="-">
                        </div>
                        </div>
                    </div>
                    <div class="row mitarbeiter-subform-row">
                        <div class="col">
                        <div class="form-group">
                            <label for="arbeiter_first_name">First name</label>
                            <input type="text" class="form-control" id="arbeiter_first_name" name="arbeiter_first_name" placeholder="-">
                        </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="arbeiter_last_name">Second name</label>
                                <input type="text" class="form-control" id="arbeiter_last_name" name="arbeiter_last_name" placeholder="-">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center modal-footer-custom">
                    <div role="group" aria-label="Basic example" class="btn-group btn-group-md">
                        <button type="button" class="btn btn-secondary trans-up" data-dismiss="modal">Abbrechen</button>
                        <button type="button" class="btn btn-secondary trans-up js_mitarbeiter_subform_submit_button">Einreichen</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Delete mitarbeiter -->
    <div class="modal fade" id="deleteMitarbeiter" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header text-center modal-header-custom">
                    <h3 class="modal-title w-100" id="exampleModalLabel">Achtung</h3>
                </div>
                <div class="modal-body modal-body-custom text-center">
                    <span>Sind Sie sicher, dass Sie löschen möchten</span>
                </div>
                <div class="modal-footer d-flex justify-content-center modal-footer-custom">
                    <div role="group" aria-label="Basic example" class="btn-group btn-group-md">
                        <button type="button" class="btn btn-secondary trans-up" data-dismiss="modal" style="background-color: rgba(255, 0, 0, 0.7);">Abbrechen</button>
                        <button type="button" class="btn btn-secondary trans-up js_btn_delete_mitarbeiter" style="background-color:rgba(0, 128, 0, 0.63);">Einreichen</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Delete Art Des Feedback -->
    <div class="modal fade" id="deleteArtDesFeedback" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header text-center modal-header-custom">
                    <h3 class="modal-title w-100" id="exampleModalLabel">Achtung</h3>
                </div>
                <div class="modal-body modal-body-custom text-center">
                    <span>Sind Sie sicher, dass Sie löschen möchten</span>
                </div>
                <div class="modal-footer d-flex justify-content-center modal-footer-custom">
                    <div role="group" aria-label="Basic example" class="btn-group btn-group-md">
                        <button type="button" class="btn btn-secondary trans-up" data-dismiss="modal" style="background-color: rgba(255, 0, 0, 0.7);" data-dismiss="modal">Abbrechen</button>
                        <button type="button" class="btn btn-secondary trans-up js_btn_delete_art_des_feedback" style="background-color:rgba(0, 128, 0, 0.63);">Einreichen</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Delete angebot -->
    <div class="modal fade" id="deleteAngebot" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header text-center modal-header-custom">
                    <h3 class="modal-title w-100" id="exampleModalLabel">Achtung</h3>
                </div>
                <div class="modal-body modal-body-custom text-center">
                    <span>Sind Sie sicher, dass Sie löschen möchten</span>
                </div>
                <div class="modal-footer d-flex justify-content-center modal-footer-custom">
                    <div role="group" aria-label="Basic example" class="btn-group btn-group-md">
                        <button type="button" class="btn btn-secondary trans-up" data-dismiss="modal" style="background-color: rgba(255, 0, 0, 0.7);" data-dismiss="modal">Abbrechen</button>
                        <button type="button" class="btn btn-secondary trans-up js_btn_delete_angebot" style="background-color:rgba(0, 128, 0, 0.63);">Einreichen</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Delete last record -->
    <div class="modal fade" id="deleteLastRecord" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header text-center modal-header-custom">
                    <h3 class="modal-title w-100" id="exampleModalLabel">Achtung</h3>
                </div>
                <div class="modal-body modal-body-custom text-center">
                    <span>Sind Sie sicher, dass Sie löschen möchten</span>
                </div>
                <div class="modal-footer d-flex justify-content-center modal-footer-custom">
                    <div role="group" aria-label="Basic example" class="btn-group btn-group-md">
                        <button type="button" class="btn btn-secondary trans-up" data-dismiss="modal" style="background-color: rgba(255, 0, 0, 0.7);" data-dismiss="modal">Abbrechen</button>
                        <button type="button" class="btn btn-secondary trans-up js_revert_button" style="background-color:rgba(0, 128, 0, 0.63);">Einreichen</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container container-loader" id="loader-popup">
        <div class="row">
            <div class="col-md-12">
                <h5 class="text-center mt-5 mb-5">Please do not close this window <br> until the file will be loaded..</h5>
                <div class="middle">
                    <div class="sk-folding-cube">
                        <div class="sk-cube1 sk-cube"></div>
                        <div class="sk-cube2 sk-cube"></div>
                        <div class="sk-cube4 sk-cube"></div>
                        <div class="sk-cube3 sk-cube"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="../../../resources/frameworks/assets4/js/jquery.min.js"></script>
    <script src="../../../resources/frameworks/assets4/js/popper.js"></script>
    <script src="../../../resources/frameworks/bootstrap4/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../../resources/frameworks/assets4/js/ie10-viewport-bug-workaround.js"></script>
    <!-- Notifications framework "Toastr" -->
    <script type="text/javascript" src="../../../resources/frameworks/toastr-master/build/toastr.min.js"></script>
    <!-- Custom scripts -->
    <script src="../../../helpersJS/subforms_add_listeners_js.js"></script>
    <script src="../../../helpersJS/subforms_remove_listeners_js.js"></script>
    <script src="../../../helpersJS/common_js_functions.js"></script>
    <script src="active_feedback_js.js"></script>
</body>
</html>