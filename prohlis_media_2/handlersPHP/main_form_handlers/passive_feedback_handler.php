<?php

require('../../resources/dbconnection.php');

	$err = array();
	$response = array();

	if (trim($_POST['angebot']) == '') {
		$err[] = 'Write angebot!';
	} else {
		$angebot = trim($_POST['angebot']);
	}

	if (trim($_POST['aktion']) == '') {
		$err[] = 'Write aktion!';
	} else {
		$aktion = trim($_POST['aktion']);
	}
	
	if (trim($_POST['datum']) == '') {
		$err[] = 'Write datum!';
	} else {
		$datum = trim($_POST['datum']);
	}
	
	if (trim($_POST['mitarbeiter']) == '') {
		$err[] = 'Write mitarbeiter!';
	} else {
		$mitarbeiter = trim($_POST['mitarbeiter']);
	}
	
	if (trim($_POST['positiv_resp']) == '') {
		$err[] = 'Write positiv_resp!';
	} else {
		$positiv_resp = trim($_POST['positiv_resp']);
	}

	if (trim($_POST['negativ_resp']) == '') {
		$err[] = 'Write negativ_resp!';
	} else {
		$negativ_resp = trim($_POST['negativ_resp']);
	}

	if (trim($_POST['schlussfolgerungen']) == '') {
		$err[] = 'Write schlussfolgerungen!';
	} else {
		$schlussfolgerungen = trim($_POST['schlussfolgerungen']);
	}

	if (trim($_POST['rahmenbedingungen']) == '') {
		$err[] = 'Write rahmenbedingungen!';
	} else {
		$rahmenbedingungen = trim($_POST['rahmenbedingungen']);
	}

	if (trim($_POST['teilnehmer_innen']) == '') {
		$err[] = 'Write teilnehmer_innen!';
	} else {
		$teilnehmer_innen = trim($_POST['teilnehmer_innen']);
	}

	if (trim($_POST['fb_anzahl']) == '') {
		$err[] = 'Write fb_anzahl!';
	} else {
		$fb_anzahl = trim($_POST['fb_anzahl']);
	}

	if (trim($_POST['fb_geber_innen']) == '') {
		$err[] = 'Write fb_geber_innen!';
	} else {
		$fb_geber_innen = trim($_POST['fb_geber_innen']);
	}

	if (trim($_POST['allgemeine_einschatzung']) == '') {
		$err[] = 'Write allgemeine_einschatzung!';
	} else {
		$allgemeine_einschatzung = trim($_POST['allgemeine_einschatzung']);
	}

	 if (empty($err)){
		
		$query = "INSERT INTO passive_feedback_record (angebot, aktion, datum, mitarbeiter, positiv_resp, negativ_resp, schlussfolgerungen, rahmenbedingungen, teilnehmer_innen, fb_anzahl, fb_geber_innen, allgemeine_einschatzung) VALUES ('$angebot', '$aktion', '$datum', '$mitarbeiter', '$positiv_resp', '$negativ_resp', '$schlussfolgerungen', '$rahmenbedingungen', '$teilnehmer_innen', '$fb_anzahl', '$fb_geber_innen', '$allgemeine_einschatzung')";
		mysqli_query($dbc,$query);
		$affected_rows = mysqli_affected_rows($dbc);
		
		if ($affected_rows == 1) {
			
			$response['message'] = "Record added";

		} else {

			$response['message'] = 'Error with request execution';
		}

		mysqli_close($dbc);
		echo json_encode($response);
		
	} else {

	 	echo 'Data missing'+$err;
	 }
?>
