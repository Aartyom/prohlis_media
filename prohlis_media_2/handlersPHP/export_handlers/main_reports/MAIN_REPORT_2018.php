<?php 
 
require '../../../resources/frameworks/phpspreadsheet/vendor/autoload.php';
require '../../../resources/dbconnection.php';
 
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
$objPHPExcel = $reader->load("../../../resources/excel-templates/18.xlsx");
 
$sheetIndex = 3; 

// $objPHPExcel->setActiveSheetIndex($sheetIndex);
// $objPHPExcel->getActiveSheet()->SetCellValue('G6', 'aasdas');

date_default_timezone_set('UTC'); 

$year_part = '2018-0'; 
$begining_day = '-01'; 
$end_day = '-31'; 
 
//* Loop over the mounths  
 
for ($j=1; $j <= 12; $j++) { 
 
  $objPHPExcel->setActiveSheetIndex($sheetIndex); 
  //echo '<p style="color:red">Sheet Index is: '.$sheetIndex.'</p>'; 
  $row = 5; 
 
    //* Loop over mounths where 31 day 
 
  if ($j==1 || $j==3 || $j==5 || $j==7 || $j==8 || $j==10 || $j==12) { 
 
    $end_day = '-31'; 
 
        //* if mounth number gets over 9, mounth number becomes two-digit 
    if ($j >= 9) { 
      $year_part = '2018-'; // change from "2018-0" to "2018-" 
    } 
  } 
 
   if ($j >= 10) { 
    $year_part = '2018-'; 
  } 
 
    //* Loop over mounths where 31 day 
 
  if ($j==4 || $j==6 || $j==9 || $j==11) { 
 
    $end_day = '-30'; 
 
    if ($j >= 10) { 
      $year_part = '2018-'; 
    } 
 
  } 
 
  if ($j==2){ 
    $end_day = '-28'; 
  } 
 
  $date = $year_part.$j.$begining_day; 
  $end_date = $year_part.$j.$end_day; 
 
 
  while (strtotime($date) <= strtotime($end_date)) { 
 
    $zx = 0; 
    $zc = 0; 
    $zv = 0; 
    $zb = 0; 
    $zn = 0; 
    $zm = 0; 
 
    $all_works_record_query = mysqli_query($dbc, "SELECT datum, n_u12_m, n_u12_w, n_u14_m, n_u14_w, n_u14_17_m, n_u14_17_w, n_u18_20_m, n_u18_20_w, n_u21_24_m, n_u21_24_w, n_u25_26_m, n_u25_26_w, n_o27_m, n_o27_w FROM streetwork_record UNION ALL SELECT datum, n_u12_m, n_u12_w, n_u14_m, n_u14_w, n_u14_17_m, n_u14_17_w, n_u18_20_m, n_u18_20_w, n_u21_24_m, n_u21_24_w, n_u25_26_m, n_u25_26_w, n_o27_m, n_o27_w FROM gruppenarbeit_record UNION ALL SELECT datum, n_u12_m, n_u12_w, n_u14_m, n_u14_w, n_u14_17_m, n_u14_17_w, n_u18_20_m, n_u18_20_w, n_u21_24_m, n_u21_24_w, n_u25_26_m, n_u25_26_w, n_o27_m, n_o27_w FROM einzelfallhilfe_record UNION ALL SELECT datum, n_u12_m, n_u12_w, n_u14_m, n_u14_w, n_u14_17_m, n_u14_17_w, n_u18_20_m, n_u18_20_w, n_u21_24_m, n_u21_24_w, n_u25_26_m, n_u25_26_w, n_o27_m, n_o27_w FROM gemeinwesenarbeit_record"); 
 
    while ($all_works_record_array = mysqli_fetch_array($all_works_record_query)) { 
 
            if ($all_works_record_array['datum']==$date) { 
                 
                /** 
                 * Collecting all men and women from 12-17 y.o.  "zx" and "zc" variables 
                 * **/ 
                $n_u12_17m_all = 0; 
                $n_u12_17m_all = $all_works_record_array['n_u12_m']+$all_works_record_array['n_u14_m']+$all_works_record_array['n_u14_17_m']; 
                $zx = $zx + $n_u12_17m_all; 
 
                $n_u12_17w_all = 0; 
                $n_u12_17w_all = $all_works_record_array['n_u12_w']+$all_works_record_array['n_u14_w']+$all_works_record_array['n_u14_17_w']; 
                $zc = $zc + $n_u12_17w_all; 
 
                /** 
                 * Collecting all men and women from 18-21 y.o.  "zv" and "zb" variables 
                **/ 
                $n_u18_20m_all = 0; 
                $n_u18_20m_all = $all_works_record_array['n_u18_20_m']; 
                $zv = $zv + $n_u18_20m_all; 
 
                $n_u18_20w_all = 0; 
                $n_u18_20w_all = $all_works_record_array['n_u18_20_w']; 
                $zb = $zb + $n_u18_20w_all; 
 
                /** 
                 * Collecting all men and women from 22-27 y.o.  "zn" and "zm" variables 
                **/ 
                $n_u22_27m_all = 0; 
                $n_u22_27m_all = $all_works_record_array['n_u21_24_m']+$all_works_record_array['n_u25_26_m']+$all_works_record_array['n_o27_m']; 
                $zn = $zn + $n_u22_27m_all; 
 
                $n_u22_27w_all = 0; 
                $n_u22_27w_all = $all_works_record_array['n_u21_24_w']+$all_works_record_array['n_u25_26_w']+$all_works_record_array['n_o27_w']; 
                $zm = $zm + $n_u22_27w_all; 
            } 
    } 
 
    //**** Filling the 6-13 field 
    $objPHPExcel->getActiveSheet()->SetCellValue('G'.$row, $zx); 
    //echo "sent to H".$row; 
    //echo "</br>".$date." ".$zx."</br>"; 
 
    //**** Filling the 14-17 field 
    $objPHPExcel->getActiveSheet()->SetCellValue('I'.$row, $zc); 
    // echo "sent to I".$row; 
    // echo "</br>".$date." ".$zc."</br>"; 
 
    //**** Filling the 18-20 field 
    $objPHPExcel->getActiveSheet()->SetCellValue('J'.$row, $zv); 
    // echo "sent to J".$row; 
    // echo "</br>".$date." ".$zv."</br>"; 
 
    //**** Filling the 21-24 field 
    $objPHPExcel->getActiveSheet()->SetCellValue('L'.$row, $zb); 
    // echo "sent to K".$row; 
    // echo "</br>".$date." ".$zb."</br>"; 
 
    //**** Filling the 25-26 field 
    $objPHPExcel->getActiveSheet()->SetCellValue('M'.$row, $zn); 
    // echo "sent to L".$row; 
    // echo "</br>".$date." ".$zn."</br>"; 
 
    //**** Filling the 27+ field 
    $objPHPExcel->getActiveSheet()->SetCellValue('O'.$row, $zm); 
    // echo "sent to M".$row; 
    // echo "</br>".$date." ".$zm."</br>"; 
 
 
    // ****** Streetwork ************************************************************************************************* 
 
        $streetwork_time_query = mysqli_query($dbc,"SELECT datum, uhrzeit FROM streetwork_record"); 
 
        $fquery = array(); 
 
        while ($streetwork_time_array = mysqli_fetch_array($streetwork_time_query)) { 
 
            if ($streetwork_time_array['datum']==$date) { 
                $fquery[] = $streetwork_time_array['uhrzeit']; 
            } 
        } 
 
        if(empty($fquery)){ 
            $timediff=0; 
            $objPHPExcel->getActiveSheet()->SetCellValue('S'.$row, $timediff); 
             
        }else{ 
 
            $maxtime = max($fquery); 
            $mintime = min($fquery); 
            $timediff=$maxtime-$mintime; 
 
            $objPHPExcel->getActiveSheet()->SetCellValue('S'.$row, $timediff); 
 
        } 
 
      // ******************************************************************************************************************** 
 
      // ****** Gruppenarbeit *********************************************************************************************** 
 
    $gruppenarbeit_day_sum = 0; 
 
    $all_dauer_gruppenarbeit_query = mysqli_query($dbc,"SELECT datum, dauer_in_h FROM gruppenarbeit_record"); 
 
    while ($all_dauer_gruppenarbeit_array = mysqli_fetch_array($all_dauer_gruppenarbeit_query)) { 
 
      if ($all_dauer_gruppenarbeit_array['datum']==$date) { 
 
        $gruppenarbeit_day_sum = $gruppenarbeit_day_sum + $all_dauer_gruppenarbeit_array['dauer_in_h']; 
 
      } 
    } 
 
    $objPHPExcel->getActiveSheet()->SetCellValue('R'.$row, $gruppenarbeit_day_sum); 
 
 
      // ******************************************************************************************************************** 
 
      // ****** Einzelarbeit ************************************************************************************************ 
 
    $einzelarbeit_day_sum = 0; 
 
    $all_dauer_einzelarbeit_query = mysqli_query($dbc,"SELECT datum, dauer_in_h FROM einzelfallhilfe_record"); 
 
    while ($all_dauer_einzelarbeit_array = mysqli_fetch_array($all_dauer_einzelarbeit_query)) { 
 
      if ($all_dauer_einzelarbeit_array['datum']==$date) { 
 
        $einzelarbeit_day_sum = $einzelarbeit_day_sum + $all_dauer_einzelarbeit_array['dauer_in_h']; 
 
      } 
    } 
 
    $objPHPExcel->getActiveSheet()->SetCellValue('Q'.$row, $einzelarbeit_day_sum); 
 
        // ******************************************************************************************************************** 
 
        // *** Gemeinwesenarbeit ********************************************************************************************** 
 
        $gemeinwesenarbeit_day_sum = 0; 
 
        $all_dauer_gemeinwesenarbeit_query = mysqli_query($dbc,"SELECT datum, dauer_in_h FROM gemeinwesenarbeit_record"); 
 
        while ($all_dauer_gemeinwesenarbeit_array = mysqli_fetch_array($all_dauer_gemeinwesenarbeit_query)) { 
 
            if ($all_dauer_gemeinwesenarbeit_array['datum']==$date) { 
 
                $gemeinwesenarbeit_day_sum = $gemeinwesenarbeit_day_sum + $all_dauer_gemeinwesenarbeit_array['dauer_in_h']; 
 
            } 
        } 
 
    $objPHPExcel->getActiveSheet()->SetCellValue('P'.$row, $gemeinwesenarbeit_day_sum); 
 
 
      // ******************************************************************************************************************** 
 
 
    $weiblich_day_sum = 0; 
    $mannlich_day_sum = 0; 
 
    $all_genders_query = mysqli_query($dbc,"SELECT datum, n_u12_m, n_u12_w, n_u14_m, n_u14_w, n_u14_17_m, n_u14_17_w, n_u18_20_m, n_u18_20_w, n_u21_24_m, n_u21_24_w, n_u25_26_m, n_u25_26_w, n_o27_m, n_o27_w FROM streetwork_record UNION ALL SELECT datum, n_u12_m, n_u12_w, n_u14_m, n_u14_w, n_u14_17_m, n_u14_17_w, n_u18_20_m, n_u18_20_w, n_u21_24_m, n_u21_24_w, n_u25_26_m, n_u25_26_w, n_o27_m, n_o27_w FROM gruppenarbeit_record"); 
 
    while ($all_genders_array = mysqli_fetch_array($all_genders_query)) { 
 
      if ($all_genders_array['datum']==$date) { 
 
        $weiblich_day_sum = $weiblich_day_sum + $all_genders_array['n_u12_w'] + $all_genders_array['n_u14_w'] + $all_genders_array['n_u14_17_w'] + $all_genders_array['n_u18_20_w'] + $all_genders_array['n_u21_24_w'] + $all_genders_array['n_u25_26_w'] + $all_genders_array['n_o27_w']; 
 
        $mannlich_day_sum = $mannlich_day_sum + $all_genders_array['n_u12_m'] + $all_genders_array['n_u14_m'] + $all_genders_array['n_u14_17_m'] + $all_genders_array['n_u18_20_m'] + $all_genders_array['n_u21_24_m'] + $all_genders_array['n_u25_26_m'] + $all_genders_array['n_o27_m']; 
      } 
    } 
 
    $objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, $weiblich_day_sum); 
    $objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, $mannlich_day_sum); 
 
    $row++; 
    $date = date ("Y-m-d", strtotime("+1 day", strtotime($date))); 
    } 
     
  $sheetIndex++; 
} 
 

// header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
// header('Content-Disposition: attachment;filename="Statistik_18_mobile_JA.xlsx"');
// header('Cache-Control: max-age=0');

// $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
// $writer->save('php://output');

$writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);

ob_start();
$writer->save('php://output');
$xlsData = ob_get_contents();
ob_end_clean();
$response =  array(
    'op' => 'ok',
    'file' => "data:application/vnd.ms-excel;base64,".base64_encode($xlsData)
);
die(json_encode($response));
 
?> 