<?php

	require('../../resources/dbconnection.php');

	$data_missing = array();
	$response = array();

	if(isset($_POST['project_name'])) {
		$project_name = trim($_POST['project_name']);
	} else {
		$data_missing[] = 'project_name';
	}

	if(isset($_POST['datum'])) {
		$datum = trim($_POST['datum']);
	} else {
		$data_missing[] = 'datum';
	}

	if(isset($_POST['dauer_in_h'])) {
		$dauer_in_h = trim($_POST['dauer_in_h']);
	} else {
		$data_missing[] = 'dauer_in_h';
	}
	
	if(isset($_POST['n_u12_m'])) {
		$n_u12_m = trim($_POST['n_u12_m']);
	} else {
		$data_missing[] = 'n_u12_m';
	}

	if(isset($_POST['n_u12_w'])) {
		$n_u12_w = trim($_POST['n_u12_w']);
	} else {
		$data_missing[] = 'n_u12_w';
	}

	if(!isset($_POST['n_u14_m'])) {
		$data_missing[] = 'n_u14_m';
	} else {
		$n_u14_m = trim($_POST['n_u14_m']);
	}

	if(!isset($_POST['n_u14_w'])) {
		$data_missing[] = 'n_u14_w';
	} else {
		$n_u14_w = trim($_POST['n_u14_w']);
	}

	if(!isset($_POST['n_u14_17_m'])) {
		$data_missing[] = 'n_u14_17_m';
	} else {
		$n_u14_17_m = trim($_POST['n_u14_17_m']);
	}

	if(!isset($_POST['n_u14_17_w'])) {
		$data_missing[] = 'n_u14_17_w';
	} else {
		$n_u14_17_w = trim($_POST['n_u14_17_w']);
	}
	
	if(!isset($_POST['n_u18_20_m'])) {
		$data_missing[] = 'n_u18_20_m';
	} else {
		$n_u18_20_m = trim($_POST['n_u18_20_m']);
	}

	if(!isset($_POST['n_u18_20_w'])) {
		$data_missing[] = 'n_u18_20_w';
	} else {
		$n_u18_20_w = trim($_POST['n_u18_20_w']);
	}

	if(!isset($_POST['n_u21_24_m'])) {
		$data_missing[] = 'n_u21_24_m';
	} else {
		$n_u21_24_m = trim($_POST['n_u21_24_m']);
	}

	if(!isset($_POST['n_u21_24_w'])) {
		$data_missing[] = 'n_u21_24_w';
	} else {
		$n_u21_24_w = trim($_POST['n_u21_24_w']);
	}

	if(!isset($_POST['n_u25_26_m'])) {
		$data_missing[] = 'n_u25_26_m';
	} else {
		$n_u25_26_m = trim($_POST['n_u25_26_m']);
	}

	if(!isset($_POST['n_u25_26_w'])) {
		$data_missing[] = 'n_u25_26_w';
	} else {
		$n_u25_26_w = trim($_POST['n_u25_26_w']);
	}

	if(!isset($_POST['n_o27_m'])) {
		$data_missing[] = 'n_o27_m';
	} else {
		$n_o27_m = trim($_POST['n_o27_m']);
	}

	if(!isset($_POST['n_o27_w'])) {
		$data_missing[] = 'n_o27_w';
	} else {
		$n_o27_w = trim($_POST['n_o27_w']);
	}

	if(!isset($_POST['n_bekannt'])) {
		$data_missing[] = 'n_bekannt';
	} else {
		$n_bekannt = trim($_POST['n_bekannt']);
	}

	if(!isset($_POST['n_unbekannt'])) {
		$data_missing[] = 'n_unbekannt';
	} else {
		$n_unbekannt = trim($_POST['n_unbekannt']);
	}

	if(!isset($_POST['th_sucht'])) {
		$data_missing[] = 'th_sucht';
	} else {
		$th_sucht = trim($_POST['th_sucht']);
	}

	if(!isset($_POST['th_gewalt'])) {
		$data_missing[] = 'th_gewalt';
	} else {
		$th_gewalt = trim($_POST['th_gewalt']);
	}

	if(!isset($_POST['th_kriminalitat'])) {
		$data_missing[] = 'th_kriminalitat';
	} else {
		$th_kriminalitat = trim($_POST['th_kriminalitat']);
	}

	if(!isset($_POST['th_gesundheit'])) {
		$data_missing[] = 'th_gesundheit';
	} else {
		$th_gesundheit = trim($_POST['th_gesundheit']);
	}

	if(!isset($_POST['th_politische_themen'])) {
		$data_missing[] = 'th_politische_themen';
	} else {
		$th_politische_themen = trim($_POST['th_politische_themen']);
	}

	if(!isset($_POST['th_wohnraum'])) {
		$data_missing[] = 'th_wohnraum';
	} else {
		$th_wohnraum = trim($_POST['th_wohnraum']);
	}

	if(!isset($_POST['th_arbeit'])) {
		$data_missing[] = 'th_arbeit';
	} else {
		$th_arbeit = trim($_POST['th_arbeit']);
	}

	if(!isset($_POST['th_behorden'])) {
		$data_missing[] = 'th_behorden';
	} else {
		$th_behorden = trim($_POST['th_behorden']);
	}

	if(!isset($_POST['th_finanzielle'])) {
		$data_missing[] = 'th_finanzielle';
	} else {
		$th_finanzielle = trim($_POST['th_finanzielle']);
	}

	if(!isset($_POST['th_freizeit'])) {
		$data_missing[] = 'th_freizeit';
	} else {
		$th_freizeit = trim($_POST['th_freizeit']);
	}

	if(!isset($_POST['th_beziehung'])) {
		$data_missing[] = 'th_beziehung';
	} else {
		$th_beziehung = trim($_POST['th_beziehung']);
	}

	if(!isset($_POST['th_identitat'])) {
		$data_missing[] = 'th_identitat';
	} else {
		$th_identitat = trim($_POST['th_identitat']);
	}

	if(!isset($_POST['th_digitales'])) {
		$data_missing[] = 'th_digitales';
	} else {
		$th_digitales = trim($_POST['th_digitales']);
	}

	if(!isset($_POST['th_vorstellung'])) {
		$data_missing[] = 'th_vorstellung';
	} else {
		$th_vorstellung = trim($_POST['th_vorstellung']);
	}

	if(!isset($_POST['th_familie'])) {
		$data_missing[] = 'th_familie';
	} else {
		$th_familie = trim($_POST['th_familie']);
	}

	if(!isset($_POST['th_sonstiges'])) {
		$data_missing[] = 'th_sonstiges';
	} else {
		$th_sonstiges = trim($_POST['th_sonstiges']);
	}

	if(!isset($_POST['anmerkungen'])) {
		$data_missing[] = 'anmerkungen';
	} else {
		$anmerkungen = trim($_POST['anmerkungen']);
	}

	if(!isset($_POST['mitarbeiter01'])) {
		$data_missing[] = 'mitarbeiter01';
	} else {
		$mitarbeiter01 = trim($_POST['mitarbeiter01']);
	}

	if(!isset($_POST['mitarbeiter02'])) {
		$data_missing[] = 'mitarbeiter02';
	} else {
		$mitarbeiter02 = trim($_POST['mitarbeiter02']);
	}

	if(!isset($_POST['mitarbeiter03'])) {
		$data_missing[] = 'mitarbeiter03';
	} else {
		$mitarbeiter03 = trim($_POST['mitarbeiter03']);
	}

if (empty($data_missing)) {

	$query = "INSERT INTO gruppenarbeit_record (datum, dauer_in_h, mitarbeiter01, mitarbeiter02, mitarbeiter03, n_bekannt, n_unbekannt, n_u12_m, n_u12_w, n_u14_m, n_u14_w, n_u14_17_m, n_u14_17_w, n_u18_20_m, n_u18_20_w, n_u21_24_m, n_u21_24_w, n_u25_26_m, n_u25_26_w, n_o27_m, n_o27_w, th_sucht, th_gewalt, th_kriminalitat, th_gesundheit, th_freizeit, th_identitat, th_wohnraum, th_familie, th_behorden, th_finanzielle, th_arbeit, th_beziehung, th_politische_themen, th_digitales, th_vorstellung, th_sonstiges, anmerkungen, project_name) VALUES ('$datum', '$dauer_in_h', '$mitarbeiter01', '$mitarbeiter02', '$mitarbeiter03', '$n_bekannt', '$n_unbekannt', '$n_u12_m', '$n_u12_w', '$n_u14_m', '$n_u14_w', '$n_u14_17_m', '$n_u14_17_w', '$n_u18_20_m', '$n_u18_20_w', '$n_u21_24_m', '$n_u21_24_w', '$n_u25_26_m', '$n_u25_26_w', '$n_o27_m', '$n_o27_w', '$th_sucht', '$th_gewalt', '$th_kriminalitat', '$th_gesundheit', '$th_freizeit', '$th_identitat', '$th_wohnraum', '$th_familie', '$th_behorden', '$th_finanzielle', '$th_arbeit', '$th_beziehung', '$th_politische_themen', '$th_digitales', '$th_vorstellung', '$th_sonstiges', '$anmerkungen', '$project_name')";

	mysqli_query($dbc,$query);
	$affected_rows = mysqli_affected_rows($dbc);
 	
		if ($affected_rows == 1) {
			$response['message'] = "Record added";
		} else {
			$response['message'] = "Error with request execution";
		}

		mysqli_close($dbc);
		echo json_encode($response);

	}else{
		echo "data_missing";

		foreach ($data_missing as $missing) {
			echo "<p>";
			echo "$missing <br/>";}
	}
?>